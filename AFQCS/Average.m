% AVG function takes populations' data in specific locus and loaded data
%AVG function produce AVG line and (pop.test / saved pop.) line
function [ Avr , locs_test ] = Average( locs ,~ ,load_data   )
if isempty(load_data)
   msgbox('No population entered','Error','Error');
else
locs(isnan(locs))=0; %put zeroes to element of populations' data in specific locus which values are nan
[p,~]=size(locs);
locs_test = load_data;
[~,col_locs]=size(locs);
if  isnan(locs_test) == 1 ;  % if no loaded country (testes/saved) plot only avarage line
    c(:,(2:col_locs))=0;
   locs=[locs;c];
   [~,idx]=sort(locs(:,1));    
   locs=locs(idx,:);
   s=sum(locs(:,(2:end)),2);
   Avr=zeros(p,1);
   for i=1:p
     Avr(i,1)=s(i,1)./nnz(locs(i,:));
   end
else
   [r,~]=size(locs_test);
   t=1;
   for i=1:r
       if locs_test(t,2)==0
           locs_test(t,:)=[];
       else
           t=t+1;
       end
   end
   [c,~]=setdiff(locs(:,1),locs_test(:,1));
   c(:,2)=0;
   locs_test=[locs_test;c];
   [~,idx]=sort(locs_test(:,1));    
   locs_test=locs_test(idx,:);
   [c,~]=setdiff(locs_test(:,1),locs(:,1));
   c(:,(2:col_locs))=0;
   locs=[locs;c];
   [~,idx]=sort(locs(:,1));
   locs=locs(idx,:);
   s=sum(locs(:,(2:end)),2);
   Avr=zeros(p,1);
   for i=1:p
     Avr(i,1)=s(i,1)./nnz(locs(i,:));
   end
end
end
end