function [fig,all_data] = Comparison_Result( corr_file ,pvalue_file,locs_name ,image_name , all_data ,countries)
if isnan(all_data)
msgbox('No population selected');
fig = nan ;
else
%% Statistics
[~,COL]=size(all_data) ;
if ( (COL-1) < 6 )
The_median = 0;     
First_quartile = 0 ;
Third_quartile = 0 ;
Interquartire_range = 0   ; 
else
[iqr,med,thirdQ,firstQ] = IQR(all_data) ;
The_median = med;     
First_quartile = firstQ ;
Third_quartile = thirdQ ;
Interquartire_range = iqr   ; 
end

% read data with no zero to compute corr. coef.
poles= any(all_data);
countries=countries(poles(2:end));
[~,counrties_number] = size(countries);
data_no_allels=all_data(:,2:end);
data_no_allels=data_no_allels(:,poles(2:end));

%compute correlation and p-value matrix then write them into excel files
[~,locusColumns] = size(data_no_allels);
[RofLocus,PofLocus] = corr(data_no_allels,data_no_allels);


xlswrite(corr_file,RofLocus,'Correlation Coef.','B2');
xlswrite(corr_file,countries','Correlation Coef.','A2');
xlswrite(corr_file,countries,'Correlation Coef.','B1');

xlswrite(pvalue_file,PofLocus,'P-values','B2');
xlswrite(pvalue_file,countries','P-values','A2');
xlswrite(pvalue_file,countries,'P-values','B1');

% size of correlation matrix
matrix_size = size(RofLocus) ;
if ( (COL-1) < 2 )
minum = 0;
maxum = 0;
else
%min CCM elements
minum = min(min(RofLocus));

%max CCM elements
CCM_not_one =RofLocus(RofLocus~=1);
maxum = max(CCM_not_one);
end
%%
gaussianMatrixSize = 0 ;
for i = 1:(locusColumns-1)
    gaussianMatrixSize = gaussianMatrixSize + i;
end
gaussianMatrix = zeros(gaussianMatrixSize,1);
pValues= zeros(gaussianMatrixSize,1);
newcounter=2;
gaussianMatrixCounter = 1 ;
for x = 1:(locusColumns-1)
    for y = newcounter:locusColumns
        gaussianMatrix(gaussianMatrixCounter,1) = RofLocus(x,y);
        pValues(gaussianMatrixCounter,1) = PofLocus(x,y);
        gaussianMatrixCounter = gaussianMatrixCounter + 1;

    end
    newcounter = newcounter + 1 ;
end

%count of CCM elements 
count = gaussianMatrixSize ;
count1 = 0 ;
for i = 1:gaussianMatrixSize
    if pValues(i,1) < 0.05
       count1 = count1 + 1 ;
    end
end
% %of p-value matrix elements < alpha=0.05
pValuesPer = (count1 / gaussianMatrixSize) * 100;

%% outliers
locs = all_data ;
locsout=locs(:,(2:end));
[r,c]=size(locsout);
totout=zeros(r,c);
dfout=zeros(r,c);
dfouttest=zeros(r,1);


m=mean(locsout,2);
s=std(locsout,0,2);
mr=repmat(m,1,c);
sr=repmat(s,1,c);
mm=abs(mr-locsout);
d=(3*sr)-mm;
[A,B]=find(d<0);
for i=1:length(A)
    totout(A(i),B(i))=locsout(A(i),B(i));
end

outlierLoad_data = nan;
Load_data = nan ;
if (~isnan(Load_data))
 mm=abs(m-Load_data(:,2));
 d=(3*s)-mm;
 [q,~]=find(d<0);
  outlierLoad_data=zeros(size(d,1),1);
  outlierLoad_data(q,1)=Load_data(q,2);   
end


for i=2:r
    row=locsout(i,:)-locsout(i-1,:);
    rowtest = 0;
    Load_data = nan ;
    if (~isnan(Load_data))
     rowtest=Load_data(i,2)-Load_data(i-1,2);
    end
    postslp=find(row>0);
    negtslp=find(row<0);
    [~,np]=size(postslp);
    [~,nn]=size(negtslp);
    if np>nn
        for h=1:nn
     dfout(i,negtslp(h))=locsout(i,negtslp(h));
        end
       if rowtest<0
           dfouttest(i,1)=Load_data(i,2);
       end
    else
        for h=1:np
       dfout(i,postslp(h))=locsout(i,postslp(h));
        end
         if rowtest>0
            dfouttest(i,1)=Load_data(i,2);
        end
    end
end
[g,h]=find(dfout~=totout);
dfout(g,h)=0;
fout=totout-dfout;
[g1,h1]=find(dfouttest~=outlierLoad_data);
dfouttest(g1,h1)=0;
fouttest=outlierLoad_data-dfouttest;
fout=[fout,fouttest];
dfout=[dfout,dfouttest];
outlierLoad_data = nan ;
if (~isnan(outlierLoad_data))
totout=[totout,outlierLoad_data];
end
fout(fout==0)=NaN;
dfout(dfout==0)=NaN;

fig=figure('Name',locs_name,'NumberTitle','off','units','normalized','outerposition',[0 0 1 1]);

%% divide figure into axeses to plot trend , images , table and statistics
ax1 = axes('Position',[0 0 1 1],'Visible','off'); 
ax2 = axes('Position',[.3 .1 .69 .82]);
ax3 = axes('Position',[0.035 0.72 .2 .1],'Visible','off');
ax4 = axes('Position',[0 0 1 1],'Visible','off');
ax5 = axes('Position',[0 0 1 1],'Visible','off');
ax6 = axes('Position',[0.025 0.045 .02 .02],'Visible','off');
ax7 = axes('Position',[0.025 0.08 .02 .02],'Visible','off');

% write title of figure
axes (ax5)
Load_data = nan ;
if isnan(Load_data)
    locs_name_text = text(.3,0.97,locs_name);
    set(locs_name_text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 22);
    Pop_name = nan ;
else
    Pop_name =  getappdata (0, 'population_Name');
    locs_name = strcat( Pop_name , '  -  ' ,locs_name);
    locs_name_text = text(.3,0.97,locs_name);
    set(locs_name_text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 22);
end 

% write sub-title for the statistics part
   Title_1 = '           Result';
   line_1 = 'This graph represents'; 
   Country_num = counrties_number; %the number of counties in each locus
   line_1_2 = {' countries '
                     ' '};
   line_2 = {'The correlation coff. matrix of size '};

axes(ax1) 
    Tit_text = text(.025,0.98,Title_1);
    set(Tit_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')

    line1_text = text(.025,0.93,line_1);
    set(line1_text, 'FontName', 'Times', 'FontWeight', 'bold')

    Country_num_text =text (.12,0.93,num2str(Country_num));
    set(Country_num_text, 'FontName', 'Times', 'FontWeight', 'bold')

    line_1_2_text = text (.13,0.92,line_1_2);
    set(line_1_2_text, 'FontName', 'Times', 'FontWeight', 'bold')

    line_2_text =text (.025,0.91,line_2);
    set(line_2_text, 'FontName', 'Times', 'FontWeight', 'bold')

    matrix_size_text = text (.17,0.91,num2str(matrix_size));
    set(matrix_size_text, 'FontName', 'Times', 'FontWeight', 'bold')

    % comment before screen shoot of sample CCM
    line_3 = 'Following is a sample of correlation coff. matrix (CCM)' ;
    line_3_text = text (.025,0.87,line_3);
    set(line_3_text, 'FontName', 'Times', 'FontWeight', 'bold')
%ploting image (screen shot of CCM)
axes (ax3)
    imshow (image_name);

%sub-title and values of mean , std ... and other values
axes (ax4)
    line5 = 'CCM Descriptive Statistics: ' ;
    line5_text =text (0.025 , 0.64,line5);
    set(line5_text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 15)

   line6 = 'The median is ';
   line6_text =text (0.025 , 0.60,line6);
   set(line6_text, 'FontName', 'Times', 'FontWeight', 'bold')
   median_Text =text (0.2,0.60,num2str(The_median));
   set(median_Text, 'FontName', 'Times', 'FontWeight', 'bold')
   
   line7 = 'First quaritile ';
   line7_text =text (0.025 , 0.57,line7);
   set(line7_text, 'FontName', 'Times', 'FontWeight', 'bold')
   First_quaritile_text  =text (0.2,0.57,num2str(First_quartile));
   set(First_quaritile_text, 'FontName', 'Times', 'FontWeight', 'bold')
   
   line8 = 'Third quaritile ';
   line8_text =text (0.025 , 0.54,line8);
   set(line8_text, 'FontName', 'Times', 'FontWeight', 'bold')
   Third_quaritile_text =text (0.2,0.54,num2str(Third_quartile));
   set(Third_quaritile_text, 'FontName', 'Times', 'FontWeight', 'bold')
   
   line9 = 'The interquartire range ';
   line9_text =text (0.025 , 0.51,line9);
   set(line9_text, 'FontName', 'Times', 'FontWeight', 'bold')
   Interquartire_range_text =text (0.2,0.51,num2str(Interquartire_range));
   set(Interquartire_range_text, 'FontName', 'Times', 'FontWeight', 'bold')
   

    line10 ='count of CCM elements.' ;
    line10_text = text (0.025 , 0.48,line10);
    set(line10_text, 'FontName', 'Times', 'FontWeight', 'bold')
    countText = text (0.2,0.48,num2str(count));
    set(countText, 'FontName', 'Times', 'FontWeight', 'bold')    
    
    line8 = 'minimum value of CCM elements ' ;
    line8_text = text (0.025 , 0.45,line8);
    set(line8_text, 'FontName', 'Times', 'FontWeight', 'bold')
    minText = text (0.2,0.45,num2str(minum));
    set(minText, 'FontName', 'Times', 'FontWeight', 'bold')
    
    line9 = 'maximum value of CCM elements' ;
    line9_text = text (0.025 , 0.42,line9);
    set(line9_text, 'FontName', 'Times', 'FontWeight', 'bold')
    maxText = text (0.2,0.42,num2str(maxum));
    set(maxText, 'FontName', 'Times', 'FontWeight', 'bold')

    
    line14 = '% of P-value matrix elements < ( \alpha =0.05)  = ';
    line14_Text = text (0.025 , 0.39,line14);
    set(line14_Text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize' , 9.5)
    pvalue_text =text (0.2,0.39,num2str(pValuesPer));
    set(pvalue_text, 'FontName', 'Times', 'FontWeight', 'bold')

    %sub-title for outliers table
    line16 = 'Outliers:' ;
    line16_text =text (0.025,0.32,line16);
    set(line16_text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 15)
    
    if (totout==0)
    axes (ax4)
    line ={ 'There is no outlier value between' ; 
                'the populations selected' };
    line_text =text (0.025,0.25,line);
    set(line_text, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 15)
 
    else    %  create legend after outliers table 
    line17 = 'Following the trend';
    line18 = 'Does not follow the trend';
    text (0.05,0.09,line17);
    text (0.05,0.06,line18);
 
    axes (ax6)
    x = [0 1 1 0];
    y = [0 0 1 1];
    patch(x,y,'red')
 
axes (ax7)
    x = [0 1 1 0];
    y = [0 0 1 1];
    patch(x,y,'blue')
    end
%ploting the trend with outliers and (tested pop. / saved pop.) if existed
    axes(ax2);
    cla
    a=all_data(:,1);
    all_data=all_data(:,(2:end));
    plot(all_data); hold on % trend
    plot(fout,'b+','MarkerSize',7);  % outliers following trend
    plot(dfout,'ro','MarkerSize',7); % outliers doesn't follow trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    setappdata(0,'countries',countries);
    legend(countries) % create box of legend    

    fout(isnan(fout))=0;
    dfout(isnan(dfout))=0;
    any(totout) ;
    if (~isnan(Pop_name))
    countries=[countries,Pop_name];
    end
    countries=countries';
    g=any(totout,2);
    alleles=a(g);
    alleles=alleles';
    o=~any(totout,2);
    u=~any(totout,1);
    totout(o,:)=[];
    totout(:,u)=[];
    dfout(o,:)=[];
    dfout(:,u)=[];
    fout(o,:)=[];
    fout(:,u)=[];
    [r,c]=find(dfout~=0);
    [q,v]=find(fout~=0);
 

 %create uitable for ooutliers
 %transform data of table to cell string element
 if(~isempty(totout))  
 totout = reshape(strtrim(cellstr(num2str(totout(:)))), size(totout));
 %color the elements
 % red for doesn't follow trend
 for i=1 : size(r)
 totout(r(i),c(i)) = strcat(...
    '<html><span style="color: #FF0000 ; font-weight: bold;">', ...
    totout(r(i),c(i)), ...
   '</span></html>');
 end
 %color the elements
 % blue for following trend
 for i=1 : size(q)
 totout(q(i),v(i)) = strcat(...
   '<html><span style="color: #0000FF ; font-weight: bold;">', ...
  totout(q(i),v(i)), ...
 '</span></html>');
end
 
ColumnName = countries;
RowName =  alleles ;
uitable('Data', totout, 'ColumnName', ColumnName ,'RowName',RowName,'FontWeight','bold' ,...
    'FontName', 'Times', 'Units','normalized','Position', [0.025 0.11 0.21 0.19]);
setappdata(0,'locsout',locsout);
setappdata(0,'locs',locs);
setappdata(0,'fout',fout);
setappdata(0,'dfout',dfout);
 end
end
end
