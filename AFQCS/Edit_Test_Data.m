%Edit_Test_Data function is a preprocessing to file loaded to be tested
%compare alleles name in our database and loaded file

function [ test_locs ,global_locs , Rejected ] = Edit_Test_Data( test_locs ,global_locs , Rejected )
   [r,~]=size(test_locs);
   t=1;
   for i=1:r
       if  test_locs(t,2)==0
           test_locs(t,:)=[];
       else
           t=t+1;
       end
   end 
   [c,~]=setdiff(global_locs(:,1),test_locs(:,1));
   c(:,2)=0;
   test_locs=[test_locs;c];
   [~,idx]=sort(test_locs(:,1));
   test_locs=test_locs(idx,:);
   [c,~]=setdiff(test_locs(:,1),global_locs(:,1));
   [~,COL] = size(global_locs);
   c(:,(2:COL))=0;        
   global_locs=[global_locs;c];
   [~,idx]=sort(global_locs(:,1));
   global_locs=global_locs(idx,:);
   
 [z,~]=setdiff (  global_locs(:,1) , Rejected(:,1) ) ;
 [~,b]=size(Rejected);
 z(:,{2:b})=0;
 Rejected=[Rejected;z];
 [~,idx]=sort( Rejected(:,1));
  Rejected= Rejected(idx,:); 
 
end
