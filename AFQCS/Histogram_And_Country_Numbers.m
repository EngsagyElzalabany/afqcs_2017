%Histogram_and_Country_number function compute gaussian Matrix to ploting histogram
%and also compute the numbers of countries in each locus
%it just take the name of file contain populations' data for specific locus
function [Columns,R  ,xbins] = Histogram_And_Country_Numbers( w )
[~,Columns] = size(w); % get the numbers of countries in each locus
w(isnan(w))=0;
w=w(:,(2:end));%exclude Alleles column and last three coloumns
z=any(w);%find any zero coloumns
w=w(:,z);%delete zero columns
R=corrcoef(w);%find correlation coeff. between countries
R=triu(R,1);% leave the upper triangle and replace the rest with zeros
ind= find(R);%find all non zero elements
R=R(ind);%exclude zero elements and make the matrix a vector
c=size(R,1);%get the number of elements in R
xbins=min(R):(max(R)-min(R))/c:max(R);% define the number of bins in histogram
