function [IQR,med,thirdQ,firstQ] = IQR(w)
w(isnan(w))=0;
w=w(:,(2:end));%exclude Alleles column and last three coloumns
z=any(w);%find any zero coloumns
w=w(:,z);%delete zero columns
R=corrcoef(w);%find correlation coeff. between countries
R=triu(R,1);% leave the upper triangle and replace the rest with zeros
ind= find(R);%find all non zero elements
R=R(ind);%exclude zero elements and make the matrix a vector
c=size(R,1);%get the number of elements in R
xbins=min(R):(max(R)-min(R))/c:max(R);% define the number of bins in histogram
[n,cent]=hist(R,xbins);%get the the coordination of each bin
figure; bar(cent,n);%draw the histogram
Q=c/4;%find the quartile value
Q=round(Q);%round it to exclude the decimalx
summ=0;% initialize the summ to enter while loop
k=2;% initialize k variable to start from the beginning
while(summ <= Q)% as long as summ is less than the quartile value do the wile
    summ=sum(n(1:k));%sum the elements inside n which is the height of the bin in histogram
  k=k+1;% increment k to add more elements
end
Q1=k-2;%after while is done here is the first quartile bin number
firstQ=cent(Q1);%the first quartile correlation coeff. value
%repetition of the while loop to find the rest quartiles
summ=0;
k=2;
while(summ <= 2*Q)
  summ=sum(n(1:k));
  k=k+1;
end
Q2=k-2;
med=cent(Q2);
summ=0;
k=2;
while(summ <= 3*Q)
  summ=sum(n(1:k));
  k=k+1;
end
Q3=k-2;
thirdQ=cent(Q3);

IQR=cent(Q3)-cent(Q1);%interquartile range is the difference between third and first quartiles
figure(1);hold on
%draw a line at median, third and first quartiles (median is the second quartile)
plot([cent(Q2) cent(Q2)],[min(n) max(n)],'color','r');
plot([cent(Q3) cent(Q3)],[min(n) max(n)],'color','r');
plot([cent(Q1) cent(Q1)],[min(n) max(n)],'color','r');
end
