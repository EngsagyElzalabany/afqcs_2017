function varargout = Load_New_Data(varargin)
% Load_New_Data MATLAB code for Load_New_Data.fig
%      Load_New_Data, by itself, creates a new Load_New_Data or raises the existing
%      singleton*.
%
%      H = Load_New_Data returns the handle to a new Load_New_Data or the handle to
%      the existing singleton*.
%
%      Load_New_Data('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in Load_New_Data.M with the given input arguments.
%
%      Load_New_Data('Property','Value',...) creates a new Load_New_Data or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Load_New_Data_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Load_New_Data_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Load_New_Data

% Last Modified by GUIDE v2.5 01-Aug-2017 10:56:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Load_New_Data_OpeningFcn, ...
                   'gui_OutputFcn',  @Load_New_Data_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Load_New_Data is made visible.
function Load_New_Data_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Load_New_Data (see VARARGIN)

% Choose default command line output for Load_New_Data
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Load_New_Data wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% creating sample table. this table is our standard to excel file of tested
% data
%this is static table with imagine value. it just to show sample of excel file to load
%file in correct way
numbers = [0,0,0.208,0,0,0,0,0,0,0,0,0,0,0,0;
          0,0,0,0.003,0.056,0.272,0.052,0.2,0.065,0.298,0.051,0,0.016,0.068,0.006;
          0.01,.008,0,0.262,0.278,0.328,0.298,0.45,0.2,0.065,0,0.005,0.282,0,0.027;
          0.01,0,0,0,0.038,0.323,0.37,0.322,0.156,0.9,0.13,0,0.121,0.235,0.067;
          0,0,0.208,0,0,0,0,0,0,0,0,0,0,0,0.3;
          0,0,0,0.003,0.056,0.272,0.052,0.065,0.48,0.298,0.051,0,0.016,0.068,0.006;
          0.01,.008,0,0.262,0.278,0.328,0.298,0.2,.07,0.065,0,0.005,0.282,0,0.027;
          0.01,0,0,0,0.038,0.323,0.37,0.322,0.156,0.130,.7,0,0.121,0.235,0.067];
Row = {'4' , '5' , '9' , '9.2', '10', '11', '11.2', '12'};
Column ={'D3S1358', 'vWA' ,'FGA' ,'TH01' , 'TPOX' ,'CSF1PO', 'D5S818', 'D13S317' ,'D7S820',...
         'D8S1179' ,'D21S11' ,'D18S51' ,'D16S539' ,'D2S133' ,'D19S433'} ;
Column = Column' ;
Row = Row' ;
uitable('Data', numbers, 'ColumnName', Column ,'RowName',Row,'FontWeight','bold'...
    ,'FontName', 'Times', 'Units','normalized','Position', [0.38 0.4 0.6 0.3]);

% UIWAIT makes Load_New_Data wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Load_New_Data_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
% --- Executes on button press in Load_button.
function Load_button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%browesing for loading excel file 
[file, path] = uigetfile('*.xlsx');
 if isequal(file,0) || isequal(path,0)
    disp('User pressed cancel')
 else
    file = strcat(path,file);
    test = xlsread(strcat(file)); %read file to test matrix
    test(isnan(test))=0; %set nan element to zero
    newpopulation = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation');
    if ~isempty(newpopulation)
    [ROW,COL]=size(newpopulation);
    newpopulation=nan(ROW,COL);
    xlswrite('Excel_Files\Tested_populations.xlsx',newpopulation,'newpopulation');
    end
    xlswrite('Excel_Files\Tested_populations.xlsx',test,'newpopulation'); %save matrix to excel file to read it by the main gui window
 % get the text from the 6 text boxes
    Jornal     = get(handles.Journal_text,'string');
    Author     = get(handles.Author_text,'string');
    Date      = get(handles.Date_text,'string');
    PaperTitle = get(handles.Paper_text,'string');
    Population = get(handles.Population_text,'string');
    sampleSize = get(handles.Size_text,'string');
 %Stract the 6 texts in one text to store and show them
    some_info = strcat('jornal:-  ',Jornal, '   /Paper Title:-   ',PaperTitle);
    some_info =  cellstr(some_info);

   Jornal =  cellstr(Jornal);
   Author =  cellstr(Author);
   Date =  cellstr(Date);
   PaperTitle =  cellstr(PaperTitle);
   Population =  cellstr(Population);
   sampleSize =  cellstr(sampleSize);
 
    % store in excel file to read them in another gui
    xlswrite('Excel_Files\Tested_populations.xlsx',some_info,'newpopulation_data1','J1');
    xlswrite('Excel_Files\Tested_populations.xlsx',Jornal,'newpopulation_data1','K1');
    xlswrite('Excel_Files\Tested_populations.xlsx',Author,'newpopulation_data1','L1');
    xlswrite('Excel_Files\Tested_populations.xlsx',Date,'newpopulation_data1','M1');
    xlswrite('Excel_Files\Tested_populations.xlsx',PaperTitle,'newpopulation_data1','N1');
    xlswrite('Excel_Files\Tested_populations.xlsx',Population,'newpopulation_data1','O1');
    xlswrite('Excel_Files\Tested_populations.xlsx',sampleSize,'newpopulation_data1','P1');
    xlswrite('Excel_Files\Tested_populations.xlsx',Population,'newpopulation_data2','F1');
    % the main gui is waitting for user to load test file and write it's info (uiwait)
    %uiresume resume the hold progrom to excute
    uiresume    

 end



function Journal_text_Callback(hObject, eventdata, handles)
% hObject    handle to Journal_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Journal_text as text
%        str2double(get(hObject,'String')) returns contents of Journal_text as a double


% --- Executes during object creation, after setting all properties.
function Journal_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Journal_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Paper_text_Callback(hObject, eventdata, handles)
% hObject    handle to Paper_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Paper_text as text
%        str2double(get(hObject,'String')) returns contents of Paper_text as a double


% --- Executes during object creation, after setting all properties.
function Paper_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Paper_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Author_text_Callback(hObject, eventdata, handles)
% hObject    handle to Author_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Author_text as text
%        str2double(get(hObject,'String')) returns contents of Author_text as a double


% --- Executes during object creation, after setting all properties.
function Author_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Author_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Date_text_Callback(hObject, eventdata, handles)
% hObject    handle to Date_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Date_text as text
%        str2double(get(hObject,'String')) returns contents of Date_text as a double


% --- Executes during object creation, after setting all properties.
function Date_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Date_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Population_text_Callback(hObject, eventdata, handles)
% hObject    handle to Population_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Population_text as text
%        str2double(get(hObject,'String')) returns contents of Population_text as a double


% --- Executes during object creation, after setting all properties.
function Population_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Population_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Size_text_Callback(hObject, eventdata, handles)
% hObject    handle to Size_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Size_text as text
%        str2double(get(hObject,'String')) returns contents of Size_text as a double


% --- Executes during object creation, after setting all properties.
function Size_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Size_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
