

- This research is about Allele Frequency Quality Control (AFQC) System made Using General pattern of Population Genetics.

- The system is based on investigating  the hypothesis of global similarity features for more than 60 populations concerning 15 STR autosomal loci
  D3S1358, vWA, FGA, TH01, TPOX, CSF1PO, D5S818, D13S317, D7S820, D8S1179, D21S11, D18S51, D16S539, D2S1338 and D19S433.

- The populations datasets covered in our database formed the global allele frequency template which is considered as a reference for any new population dataset.

- To open the AFQC system Graphical User Interface (GUI), the " main_script.m " is run on matlab .

- When the main_Script is run, the Allele Frequency Trajectories (AFTs) global trends appear as a default window .

- The under test AFTs appear on the system GUI in a red bold line corresponding to the global AFTs of the 15 STRs loci which appear in colored thin lines. 

- There are two drop down menues, one for each tested counteries  and the other for under tested counteries.

- There are two toggle button, one from global trend to histogram and vice versa, and the other button toggles the global average AFT on the AFTs global trends and vice versa .

- The system can compare two or more populations.

- Descriptive statistical results appeared by pressing the Result button.


