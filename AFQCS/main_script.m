function varargout = main_script(varargin)
% MAIN_SCRIPT MATLAB code for main_script.fig
%      MAIN_SCRIPT, by itself, creates a new MAIN_SCRIPT or raises the existing
%      singleton*.
%
%      H = MAIN_SCRIPT returns the handle to a new MAIN_SCRIPT or the handle to
%      the existing singleton*.
%
%      MAIN_SCRIPT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_SCRIPT.M with the given input arguments.
%
%      MAIN_SCRIPT('Property','Value',...) creates a new MAIN_SCRIPT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_script_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_script_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main_script

% Last Modified by GUIDE v2.5 29-Jul-2017 14:39:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_script_OpeningFcn, ...
                   'gui_OutputFcn',  @main_script_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% --- Executes just before main_script is made visible.
function main_script_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_script (see VARARGIN)

% Choose default command line output for main_script
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main_script wait for user response (see UIRESUME)
% uiwait(handles.Main_GUI);

% set popup_menu values 
[a,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Tested Populations'},Allcountries(1:end)];
set(handles.popupMenu_TestedPopulations,'String', Allcountries);

% set popup_menu values (rejected)
[a,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Populations Under Test'},Allcountries(2:end)];
set(handles.Popupmenu_UnderTestPopulation,'String', Allcountries);

    % T1 is a title of application  and edit67 is a text box that contain the title
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
    set (handles.Main_Title,'String',T1);
    
    % set the variables to nan
    population_Name = nan ;
    setappdata (0,'population_Name' , population_Name);
    popupmenu4value = nan ;
    setappdata (0,'popupmenu4value',popupmenu4value);
    test_alleles = nan ;
    setappdata(0,'test_alleles',test_alleles);

        % reading  populations' data for specific locus to draw them on the 15 axeses
    %puting empty cells in the excel file to Zero
    trend_file = 'Excel_Files\Tested_populations.xlsx' ;
    reject_file = 'Excel_Files\Under_test_populations.xlsx' ;
    
    D3S1358_global =xlsread(trend_file,'D3S1358');
    D3S1358_global(isnan(D3S1358_global))=0;
    D3S1358_plot = [nan , nan] ;
    Rejected_D3S1358 =xlsread(reject_file,'D3S1358');
    setappdata(0,'D3S1358_plot',D3S1358_plot)
    setappdata(0,'D3S1358_global',D3S1358_global)
    setappdata(0,'Rejected_D3S1358',Rejected_D3S1358)

    vWA_global=xlsread(trend_file,'vWA');
    vWA_global(isnan(vWA_global))=0;
    vWA_plot =  [nan , nan]  ;
    Rejected_vWA =xlsread(reject_file,'vWA');
    setappdata(0,'vWA_plot',vWA_plot)
    setappdata(0,'vWA_global',vWA_global)
    setappdata(0,'Rejected_vWA',Rejected_vWA)


    FGA_global=xlsread(trend_file,'FGA');
    FGA_global(isnan(FGA_global))=0;
    FGA_plot =  [nan , nan]  ;
    Rejected_FGA =xlsread(reject_file,'FGA');
    setappdata(0,'FGA_plot',FGA_plot)
    setappdata(0,'FGA_global',FGA_global)
    setappdata(0,'Rejected_FGA',Rejected_FGA)
 
    THO1_global=xlsread(trend_file,'THO1');
    THO1_global(isnan(THO1_global))=0;
    THO1_plot =  [nan , nan]  ;
    Rejected_THO1 =xlsread(reject_file,'THO1');
    setappdata(0,'THO1_plot',THO1_plot)
    setappdata(0,'THO1_global',THO1_global)
    setappdata(0,'Rejected_THO1',Rejected_THO1)
   
    TPOX_global=xlsread(trend_file,'TPOX');
    TPOX_global(isnan(TPOX_global))=0;
    TPOX_plot =  [nan , nan]  ;
    Rejected_TPOX =xlsread(reject_file,'TPOX');
    setappdata(0,'TPOX_plot',TPOX_plot)
    setappdata(0,'TPOX_global',TPOX_global)
    setappdata(0,'Rejected_TPOX',Rejected_TPOX)
       
    CSF1PO_global=xlsread(trend_file,'CSF1PO');
    CSF1PO_global(isnan(CSF1PO_global))=0;
    CSF1PO_plot =  [nan , nan]  ;
    Rejected_CSF1PO =xlsread(reject_file,'CSF1PO');
    setappdata(0,'CSF1PO_plot',CSF1PO_plot)
    setappdata(0,'CSF1PO_global',CSF1PO_global)
    setappdata(0,'Rejected_CSF1PO',Rejected_CSF1PO)
      
    D5S818_global=xlsread(trend_file,'D5S818');
    D5S818_global(isnan(D5S818_global))=0;
    D5S818_plot =  [nan , nan]  ;
    Rejected_D5S818 =xlsread(reject_file,'D5S818');
    setappdata(0,'D5S818_plot',D5S818_plot)
    setappdata(0,'D5S818_global',D5S818_global)
    setappdata(0,'Rejected_D5S818',Rejected_D5S818)
      
    D13S317_global=xlsread(trend_file,'D13S317');
    D13S317_global(isnan(D13S317_global))=0;
    D13S317_plot =  [nan , nan]  ;
    Rejected_D13S317 =xlsread(reject_file,'D13S317');
    setappdata(0,'D13S317_plot',D13S317_plot)
    setappdata(0,'D13S317_global',D13S317_global)
    setappdata(0,'Rejected_D13S317',Rejected_D13S317)
     
    D7S820_global=xlsread(trend_file,'D7S820');
    D7S820_global(isnan(D7S820_global))=0;
    D7S820_plot =  [nan , nan]  ;
    Rejected_D7S820 =xlsread(reject_file,'D7S820');
    setappdata(0,'D7S820_plot',D7S820_plot)
    setappdata(0,'D7S820_global',D7S820_global)
    setappdata(0,'Rejected_D7S820',Rejected_D7S820)
    
    D8S1179_global=xlsread(trend_file,'D8S1179');
    D8S1179_global(isnan(D8S1179_global))=0;
    D8S1179_plot =  [nan , nan]  ;
    Rejected_D8S1179 =xlsread(reject_file,'D8S1179');
    setappdata(0,'D8S1179_plot',D8S1179_plot)
    setappdata(0,'D8S1179_global',D8S1179_global)
    setappdata(0,'Rejected_D8S1179',Rejected_D8S1179)
   
    D21S11_global=xlsread(trend_file,'D21S11');
    D21S11_global(isnan(D21S11_global))=0;
    D21S11_plot =  [nan , nan]  ;
    Rejected_D21S11 =xlsread(reject_file,'D21S11');
    setappdata(0,'D21S11_plot',D21S11_plot)
    setappdata(0,'D21S11_global',D21S11_global)
    setappdata(0,'Rejected_D21S11',Rejected_D21S11)
    
    D18S51_global=xlsread(trend_file,'D18S51');
    D18S51_global(isnan(D18S51_global))=0;
    D18S51_plot =  [nan , nan]  ;
    Rejected_D18S51 =xlsread(reject_file,'D18S51');
    setappdata(0,'D18S51_plot',D18S51_plot)
    setappdata(0,'D18S51_global',D18S51_global)
    setappdata(0,'Rejected_D18S51',Rejected_D18S51)
    
    D16S539_global=xlsread(trend_file,'D16S539');
    D16S539_global(isnan(D16S539_global))=0;
    D16S539_plot =  [nan , nan]  ;
    Rejected_D16S539 =xlsread(reject_file,'D16S539');
    setappdata(0,'D16S539_plot',D16S539_plot)
    setappdata(0,'D16S539_global',D16S539_global)
    setappdata(0,'Rejected_D16S539',Rejected_D16S539)
 
    D2S1338_global=xlsread(trend_file,'D2S1338');
    D2S1338_global(isnan(D2S1338_global))=0;
    D2S1338_plot =  [nan , nan]  ;
    Rejected_D2S1338 =xlsread(reject_file,'D2S1338');
    setappdata(0,'D2S1338_plot',D2S1338_plot)
    setappdata(0,'D2S1338_global',D2S1338_global)
    setappdata(0,'Rejected_D2S1338',Rejected_D2S1338)
  
    D19S433_global=xlsread(trend_file,'D19S433');
    D19S433_global(isnan(D19S433_global))=0;
    D19S433_plot =  [nan , nan]  ;
    Rejected_D19S433 =xlsread(reject_file,'D19S433');
    setappdata(0,'D19S433_plot',D19S433_plot)
    setappdata(0,'D19S433_global',D19S433_global)
    setappdata(0,'Rejected_D19S433',Rejected_D19S433)

    
    
    % histogram function take the matrix of populations' data for specific locus and return 
    % the number of countries contains in this locus and the gaussian
    % Matrix for this locus
D3S1358_global=D3S1358_global(:,2:end);
b=sum(D3S1358_global);
d=sum(D3S1358_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D3S1358_global(:,(b==1));
D3S1358_hist=c((d==1),:);
    [Country_num1,gaussianMatrixRoundD3,xbins1 ] = Histogram_And_Country_Numbers (D3S1358_hist);
    setappdata(0,'Country_num1',Country_num1);
    setappdata (0,'gaussianMatrixRoundD3',gaussianMatrixRoundD3);
    setappdata(0,'xbins1',xbins1);
  
vWA_global=vWA_global(:,2:end);
b=sum(vWA_global);
d=sum(vWA_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=vWA_global(:,(b==1));
vWA_hist=c((d==1),:);
[Country_num2,gaussianMatrixRoundvWA,xbins2 ] = Histogram_And_Country_Numbers (vWA_hist);
    setappdata(0,'Country_num2',Country_num2);
    setappdata (0,'gaussianMatrixRoundvWA',gaussianMatrixRoundvWA);
    setappdata(0,'xbins2',xbins2);

FGA_global=FGA_global(:,2:end);
b=sum(FGA_global);
d=sum(FGA_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=FGA_global(:,(b==1));
FGA_hist=c((d==1),:);
    [Country_num3,gaussianMatrixRoundFGA,xbins3 ] = Histogram_And_Country_Numbers (FGA_hist);
    setappdata(0,'Country_num3',Country_num3);
    setappdata (0,'gaussianMatrixRoundFGA',gaussianMatrixRoundFGA);
    setappdata(0,'xbins3',xbins3);

THO1_global=THO1_global(:,2:end);
b=sum(THO1_global);
d=sum(THO1_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=THO1_global(:,(b==1));
THO1_hist=c((d==1),:);
    [Country_num4,gaussianMatrixRoundTHO1 ,xbins4] = Histogram_And_Country_Numbers (THO1_hist);
    setappdata(0,'Country_num4',Country_num4);
    setappdata (0,'gaussianMatrixRoundTHO1',gaussianMatrixRoundTHO1);
    setappdata(0,'xbins4',xbins4);

TPOX_global=TPOX_global(:,2:end);
b=sum(TPOX_global);
d=sum(TPOX_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=TPOX_global(:,(b==1));
TPOX_hist=c((d==1),:);
    [Country_num5,gaussianMatrixRoundTPOX ,xbins5] = Histogram_And_Country_Numbers (TPOX_hist);
    setappdata(0,'Country_num5',Country_num5);
    setappdata (0,'gaussianMatrixRoundTPOX',gaussianMatrixRoundTPOX);
    setappdata(0,'xbins5',xbins5);

CSF1PO_global=CSF1PO_global(:,2:end);
b=sum(CSF1PO_global);
d=sum(CSF1PO_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=CSF1PO_global(:,(b==1));
CSF1PO_hist=c((d==1),:);
    [Country_num6,gaussianMatrixRoundCSF1PO ,xbins6] = Histogram_And_Country_Numbers (CSF1PO_hist);
    setappdata(0,'Country_num6',Country_num6);
    setappdata (0,'gaussianMatrixRoundCSF1PO',gaussianMatrixRoundCSF1PO);
    setappdata(0,'xbins6',xbins6);

D5S818_global=D5S818_global(:,2:end);
b=sum(D5S818_global);
d=sum(D5S818_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D5S818_global(:,(b==1));
D5S818_hist=c((d==1),:);
    [Country_num7 ,gaussianMatrixRoundD5S818,xbins7 ] = Histogram_And_Country_Numbers (D5S818_hist);
    setappdata(0,'Country_num7',Country_num7);
    setappdata (0,'gaussianMatrixRoundD5S818',gaussianMatrixRoundD5S818);
    setappdata(0,'xbins7',xbins7);

D13S317_global=D13S317_global(:,2:end);
b=sum(D13S317_global);
d=sum(D13S317_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D13S317_global(:,(b==1));
D13S317_hist=c((d==1),:);
    [Country_num8 ,gaussianMatrixRoundD13S317,xbins8 ] = Histogram_And_Country_Numbers (D13S317_hist);
    setappdata(0,'Country_num8',Country_num8);
    setappdata (0,'gaussianMatrixRoundD13S317',gaussianMatrixRoundD13S317);
    setappdata(0,'xbins8',xbins8);

    
D7S820_global=D7S820_global(:,2:end);
b=sum(D7S820_global);
d=sum(D7S820_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D7S820_global(:,(b==1));
D7S820_hist=c((d==1),:);
    [Country_num9 ,gaussianMatrixRoundD7S820 ,xbins9] = Histogram_And_Country_Numbers (D7S820_hist);
    setappdata(0,'Country_num9',Country_num9);
    setappdata (0,'gaussianMatrixRoundD7S820',gaussianMatrixRoundD7S820);
    setappdata(0,'xbins9',xbins9);

D8S1179_global=D8S1179_global(:,2:end);
b=sum(D8S1179_global);
d=sum(D8S1179_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D8S1179_global(:,(b==1));
D8S1179_hist=c((d==1),:);
    [Country_num10 ,gaussianMatrixRoundD8S1179,xbins10 ] = Histogram_And_Country_Numbers (D8S1179_hist);
    setappdata(0,'Country_num10',Country_num10);
    setappdata (0,'gaussianMatrixRoundD8S1179',gaussianMatrixRoundD8S1179);
    setappdata(0,'xbins10',xbins10);

D21S11_global=D21S11_global(:,2:end);
b=sum(D21S11_global);
d=sum(D21S11_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D21S11_global(:,(b==1));
D21S11_hist=c((d==1),:);
    [Country_num11 ,gaussianMatrixRoundD21S11 ,xbins11] = Histogram_And_Country_Numbers (D21S11_hist);
    setappdata(0,'Country_num11',Country_num11);
    setappdata (0,'gaussianMatrixRoundD21S11',gaussianMatrixRoundD21S11);
    setappdata(0,'xbins11',xbins11);

D18S51_global=D18S51_global(:,2:end);
b=sum(D18S51_global);
d=sum(D18S51_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D18S51_global(:,(b==1));
D18S51_hist=c((d==1),:);
    [Country_num12 ,gaussianMatrixRoundD18S51 ,xbins12] = Histogram_And_Country_Numbers (D18S51_hist);
    setappdata(0,'Country_num12',Country_num12);
    setappdata (0,'gaussianMatrixRoundD18S51',gaussianMatrixRoundD18S51);
    setappdata(0,'xbins12',xbins12);

D16S539_global=D16S539_global(:,2:end);
b=sum(D16S539_global);
d=sum(D16S539_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D16S539_global(:,(b==1));
D16S539_hist=c((d==1),:);
    [Country_num13 ,gaussianMatrixRoundD16S539,xbins13 ] = Histogram_And_Country_Numbers (D16S539_hist);
    setappdata(0,'Country_num13',Country_num13);
    setappdata (0,'gaussianMatrixRoundD16S539',gaussianMatrixRoundD16S539);
    setappdata(0,'xbins13',xbins13);

D2S1338_global=D2S1338_global(:,2:end);
b=sum(D2S1338_global);
d=sum(D2S1338_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D2S1338_global(:,(b==1));
D2S1338_hist=c((d==1),:);
    [Country_num14 ,gaussianMatrixRoundD2S1338 ,xbins14] = Histogram_And_Country_Numbers (D2S1338_hist);
    setappdata(0,'Country_num14',Country_num14);
    setappdata (0,'gaussianMatrixRoundD2S1338',gaussianMatrixRoundD2S1338);
    setappdata(0,'xbins14',xbins14);

D19S433_global=D19S433_global(:,2:end);
b=sum(D19S433_global);
d=sum(D19S433_global,2);
b(b~=0)=1;
d(d~=0)=1;
c=D19S433_global(:,(b==1));
D19S433_hist=c((d==1),:);
    [Country_num15 ,gaussianMatrixRoundD19S433,xbins15 ] = Histogram_And_Country_Numbers (D19S433_hist);
    setappdata(0,'Country_num15',Country_num15);
    setappdata (0,'gaussianMatrixRoundD19S433',gaussianMatrixRoundD19S433);
    setappdata(0,'xbins15',xbins15);



    axes(handles.axes1); %select axes 
    a=D3S1358_global(:,1);  % the names of allels
    s=[];  % empty nector to clear x-axes
    D3S1358_global=D3S1358_global(:,(2:end)); %plot all matrix except the allele column
    cla
    plot(D3S1358_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s); 
    
    axes(handles.axes2);
    a=vWA_global(:,1);
    s=[];
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global); 
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);   
    
    axes(handles.axes3);    
    a=FGA_global(:,1);
    s=[];
    FGA_global=FGA_global(:,(2:end)); 
    cla
    plot(FGA_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    
    axes(handles.axes4);    
    a=THO1_global(:,1);
    s=[];
    THO1_global=THO1_global(:,(2:end)); 
    cla
    plot(THO1_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
   
    
    axes(handles.axes5);
    a=TPOX_global(:,1);
    s=[];
    TPOX_global=TPOX_global(:,(2:end));
     cla
    plot(TPOX_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    
    axes(handles.axes6);
    a=CSF1PO_global(:,1);
    s=[];
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    a=D5S818_global(:,1);
    s=[];
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes8); 
    a=D13S317_global(:,1);
    s=[];
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes9);
    a=D7S820_global(:,1);
    s=[];
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes10);
    a=D8S1179_global(:,1);
    s=[];
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    a=D21S11_global(:,1);
    s=[];
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    
    axes(handles.axes12);
    a=D18S51_global(:,1);
    s=[];
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    a=D16S539_global(:,1);
    s=[];
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    a=D2S1338_global(:,1);
    s=[];
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    a=D19S433_global(:,1);
    s=[];
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);    
% --- Outputs from this function are returned to the command line.
function varargout = main_script_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function Main_Title_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
% --- Executes during object creation, after setting all properties.
function Main_Title_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Population_Information_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double
% --- Executes during object creation, after setting all properties.
function Population_Information_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popupMenu_TestedPopulations.
function popupMenu_TestedPopulations_Callback(hObject, eventdata, handles)
% hObject    handle to popupMenu_TestedPopulations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popupMenu_TestedPopulations contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupMenu_TestedPopulations
% get the string of pushued selection
contents = get(hObject,'String');
getappdata(0,'popupmenu4value');
popupmenu4value = contents{get(handles.popupMenu_TestedPopulations,'Value')};
population_Name = popupmenu4value;
setappdata(0,'population_Name',population_Name);

if ( strcmp(population_Name , 'Tested Populations') )
    D3S1358_plot=getappdata(0,'D3S1358_plot');
    vWA_plot=getappdata(0,'vWA_plot');
    FGA_plot=getappdata(0,'FGA_plot');
    THO1_plot=getappdata(0,'THO1_plot');
    TPOX_plot=getappdata(0,'TPOX_plot');
    CSF1PO_plot=getappdata(0,'CSF1PO_plot');
    D5S818_plot=getappdata(0,'D5S818_plot');
    D13S317_plot=getappdata(0,'D13S317_plot');
    D7S820_plot=getappdata(0,'D7S820_plot');
    D8S1179_plot=getappdata(0,'D8S1179_plot');
    D21S11_plot=getappdata(0,'D21S11_plot');
    D18S51_plot=getappdata(0,'D18S51_plot');
    D16S539_plot=getappdata(0,'D16S539_plot');
    D2S1338_plot=getappdata(0,'D2S1338_plot');
    D19S433_plot=getappdata(0,'D19S433_plot');
    test = [D3S1358_plot;vWA_plot;FGA_plot;THO1_plot;TPOX_plot;CSF1PO_plot;D5S818_plot;...
      D13S317_plot;D7S820_plot;D8S1179_plot;D21S11_plot;D18S51_plot;D16S539_plot;D2S1338_plot;D19S433_plot] ;
if (isnan(test))
% setting the title of screen
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
    set (handles.Main_Title,'String',T1);
% set information box to nan
    set (handles.Population_Information,'String','  ');
    population_Name = nan;
    setappdata(0,'population_Name',population_Name);

else
    D3S1358_global=getappdata(0,'D3S1358_global');
    vWA_global=getappdata(0,'vWA_global');
    FGA_global=getappdata(0,'FGA_global');
    THO1_global=getappdata(0,'THO1_global');
    TPOX_global=getappdata(0,'TPOX_global');
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    D5S818_global=getappdata(0,'D5S818_global');
    D13S317_global=getappdata(0,'D13S317_global');
    D7S820_global=getappdata(0,'D7S820_global');
    D8S1179_global=getappdata(0,'D8S1179_global');
    D21S11_global=getappdata(0,'D21S11_global');
    D18S51_global=getappdata(0,'D18S51_global');
    D16S539_global=getappdata(0,'D16S539_global');
    D2S1338_global=getappdata(0,'D2S1338_global');
    D19S433_global=getappdata(0,'D19S433_global');
    
    axes(handles.axes1);
    s=[];
    a=D3S1358_global(:,1);  % the names of allels
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s); 
    
    axes(handles.axes2);
    s=[];
    a=vWA_global(:,1);  % the names of allels
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);   
    
    axes(handles.axes3);    
    s=[];
    a=FGA_global(:,1);  % the names of allels
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes4);
    s=[];
    a=THO1_global(:,1);  % the names of allels
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);    
    
    axes(handles.axes5);
    s=[];
    a=TPOX_global(:,1);  % the names of allels
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes6);
    s=[];
    a=CSF1PO_global(:,1);  % the names of allels
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    s=[];
    a=D5S818_global(:,1);  % the names of allels
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes8); 
    s=[];
    a=D13S317_global(:,1);  % the names of allels
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes9);
    s=[];
    a=D7S820_global(:,1);  % the names of allels
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes10);
    s=[];
    a=D8S1179_global(:,1);  % the names of allels
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    s=[];
    a=D21S11_global(:,1);  % the names of allels
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes12);
    s=[];
    a=D18S51_global(:,1);  % the names of allels
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    s=[];
    a=D16S539_global(:,1);  % the names of allels
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    s=[];
    a=D2S1338_global(:,1);  % the names of allels
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    s=[];
    a=D19S433_global(:,1);  % the names of allels
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

%set all variable to nan to prevent any bugs
    population_Name = nan ;
    D3S1358_plot = [nan , nan] ;
    vWA_plot = [nan nan] ;
    FGA_plot = [nan nan] ;
    THO1_plot = [nan nan] ;
    TPOX_plot = [nan nan] ;
    CSF1PO_plot = [nan nan] ;
    D5S818_plot = [nan nan] ;
    D13S317_plot = [nan nan] ;
    D7S820_plot = [nan nan] ;
    D8S1179_plot = [nan nan] ;
    D21S11_plot = [nan nan] ;
    D18S51_plot = [nan nan] ;
    D16S539_plot = [nan nan] ;
    D2S1338_plot = [nan nan] ;
    D19S433_plot = [nan nan] ;
setappdata(0,'D3S1358_plot',D3S1358_plot);
setappdata(0,'vWA_plot',vWA_plot);
setappdata(0,'FGA_plot',FGA_plot);
setappdata(0,'THO1_plot',THO1_plot);
setappdata(0,'TPOX_plot',TPOX_plot);
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
setappdata(0,'D5S818_plot',D5S818_plot);
setappdata(0,'D13S317_plot',D13S317_plot);
setappdata(0,'D7S820_plot',D7S820_plot);
setappdata(0,'D8S1179_plot',D8S1179_plot);
setappdata(0,'D21S11_plot',D21S11_plot);
setappdata(0,'D18S51_plot',D18S51_plot);
setappdata(0,'D16S539_plot',D16S539_plot);
setappdata(0,'D2S1338_plot',D2S1338_plot);
setappdata(0,'D19S433_plot',D19S433_plot);
setappdata(0,'population_Name',population_Name);

% setting the title of screen
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
    set (handles.Main_Title,'String',T1);
% set information box to nan
    set (handles.Population_Information,'String','  ');

population_Name = nan;
setappdata(0,'population_Name',population_Name);

end

else
    T1 = ' Allele Frequency Trajectories vs Global Trends'; 
    T1 =  strcat (population_Name,' ',T1) ;
    set (handles.Main_Title,'String',T1);

% getting global trend and saved population  then plotting
[num,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;  
Allcountries =Allcountries(1,:);
Allcountries=[{'  '},Allcountries(1:end)];
b = (strcmp(Allcountries,population_Name));

D3S1358_global = getappdata(0,'D3S1358_global');
D3S1358_plot =  D3S1358_global(:,b);
D3S1358_global(isnan(D3S1358_global))=0;
D3S1358_plot(isnan(D3S1358_plot))=0; 

vWA_global = getappdata(0,'vWA_global');
vWA_plot =  vWA_global(:,b);
vWA_global(isnan(vWA_global))=0;
vWA_plot(isnan(vWA_plot))=0; 

FGA_global =getappdata(0,'FGA_global') ;
FGA_plot =  FGA_global(:,b);
FGA_global(isnan(FGA_global))=0;
FGA_plot(isnan(FGA_plot))=0; 

THO1_global = getappdata(0,'THO1_global') ;
THO1_plot =  THO1_global(:,b);
THO1_global(isnan(THO1_global))=0;
THO1_plot(isnan(THO1_plot))=0; 
   
TPOX_global = getappdata(0,'TPOX_global') ;
TPOX_plot =  TPOX_global(:,b);
TPOX_global(isnan(TPOX_global))=0;
TPOX_plot(isnan(TPOX_plot))=0; 
   
CSF1PO_global =getappdata(0,'CSF1PO_global') ;
CSF1PO_plot =  CSF1PO_global(:,b);
CSF1PO_global(isnan(CSF1PO_global))=0;
CSF1PO_plot(isnan(CSF1PO_plot))=0; 
   
D5S818_global = getappdata(0,'D5S818_global') ;
D5S818_plot =  D5S818_global(:,b);
D5S818_global(isnan(D5S818_global))=0;
D5S818_plot(isnan(D5S818_plot))=0; 
   
D13S317_global = getappdata(0,'D13S317_global') ;
D13S317_plot =  D13S317_global(:,b);
D13S317_global(isnan(D13S317_global))=0;
D13S317_plot(isnan(D13S317_plot))=0; 
   
D7S820_global = getappdata(0,'D7S820_global') ;
D7S820_plot =  D7S820_global(:,b);
D7S820_global(isnan(D7S820_global))=0;
D7S820_plot(isnan(D7S820_plot))=0; 
   
D8S1179_global = getappdata(0,'D8S1179_global');
D8S1179_plot =  D8S1179_global(:,b);
D8S1179_global(isnan(D8S1179_global))=0;
D8S1179_plot(isnan(D8S1179_plot))=0; 
   
D21S11_global = getappdata(0,'D21S11_global') ;
D21S11_plot =  D21S11_global(:,b);
D21S11_global(isnan(D21S11_global))=0;
D21S11_plot(isnan(D21S11_plot))=0; 
   
D18S51_global = getappdata(0,'D18S51_global') ;
D18S51_plot =  D18S51_global(:,b);
D18S51_global(isnan(D18S51_global))=0;
D18S51_plot(isnan(D18S51_plot))=0; 
   
D16S539_global =getappdata(0,'D16S539_global') ;
D16S539_plot =  D16S539_global(:,b);
D16S539_global(isnan(D16S539_global))=0;
D16S539_plot(isnan(D16S539_plot))=0; 
   
D2S1338_global =getappdata(0,'D2S1338_global') ;
D2S1338_plot =  D2S1338_global(:,b);
D2S1338_global(isnan(D2S1338_global))=0;
D2S1338_plot(isnan(D2S1338_plot))=0; 
   
D19S433_global = getappdata(0,'D19S433_global') ;
D19S433_plot =  D19S433_global(:,b);
D19S433_global(isnan(D19S433_global))=0;
D19S433_plot(isnan(D19S433_plot))=0; 
   
D3S1358_plot = [D3S1358_global(:,1), D3S1358_plot];
setappdata(0,'D3S1358_plot',D3S1358_plot);
vWA_plot = [vWA_global(:,1), vWA_plot];
setappdata(0,'vWA_plot',vWA_plot);
FGA_plot = [FGA_global(:,1), FGA_plot];
setappdata(0,'FGA_plot',FGA_plot);
THO1_plot = [THO1_global(:,1), THO1_plot];
setappdata(0,'THO1_plot',THO1_plot);
TPOX_plot = [TPOX_global(:,1), TPOX_plot];
setappdata(0,'TPOX_plot',TPOX_plot);
CSF1PO_plot = [CSF1PO_global(:,1) ,CSF1PO_plot];
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
D5S818_plot = [D5S818_global(:,1) ,D5S818_plot];
setappdata(0,'D5S818_plot',D5S818_plot);
D13S317_plot = [D13S317_global(:,1), D13S317_plot];
setappdata(0,'D13S317_plot',D13S317_plot);
D7S820_plot = [D7S820_global(:,1), D7S820_plot];
setappdata(0,'D7S820_plot',D7S820_plot);
D8S1179_plot = [D8S1179_global(:,1), D8S1179_plot];
setappdata(0,'D8S1179_plot',D8S1179_plot);
D21S11_plot = [D21S11_global(:,1), D21S11_plot];
setappdata(0,'D21S11_plot',D21S11_plot);
D18S51_plot = [D18S51_global(:,1), D18S51_plot];
setappdata(0,'D18S51_plot',D18S51_plot);
D16S539_plot = [D16S539_global(:,1), D16S539_plot];
setappdata(0,'D16S539_plot',D16S539_plot);
D2S1338_plot = [D2S1338_global(:,1), D2S1338_plot];
setappdata(0,'D2S1338_plot',D2S1338_plot);
D19S433_plot = [D19S433_global(:,1), D19S433_plot];
setappdata(0,'D19S433_plot',D19S433_plot);
   
%select axeses and plot global and population trend
    axes(handles.axes1);
    a=D3S1358_global(:,1);
    s=[];
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2) ,'r','LineWidth',2); 
   
    axes(handles.axes2);
    a=vWA_global(:,1);
    s=[];
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(vWA_plot(:,2) ,'r','LineWidth',2); 
   
    axes(handles.axes3); 
    a=FGA_global(:,1);
    s=[];
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(FGA_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes4);   
    a=THO1_global(:,1);
    s=[];
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(THO1_plot(:,2) ,'r','LineWidth',2); 
 
    axes(handles.axes5);
    a=TPOX_global(:,1);
    s=[];
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(TPOX_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes6)
    a=CSF1PO_global(:,1);
    s=[];
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(CSF1PO_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes7);
    a=D5S818_global(:,1);
    s=[];
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D5S818_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes8); 
    a=D13S317_global(:,1);
    s=[];
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D13S317_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes9);
    a=D7S820_global(:,1);
    s=[];
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D7S820_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes10);
    a=D8S1179_global(:,1);
    s=[];
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D8S1179_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes11);
    a=D21S11_global(:,1);
    s=[];
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D21S11_plot(:,2) ,'r','LineWidth',2); 
    
    axes(handles.axes12);
    a=D18S51_global(:,1);
    s=[];
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D18S51_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes13);
    a=D16S539_global(:,1);
    s=[];
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D16S539_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes14);
    a=D2S1338_global(:,1);
    s=[];
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D2S1338_plot(:,2) ,'r','LineWidth',2);
    
    axes(handles.axes15);
    a=D19S433_global(:,1);
    s=[];
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D19S433_plot(:,2) ,'r','LineWidth',2); 
  
%write info in textbox 
[num,Allcountries,all]  = xlsread('Excel_Files\Tested_populations.xlsx' , 'newpopulation_data1') ;
Allcountries = Allcountries(:,1);
pop_row = strcmp(population_Name,Allcountries);
info=all(:,2);
info=info(pop_row);
set(handles.Population_Information,'String',info);
end
% --- Executes during object creation, after setting all properties.
function popupMenu_TestedPopulations_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupMenu_TestedPopulations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in Popupmenu_UnderTestPopulation.
function Popupmenu_UnderTestPopulation_Callback(hObject, eventdata, handles)
% hObject    handle to Popupmenu_UnderTestPopulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns Popupmenu_UnderTestPopulation contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Popupmenu_UnderTestPopulation

% get the string of pushued selection
contents = get(hObject,'String');
getappdata(0,'popupmenu6value');
popupmenu6value = contents{get(handles.Popupmenu_UnderTestPopulation,'Value')};
population_Name = popupmenu6value;
setappdata(0,'population_Name',population_Name);

if ( strcmp(population_Name , 'Populations Under Test') )
    D3S1358_plot=getappdata(0,'D3S1358_plot');
    vWA_plot=getappdata(0,'vWA_plot');
    FGA_plot=getappdata(0,'FGA_plot');
    THO1_plot=getappdata(0,'THO1_plot');
    TPOX_plot=getappdata(0,'TPOX_plot');
    CSF1PO_plot=getappdata(0,'CSF1PO_plot');
    D5S818_plot=getappdata(0,'D5S818_plot');
    D13S317_plot=getappdata(0,'D13S317_plot');
    D7S820_plot=getappdata(0,'D7S820_plot');
    D8S1179_plot=getappdata(0,'D8S1179_plot');
    D21S11_plot=getappdata(0,'D21S11_plot');
    D18S51_plot=getappdata(0,'D18S51_plot');
    D16S539_plot=getappdata(0,'D16S539_plot');
    D2S1338_plot=getappdata(0,'D2S1338_plot');
    D19S433_plot=getappdata(0,'D19S433_plot');
    test = [D3S1358_plot;vWA_plot;FGA_plot;THO1_plot;TPOX_plot;CSF1PO_plot;D5S818_plot;...
      D13S317_plot;D7S820_plot;D8S1179_plot;D21S11_plot;D18S51_plot;D16S539_plot;D2S1338_plot;D19S433_plot] ;
if (isnan(test))
% setting the title of screen
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
    set (handles.Main_Title,'String',T1);
% set information box to nan
    set (handles.Population_Information,'String','  ');
population_Name = nan;
setappdata(0,'population_Name',population_Name);
else
    D3S1358_global=getappdata(0,'D3S1358_global');
    vWA_global=getappdata(0,'vWA_global');
    FGA_global=getappdata(0,'FGA_global');
    THO1_global=getappdata(0,'THO1_global');
    TPOX_global=getappdata(0,'TPOX_global');
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    D5S818_global=getappdata(0,'D5S818_global');
    D13S317_global=getappdata(0,'D13S317_global');
    D7S820_global=getappdata(0,'D7S820_global');
    D8S1179_global=getappdata(0,'D8S1179_global');
    D21S11_global=getappdata(0,'D21S11_global');
    D18S51_global=getappdata(0,'D18S51_global');
    D16S539_global=getappdata(0,'D16S539_global');
    D2S1338_global=getappdata(0,'D2S1338_global');
    D19S433_global=getappdata(0,'D19S433_global');
    
    axes(handles.axes1);
    s=[];
    a=D3S1358_global(:,1);  % the names of allels
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s); 
    
    axes(handles.axes2);
    s=[];
    a=vWA_global(:,1);  % the names of allels
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);   
    
    axes(handles.axes3);    
    s=[];
    a=FGA_global(:,1);  % the names of allels
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes4);
    s=[];
    a=THO1_global(:,1);  % the names of allels
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);    
    
    axes(handles.axes5);
    s=[];
    a=TPOX_global(:,1);  % the names of allels
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes6);
    s=[];
    a=CSF1PO_global(:,1);  % the names of allels
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    s=[];
    a=D5S818_global(:,1);  % the names of allels
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes8); 
    s=[];
    a=D13S317_global(:,1);  % the names of allels
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes9);
    s=[];
    a=D7S820_global(:,1);  % the names of allels
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes10);
    s=[];
    a=D8S1179_global(:,1);  % the names of allels
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    s=[];
    a=D21S11_global(:,1);  % the names of allels
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes12);
    s=[];
    a=D18S51_global(:,1);  % the names of allels
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    s=[];
    a=D16S539_global(:,1);  % the names of allels
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    s=[];
    a=D2S1338_global(:,1);  % the names of allels
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    s=[];
    a=D19S433_global(:,1);  % the names of allels
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

%set all variable to nan to prevent any bugs
    population_Name = nan ;
    D3S1358_plot = [nan , nan] ;
    vWA_plot = [nan nan] ;
    FGA_plot = [nan nan] ;
    THO1_plot = [nan nan] ;
    TPOX_plot = [nan nan] ;
    CSF1PO_plot = [nan nan] ;
    D5S818_plot = [nan nan] ;
    D13S317_plot = [nan nan] ;
    D7S820_plot = [nan nan] ;
    D8S1179_plot = [nan nan] ;
    D21S11_plot = [nan nan] ;
    D18S51_plot = [nan nan] ;
    D16S539_plot = [nan nan] ;
    D2S1338_plot = [nan nan] ;
    D19S433_plot = [nan nan] ;
setappdata(0,'D3S1358_plot',D3S1358_plot);
setappdata(0,'vWA_plot',vWA_plot);
setappdata(0,'FGA_plot',FGA_plot);
setappdata(0,'THO1_plot',THO1_plot);
setappdata(0,'TPOX_plot',TPOX_plot);
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
setappdata(0,'D5S818_plot',D5S818_plot);
setappdata(0,'D13S317_plot',D13S317_plot);
setappdata(0,'D7S820_plot',D7S820_plot);
setappdata(0,'D8S1179_plot',D8S1179_plot);
setappdata(0,'D21S11_plot',D21S11_plot);
setappdata(0,'D18S51_plot',D18S51_plot);
setappdata(0,'D16S539_plot',D16S539_plot);
setappdata(0,'D2S1338_plot',D2S1338_plot);
setappdata(0,'D19S433_plot',D19S433_plot);
setappdata(0,'population_Name',population_Name);

% set information box to nan
    set (handles.Population_Information,'String','  ');
% setting the title of screen
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
    set (handles.Main_Title,'String',T1);
end
else
% set the title of screen
    T1 = ' Allele Frequency Trajectories vs Global Trends'; 
    T1 =  strcat (population_Name,T1) ;
    set (handles.Main_Title,'String',T1);

% getting global trend and saved population  then plotting
[num,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;  
Allcountries =Allcountries(1,:);
b = (strcmp(Allcountries,population_Name));
D3S1358_rej=xlsread('Excel_Files\Under_test_populations','D3S1358');
D3S1358_plot =  D3S1358_rej(:,b);
D3S1358_rej(isnan(D3S1358_rej))=0;
D3S1358_plot(isnan(D3S1358_plot))=0; 

vWA_rej=xlsread('Excel_Files\Under_test_populations','vWA');
vWA_plot =  vWA_rej(:,b);
vWA_rej(isnan(vWA_rej))=0;
vWA_plot(isnan(vWA_plot))=0; 

FGA_rej=xlsread('Excel_Files\Under_test_populations','FGA');
FGA_plot =  FGA_rej(:,b);
FGA_rej(isnan(FGA_rej))=0;
FGA_plot(isnan(FGA_plot))=0; 

THO1_rej=xlsread('Excel_Files\Under_test_populations','THO1');
THO1_plot =  THO1_rej(:,b);
THO1_rej(isnan(THO1_rej))=0;
THO1_plot(isnan(THO1_plot))=0; 
   
TPOX_rej=xlsread('Excel_Files\Under_test_populations','TPOX');
TPOX_plot =  TPOX_rej(:,b);
TPOX_rej(isnan(TPOX_rej))=0;
TPOX_plot(isnan(TPOX_plot))=0; 
   
CSF1PO_rej=xlsread('Excel_Files\Under_test_populations','CSF1PO');
CSF1PO_plot =  CSF1PO_rej(:,b);
CSF1PO_rej(isnan(CSF1PO_rej))=0;
CSF1PO_plot(isnan(CSF1PO_plot))=0; 
   
D5S818_rej=xlsread('Excel_Files\Under_test_populations','D5S818');
D5S818_plot =  D5S818_rej(:,b);
D5S818_rej(isnan(D5S818_rej))=0;
D5S818_plot(isnan(D5S818_plot))=0; 
   
D13S317_rej=xlsread('Excel_Files\Under_test_populations','D13S317');
D13S317_plot =  D13S317_rej(:,b);
D13S317_rej(isnan(D13S317_rej))=0;
D13S317_plot(isnan(D13S317_plot))=0; 
   
D7S820_rej=xlsread('Excel_Files\Under_test_populations','D7S820');
D7S820_plot =  D7S820_rej(:,b);
D7S820_rej(isnan(D7S820_rej))=0;
D7S820_plot(isnan(D7S820_plot))=0; 
   
D8S1179_rej=xlsread('Excel_Files\Under_test_populations','D8S1179');
D8S1179_plot =  D8S1179_rej(:,b);
D8S1179_rej(isnan(D8S1179_rej))=0;
D8S1179_plot(isnan(D8S1179_plot))=0; 
   
D21S11_rej=xlsread('Excel_Files\Under_test_populations','D21S11');
D21S11_plot =  D21S11_rej(:,b);
D21S11_rej(isnan(D21S11_rej))=0;
D21S11_plot(isnan(D21S11_plot))=0; 
   
D18S51_rej=xlsread('Excel_Files\Under_test_populations','D18S51');
D18S51_plot =  D18S51_rej(:,b);
D18S51_rej(isnan(D18S51_rej))=0;
D18S51_plot(isnan(D18S51_plot))=0; 
   
D16S539_rej=xlsread('Excel_Files\Under_test_populations','D16S539');
D16S539_plot =  D16S539_rej(:,b);
D16S539_rej(isnan(D16S539_rej))=0;
D16S539_plot(isnan(D16S539_plot))=0; 
   
D2S1338_rej=xlsread('Excel_Files\Under_test_populations','D2S1338');
D2S1338_plot =  D2S1338_rej(:,b);
D2S1338_rej(isnan(D2S1338_rej))=0;
D2S1338_plot(isnan(D2S1338_plot))=0; 
   
D19S433_rej=xlsread('Excel_Files\Under_test_populations','D19S433');
D19S433_plot =  D19S433_rej(:,b);
D19S433_rej(isnan(D19S433_rej))=0;
D19S433_plot(isnan(D19S433_plot))=0; 
   
D3S1358_plot = [D3S1358_rej(:,1), D3S1358_plot];
setappdata(0,'D3S1358_plot',D3S1358_plot);
vWA_plot = [vWA_rej(:,1), vWA_plot];
setappdata(0,'vWA_plot',vWA_plot);
FGA_plot = [FGA_rej(:,1), FGA_plot];
setappdata(0,'FGA_plot',FGA_plot);
THO1_plot = [THO1_rej(:,1), THO1_plot];
setappdata(0,'THO1_plot',THO1_plot);
TPOX_plot = [TPOX_rej(:,1), TPOX_plot];
setappdata(0,'TPOX_plot',TPOX_plot);
CSF1PO_plot = [CSF1PO_rej(:,1) ,CSF1PO_plot];
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
D5S818_plot = [D5S818_rej(:,1) ,D5S818_plot];
setappdata(0,'D5S818_plot',D5S818_plot);
D13S317_plot = [D13S317_rej(:,1), D13S317_plot];
setappdata(0,'D13S317_plot',D13S317_plot);
D7S820_plot = [D7S820_rej(:,1), D7S820_plot];
setappdata(0,'D7S820_plot',D7S820_plot);
D8S1179_plot = [D8S1179_rej(:,1), D8S1179_plot];
setappdata(0,'D8S1179_plot',D8S1179_plot);
D21S11_plot = [D21S11_rej(:,1), D21S11_plot];
setappdata(0,'D21S11_plot',D21S11_plot);
D18S51_plot = [D18S51_rej(:,1), D18S51_plot];
setappdata(0,'D18S51_plot',D18S51_plot);
D16S539_plot = [D16S539_rej(:,1), D16S539_plot];
setappdata(0,'D16S539_plot',D16S539_plot);
D2S1338_plot = [D2S1338_rej(:,1), D2S1338_plot];
setappdata(0,'D2S1338_plot',D2S1338_plot);
D19S433_plot = [D19S433_rej(:,1), D19S433_plot];
setappdata(0,'D19S433_plot',D19S433_plot);
   
%select axeses and plot global and population trend
    
    D3S1358_global=getappdata(0,'D3S1358_global');
    vWA_global=getappdata(0,'vWA_global');
    FGA_global=getappdata(0,'FGA_global');
    THO1_global=getappdata(0,'THO1_global');
    TPOX_global=getappdata(0,'TPOX_global');
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    D5S818_global=getappdata(0,'D5S818_global');
    D13S317_global=getappdata(0,'D13S317_global');
    D7S820_global=getappdata(0,'D7S820_global');
    D8S1179_global=getappdata(0,'D8S1179_global');
    D21S11_global=getappdata(0,'D21S11_global');
    D18S51_global=getappdata(0,'D18S51_global');
    D16S539_global=getappdata(0,'D16S539_global');
    D2S1338_global=getappdata(0,'D2S1338_global');
    D19S433_global=getappdata(0,'D19S433_global');

    axes(handles.axes1);
    a=D3S1358_global(:,1);
    s=[];
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2) ,'r','LineWidth',2); 
   
    axes(handles.axes2);
    a=vWA_global(:,1);
    s=[];
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(vWA_plot(:,2) ,'r','LineWidth',2); 
   
    axes(handles.axes3); 
    a=FGA_global(:,1);
    s=[];
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(FGA_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes4);   
    a=THO1_global(:,1);
    s=[];
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(THO1_plot(:,2) ,'r','LineWidth',2); 
 
    axes(handles.axes5);
    a=TPOX_global(:,1);
    s=[];
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(TPOX_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes6)
    a=CSF1PO_global(:,1);
    s=[];
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(CSF1PO_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes7);
    a=D5S818_global(:,1);
    s=[];
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D5S818_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes8); 
    a=D13S317_global(:,1);
    s=[];
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D13S317_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes9);
    a=D7S820_global(:,1);
    s=[];
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D7S820_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes10);
    a=D8S1179_global(:,1);
    s=[];
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D8S1179_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes11);
    a=D21S11_global(:,1);
    s=[];
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D21S11_plot(:,2) ,'r','LineWidth',2); 
    
    axes(handles.axes12);
    a=D18S51_global(:,1);
    s=[];
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D18S51_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes13);
    a=D16S539_global(:,1);
    s=[];
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D16S539_plot(:,2) ,'r','LineWidth',2); 

    axes(handles.axes14);
    a=D2S1338_global(:,1);
    s=[];
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D2S1338_plot(:,2) ,'r','LineWidth',2);
    
    axes(handles.axes15);
    a=D19S433_global(:,1);
    s=[];
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D19S433_plot(:,2) ,'r','LineWidth',2); 

  
%write info in textbox 
[num,Allcountries,all]  = xlsread('Excel_Files\Under_test_populations.xlsx' , 'newpopulation_data1') ;
Allcountries = Allcountries(:,1);
pop_row = strcmp(population_Name,Allcountries);
info=all(:,2);
info=info(pop_row);
set(handles.Population_Information,'String',info);

Jornal=all(:,3);
Jornal=Jornal(pop_row);
Paper=all(:,4);
Paper=Paper(pop_row);
Author=all(:,5);
Author=Author(pop_row);
Date=all(:,7);
Date=Date(pop_row);
Pop_name=all(:,6);
Pop_name=Pop_name(pop_row);
Sample_size=all(:,8);
Sample_size=Sample_size(pop_row);
all_info = {Jornal{1};Paper{1};Author{1};Date{1};Pop_name{1};Sample_size{1}};
setappdata(0,'all_info',all_info);
end
% --- Executes during object creation, after setting all properties.
function Popupmenu_UnderTestPopulation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popupmenu_UnderTestPopulation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

    % histogram button
function Histogram_button_Callback(hObject, eventdata, handles)
% hObject    handle to Histogram_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Histogram_button

a= get(hObject,'Value') ;
if a==1

    %seting title of screen
    T1 = 'Correlation Coefficients Probability Histograms ' ;  
    set (handles.Main_Title,'String',T1);
   
   %select axes ,, clear it ,, draw histogram 
    s=[];
    axes(handles.axes1);
    cla 
    xbins1 = getappdata(0,'xbins1');
    gaussianMatrixRoundD3=getappdata(0,'gaussianMatrixRoundD3');
    [n,cent]=hist(gaussianMatrixRoundD3,xbins1);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);


    axes(handles.axes2);
    cla
    xbins2 = getappdata(0,'xbins2');
    gaussianMatrixRoundvWA=getappdata(0,'gaussianMatrixRoundvWA');
    [n,cent]=hist(gaussianMatrixRoundvWA,xbins2);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes3);
    cla
    xbins3 = getappdata(0,'xbins3');
    gaussianMatrixRoundFGA=getappdata(0,'gaussianMatrixRoundFGA');
    [n,cent]= hist(gaussianMatrixRoundFGA,xbins3);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);
 
    axes(handles.axes4);
    cla
    xbins4 = getappdata(0,'xbins4');
    gaussianMatrixRoundTHO1=getappdata(0,'gaussianMatrixRoundTHO1');
    [n,cent]=hist(gaussianMatrixRoundTHO1,xbins4);   
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);
  
    axes(handles.axes5);
    cla
    xbins5 = getappdata(0,'xbins5');
    gaussianMatrixRoundTPOX=getappdata(0,'gaussianMatrixRoundTPOX');
    hist(gaussianMatrixRoundTPOX,size(xbins5,2))
    set(gca,'XTickLabel',s);


    axes(handles.axes6);
    cla
    xbins6 = getappdata(0,'xbins6');
    gaussianMatrixRoundCSF1PO=getappdata(0,'gaussianMatrixRoundCSF1PO');
    [n,cent]=hist(gaussianMatrixRoundCSF1PO,xbins6);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    cla
    xbins7 = getappdata(0,'xbins7');
    gaussianMatrixRoundD5S818=getappdata(0,'gaussianMatrixRoundD5S818');
    [n,cent]=hist(gaussianMatrixRoundD5S818,xbins7);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes8);
    cla
    xbins8 = getappdata(0,'xbins8');
    gaussianMatrixRoundD13S317=getappdata(0,'gaussianMatrixRoundD13S317');
    [n,cent]=hist(gaussianMatrixRoundD13S317,xbins8);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);


    axes(handles.axes9);
    cla
    xbins9 = getappdata(0,'xbins9');
    gaussianMatrixRoundD7S820=getappdata(0,'gaussianMatrixRoundD7S820');
    [n,cent]=hist(gaussianMatrixRoundD7S820,xbins9);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);
     
    axes(handles.axes10);
    cla
    xbins10 = getappdata(0,'xbins10');
    gaussianMatrixRoundD8S1179=getappdata(0,'gaussianMatrixRoundD8S1179');
    [n,cent]=hist(gaussianMatrixRoundD8S1179,xbins10);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    cla 
    xbins11 = getappdata(0,'xbins11');
    gaussianMatrixRoundD21S11=getappdata(0,'gaussianMatrixRoundD21S11');
    [n,cent]=hist(gaussianMatrixRoundD21S11,xbins11);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes12);
    cla
    xbins12 = getappdata(0,'xbins12');
    gaussianMatrixRoundD18S51=getappdata(0,'gaussianMatrixRoundD18S51');
    [n,cent]=hist(gaussianMatrixRoundD18S51,xbins12);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    cla
    xbins13 = getappdata(0,'xbins13');
    gaussianMatrixRoundD16S539=getappdata(0,'gaussianMatrixRoundD16S539');
    [n,cent]=hist(gaussianMatrixRoundD16S539,xbins13);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    cla   
    xbins14 = getappdata(0,'xbins14');
    gaussianMatrixRoundD2S1338=getappdata(0,'gaussianMatrixRoundD2S1338');
    [n,cent]=hist(gaussianMatrixRoundD2S1338,xbins14) ;  
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    cla
    xbins15 = getappdata(0,'xbins15');
    gaussianMatrixRoundD19S433=getappdata(0,'gaussianMatrixRoundD19S433');
    [n,cent]=hist(gaussianMatrixRoundD19S433,xbins15);
    bar(cent,n);%draw the histogram
    set(gca,'XTickLabel',s);

    set (handles.Histogram_button,'String','AFTs Global Trends');

else
        %pushing button for next time
    % reterive application to openning screen
    %getting populations' and test data for ploting
        D3S1358_global=getappdata(0,'D3S1358_global');
        D3S1358_plot=getappdata(0,'D3S1358_plot');
        vWA_global=getappdata(0,'vWA_global');
        vWA_plot=getappdata(0,'vWA_plot');
        FGA_global=getappdata(0,'FGA_global');
        FGA_plot=getappdata(0,'FGA_plot');
        THO1_global=getappdata(0,'THO1_global');
        THO1_plot=getappdata(0,'THO1_plot');
        TPOX_global=getappdata(0,'TPOX_global');
        TPOX_plot=getappdata(0,'TPOX_plot');
        CSF1PO_global=getappdata(0,'CSF1PO_global');
        CSF1PO_plot=getappdata(0,'CSF1PO_plot');
        D5S818_global=getappdata(0,'D5S818_global');
        D5S818_plot=getappdata(0,'D5S818_plot');
        D13S317_global=getappdata(0,'D13S317_global');
        D13S317_plot=getappdata(0,'D13S317_plot');
        D7S820_global=getappdata(0,'D7S820_global');
        D7S820_plot=getappdata(0,'D7S820_plot');
        D8S1179_global=getappdata(0,'D8S1179_global');
        D8S1179_plot=getappdata(0,'D8S1179_plot');
        D21S11_global=getappdata(0,'D21S11_global');
        D21S11_plot=getappdata(0,'D21S11_plot');
        D18S51_global=getappdata(0,'D18S51_global');
        D18S51_plot=getappdata(0,'D18S51_plot');
        D16S539_global=getappdata(0,'D16S539_global');
        D16S539_plot=getappdata(0,'D16S539_plot');
        D19S433_global=getappdata(0,'D19S433_global');
        D19S433_plot=getappdata(0,'D19S433_plot');
        D2S1338_global=getappdata(0,'D2S1338_global');
        D2S1338_plot=getappdata(0,'D2S1338_plot');
    
    %select axes the plot global trend and tested population
    axes(handles.axes1);
    a=D3S1358_global(:,1);
    s=[];
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes2);
   a=vWA_global(:,1);
   s=[];
   vWA_global=vWA_global(:,(2:end));
   cla
   plot(vWA_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(vWA_plot(:,2) ,'r','LineWidth',2); 
   
   axes(handles.axes3); 
   a=FGA_global(:,1);
   s=[];
   FGA_global=FGA_global(:,(2:end));
   cla
   plot(FGA_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(FGA_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes4);   
   a=THO1_global(:,1);
   s=[];
   THO1_global=THO1_global(:,(2:end));
   cla
   plot(THO1_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(THO1_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes5);
   a=TPOX_global(:,1);
   s=[];
   TPOX_global=TPOX_global(:,(2:end));
   cla
   plot(TPOX_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(TPOX_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes6);
   a=CSF1PO_global(:,1);
   s=[];
   CSF1PO_global=CSF1PO_global(:,(2:end));
   cla
   plot(CSF1PO_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(CSF1PO_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes7);
   a=D5S818_global(:,1);
   s=[];
   D5S818_global=D5S818_global(:,(2:end));
   cla
   plot(D5S818_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D5S818_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes8); 
   a=D13S317_global(:,1);
   s=[];
   D13S317_global=D13S317_global(:,(2:end));
   cla
   plot(D13S317_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D13S317_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes9);
   a=D7S820_global(:,1);
   s=[];
   D7S820_global=D7S820_global(:,(2:end));
   cla
   plot(D7S820_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D7S820_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes10);
   a=D8S1179_global(:,1);
   s=[];
   D8S1179_global=D8S1179_global(:,(2:end));
   cla
   plot(D8S1179_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D8S1179_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes11);
   a=D21S11_global(:,1);
   s=[];
   D21S11_global=D21S11_global(:,(2:end));
   cla
   plot(D21S11_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D21S11_plot(:,2) ,'r','LineWidth',2); 
    
   axes(handles.axes12);
   a=D18S51_global(:,1);
   s=[];
   D18S51_global=D18S51_global(:,(2:end));
   cla
   plot(D18S51_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D18S51_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes13);
   a=D16S539_global(:,1);
   s=[];
   D16S539_global=D16S539_global(:,(2:end));
   cla
   plot(D16S539_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D16S539_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes14);
   a=D2S1338_global(:,1);
   s=[];
   D2S1338_global=D2S1338_global(:,(2:end));
   cla
   plot(D2S1338_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D2S1338_plot(:,2) ,'r','LineWidth',2); 

   axes(handles.axes15);
   a=D19S433_global(:,1);
   s=[];
   D19S433_global=D19S433_global(:,(2:end));
   cla
   plot(D19S433_global);hold on
   set(gca,'XTick',[1:size(a,1)]);
   set(gca,'XTickLabel',s);
   plot(D19S433_plot(:,2) ,'r','LineWidth',2); 

% setting the title of screen
     population_Name = getappdata(0,'population_Name');
    if (isnan(population_Name) )
     T1 = 'Allele Frequency Trajectories Global Trends' ;  
     set (handles.Main_Title,'String',T1);
    else
    T1 = ' Allele Frequency Trajectories vs Global Trends'; 
    T1 =  strcat (population_Name,' ',T1) ;
    set (handles.Main_Title,'String',T1);
    end

    set (handles.Histogram_button,'String','Histogram');
end

    % load new data button
function Load_button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% clear text box which showed the information of tested/saved data
set(handles.Population_Information,'String','  ');

%open the window of adding info and test file
get (Load_New_Data);

%we want hold program until enter info and load test file
uiwait % pause program until uiresume

%reading population name
    [v,a] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data2','F1');
    population_Name = a{1} ; % reading string value of excel cell
    setappdata(0,'population_Name',population_Name); % set pop.name to population_Name variable
    %reading tested data from database
    test = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation');
    %saving data with pop. name to easily restore them in saving operation
   
    %getting populations' data and test data for specific locus to
    %preprocessing before ploting then set them to using in another
    %sections of the program
D3S1358_global = getappdata(0,'D3S1358_global');
vWA_global = getappdata(0,'vWA_global');
FGA_global = getappdata(0,'FGA_global');
THO1_global = getappdata(0,'THO1_global');
TPOX_global = getappdata(0,'TPOX_global');
CSF1PO_global = getappdata(0,'CSF1PO_global');
D5S818_global = getappdata(0,'D5S818_global');
D13S317_global = getappdata(0,'D13S317_global');
D7S820_global = getappdata(0,'D7S820_global');
D8S1179_global = getappdata(0,'D8S1179_global');
D21S11_global = getappdata(0,'D21S11_global');
D18S51_global = getappdata(0,'D18S51_global');
D16S539_global = getappdata(0,'D16S539_global');
D2S1338_global = getappdata(0,'D2S1338_global');
D19S433_global = getappdata(0,'D19S433_global');

Rejected_D3S1358 = getappdata(0,'Rejected_D3S1358');
Rejected_vWA = getappdata(0,'Rejected_vWA');
Rejected_FGA = getappdata(0,'Rejected_FGA');
Rejected_THO1 = getappdata(0,'Rejected_THO1');
Rejected_TPOX = getappdata(0,'Rejected_TPOX');
Rejected_CSF1PO = getappdata(0,'Rejected_CSF1PO');
Rejected_D5S818 = getappdata(0,'Rejected_D5S818');
Rejected_D13S317 = getappdata(0,'Rejected_D13S317');
Rejected_D7S820 = getappdata(0,'Rejected_D7S820');
Rejected_D8S1179 = getappdata(0,'Rejected_D8S1179');
Rejected_D21S11 = getappdata(0,'Rejected_D21S11');
Rejected_D18S51 = getappdata(0,'Rejected_D18S51');
Rejected_D16S539 = getappdata(0,'Rejected_D16S539');
Rejected_D2S1338 = getappdata(0,'Rejected_D2S1338');
Rejected_D19S433 = getappdata(0,'Rejected_D19S433');

  D3S1358test=test(:,(1:2));
[ D3S1358_plot ,D3S1358_global ,Rejected_D3S1358 ] = Edit_Test_Data( D3S1358test ,D3S1358_global,Rejected_D3S1358  ) ;
   setappdata(0,'D3S1358_plot',D3S1358_plot);
   setappdata(0,'D3S1358_global',D3S1358_global);
   setappdata(0,'Rejected_D3S1358',Rejected_D3S1358);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D3S1358,'D3S1358','A2');

  vWAtest= [test(:,1),test(:,3)];
[ vWA_plot ,vWA_global ,Rejected_vWA ] = Edit_Test_Data( vWAtest ,vWA_global ,Rejected_vWA ) ;
   setappdata(0,'vWA_plot',vWA_plot);
   setappdata(0,'vWA_global',vWA_global);
   setappdata(0,'Rejected_vWA',Rejected_vWA);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_vWA,'vWA','A2');

   
  FGAtest=[test(:,1),test(:,4)];
[ FGA_plot ,FGA_global ,Rejected_FGA ] = Edit_Test_Data( FGAtest ,FGA_global ,Rejected_FGA ) ;
   setappdata(0,'FGA_plot',FGA_plot);
   setappdata(0,'FGA_global',FGA_global);
   setappdata(0,'Rejected_FGA',Rejected_FGA);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_FGA,'FGA','A2');

  THO1test=[test(:,1),test(:,5)];
[ THO1_plot ,THO1_global ,Rejected_THO1] = Edit_Test_Data( THO1test ,THO1_global ,Rejected_THO1 ) ;
   setappdata(0,'THO1_plot',THO1_plot);
   setappdata(0,'THO1_global',THO1_global);
   setappdata(0,'Rejected_THO1',Rejected_THO1);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_THO1,'THO1','A2');

  TPOXtest=[test(:,1),test(:,6)];
[ TPOX_plot ,TPOX_global ,Rejected_TPOX ] = Edit_Test_Data( TPOXtest ,TPOX_global ,Rejected_TPOX  ) ;
   setappdata(0,'TPOX_plot',TPOX_plot);
   setappdata(0,'TPOX_global',TPOX_global);
   setappdata(0,'Rejected_TPOX',Rejected_TPOX);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_TPOX,'TPOX','A2');
   
  CSF1POtest=[test(:,1),test(:,7)];
[ CSF1PO_plot ,CSF1PO_global , Rejected_CSF1PO] = Edit_Test_Data( CSF1POtest ,CSF1PO_global ,Rejected_CSF1PO  ) ;
   setappdata(0,'CSF1PO_plot',CSF1PO_plot);
   setappdata(0,'CSF1PO_global',CSF1PO_global);
   setappdata(0,'Rejected_CSF1PO',Rejected_CSF1PO);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_CSF1PO,'CSF1PO','A2');
   
  D5S818test=[test(:,1),test(:,8)];
[ D5S818_plot ,D5S818_global ,Rejected_D5S818 ] = Edit_Test_Data( D5S818test ,D5S818_global ,Rejected_D5S818 ) ;
   setappdata(0,'D5S818_plot',D5S818_plot);
   setappdata(0,'D5S818_global',D5S818_global);
   setappdata(0,'Rejected_D5S818',Rejected_D5S818);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D5S818,'D5S818','A2');
   
  D13S317test=[test(:,1),test(:,9)];
[ D13S317_plot ,D13S317_global ,Rejected_D13S317 ] = Edit_Test_Data( D13S317test ,D13S317_global ,Rejected_D13S317 ) ;
    setappdata(0,'D13S317_plot',D13S317_plot);
   setappdata(0,'D13S317_global',D13S317_global);
   setappdata(0,'Rejected_D13S317',Rejected_D13S317);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D13S317,'D13S317','A2');
   
  D7S820test=[test(:,1),test(:,10)];
[ D7S820_plot ,D7S820_global ,Rejected_D7S820] = Edit_Test_Data( D7S820test ,D7S820_global ,Rejected_D7S820 ) ;
    setappdata(0,'D7S820_plot',D7S820_plot);
   setappdata(0,'D7S820_global',D7S820_global);
   setappdata(0,'Rejected_D7S820',Rejected_D7S820);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D7S820,'D7S820','A2');
   
  D8S1179test=[test(:,1),test(:,11)];
[ D8S1179_plot ,D8S1179_global ,Rejected_D8S1179 ] = Edit_Test_Data( D8S1179test ,D8S1179_global ,Rejected_D8S1179  ) ;
    setappdata(0,'D8S1179_plot',D8S1179_plot);
   setappdata(0,'D8S1179_global',D8S1179_global);
   setappdata(0,'Rejected_D8S1179',Rejected_D8S1179);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D8S1179,'D8S1179','A2');
   
  D21S11test=[test(:,1),test(:,12)];
[ D21S11_plot ,D21S11_global ,Rejected_D21S11  ] = Edit_Test_Data( D21S11test ,D21S11_global ,Rejected_D21S11 ) ;
    setappdata(0,'D21S11_plot',D21S11_plot);
   setappdata(0,'D21S11_global',D21S11_global);
   setappdata(0,'Rejected_D21S11',Rejected_D21S11);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D21S11,'D21S11','A2');
   
  D18S51test=[test(:,1),test(:,13)];
[ D18S51_plot ,D18S51_global , Rejected_D18S51] = Edit_Test_Data( D18S51test ,D18S51_global , Rejected_D18S51 ) ;
    setappdata(0,'D18S51_plot',D18S51_plot);
   setappdata(0,'D18S51_global',D18S51_global);
   setappdata(0,'Rejected_D18S51',Rejected_D18S51);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D18S51,'D18S51','A2');
   
  D16S539test=[test(:,1),test(:,14)];
[ D16S539_plot ,D16S539_global , Rejected_D16S539 ] = Edit_Test_Data( D16S539test ,D16S539_global , Rejected_D16S539  ) ;
    setappdata(0,'D16S539_plot',D16S539_plot);
   setappdata(0,'D16S539_global',D16S539_global);
   setappdata(0,'Rejected_D16S539',Rejected_D16S539);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D16S539,'D16S539','A2');
   
  D2S1338test=[test(:,1),test(:,15)];
[ D2S1338_plot ,D2S1338_global, Rejected_D2S1338 ] = Edit_Test_Data( D2S1338test ,D2S1338_global , Rejected_D2S1338 ) ;
setappdata(0,'D2S1338_plot',D2S1338_plot);
   setappdata(0,'D2S1338_global',D2S1338_global);
   setappdata(0,'Rejected_D2S1338',Rejected_D2S1338);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D2S1338,'D2S1338','A2');
   
  D19S433test=[test(:,1),test(:,16)];
[ D19S433_plot ,D19S433_global , Rejected_D19S433 ] = Edit_Test_Data( D19S433test ,D19S433_global , Rejected_D19S433  ) ;
setappdata(0,'D19S433_plot',D19S433_plot);    
   setappdata(0,'D19S433_global',D19S433_global);
   setappdata(0,'Rejected_D19S433',Rejected_D19S433);
   xlswrite('Excel_Files\Under_test_populations.xlsx',Rejected_D19S433,'D19S433','A2');

%select axes and plot pop. data and test data over them
    axes(handles.axes1);
    a=D3S1358_global(:,1);
    s=[];
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D3S1358_plot=D3S1358_plot(:,2) ;
    plot(D3S1358_plot ,'r','LineWidth',2); %with color red
   
    axes(handles.axes2);
    a=vWA_global(:,1);
    s=[];
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    vWA_plot=vWA_plot(:,2) ;
    plot(vWA_plot ,'r','LineWidth',2); %with color red
   
    axes(handles.axes3); 
    a=FGA_global(:,1);
    s=[];
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    FGA_plot=FGA_plot(:,2) ;
    plot(FGA_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes4);   
    a=THO1_global(:,1);
    s=[];
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    THO1_plot=THO1_plot(:,2) ;
    plot(THO1_plot ,'r','LineWidth',2); %with color red
 
    axes(handles.axes5);
    a=TPOX_global(:,1);
    s=[];
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    TPOX_plot=TPOX_plot(:,2) ;
    plot(TPOX_plot ,'r','LineWidth',2); %with color red
 
    axes(handles.axes6);
    a=CSF1PO_global(:,1);
    s=[];
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    CSF1PO_plot=CSF1PO_plot(:,2) ;
    plot(CSF1PO_plot ,'r','LineWidth',2); %with color red
 
    axes(handles.axes7);
    a=D5S818_global(:,1);
    s=[];
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D5S818_plot=D5S818_plot(:,2) ;
    plot(D5S818_plot ,'r','LineWidth',2); %with color red
 
    axes(handles.axes8); 
    a=D13S317_global(:,1);
    s=[];
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D13S317_plot=D13S317_plot(:,2) ;
    plot(D13S317_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes9);
    a=D7S820_global(:,1);
    s=[];
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D7S820_plot=D7S820_plot(:,2) ;
    plot(D7S820_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes10);
    a=D8S1179_global(:,1);
    s=[];
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D8S1179_plot=D8S1179_plot(:,2) ;
    plot(D8S1179_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes11);
    a=D21S11_global(:,1);
    s=[];
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D21S11_plot=D21S11_plot(:,2) ;
    plot(D21S11_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes12);
    a=D18S51_global(:,1);
    s=[];
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D18S51_plot=D18S51_plot(:,2) ;
    plot(D18S51_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes13);
    a=D16S539_global(:,1);
    s=[];
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D16S539_plot=D16S539_plot(:,2) ;
    plot(D16S539_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes14);
    a=D2S1338_global(:,1);
    s=[];
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D2S1338_plot=D2S1338_plot(:,2) ;
    plot(D2S1338_plot ,'r','LineWidth',2); %with color red

    axes(handles.axes15);
    a=D19S433_global(:,1);
    s=[];
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    D19S433_plot=D19S433_plot(:,2) ;
    plot(D19S433_plot ,'r','LineWidth',2); %with color red



    %read info of data and write it in text box
   [v,Jornal] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','K1');
   [v,Paper] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','N1');
   [v,Author] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','L1');
   [Date,v] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','M1');
   [v,Pop_name] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','O1');
   [v,Sample_size] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','P1');
   all_info = {Jornal{1};Paper{1};Author{1};num2str(Date);Pop_name{1};Sample_size{1}};
   [v,some_info] = xlsread('Excel_Files\Tested_populations.xlsx','newpopulation_data1','J1');
   setappdata (0,'all_info',all_info);
   setappdata (0,'some_info',some_info);
   setappdata(0,'Jornal',Jornal);
   setappdata(0,'Paper',Paper);
   setappdata(0,'Author',Author);
   setappdata(0,'Date',Date);
   setappdata(0,'Pop_name',Pop_name);
   setappdata(0,'Sample_size',Sample_size);

   set(handles.Population_Information,'String',some_info);
   % setting the title of screen
    T1 = ' Allele Frequency Trajectories vs Global Trends'; 
    T1 =  strcat (population_Name,' ',T1) ;
    set (handles.Main_Title,'String',T1);
   clc
   
    %AVG button
function AVG_button_Callback(hObject, eventdata, handles)
% hObject    handle to AVG_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of AVG_button
a= get(hObject,'Value') ;
if a==1
    
  %  select axeses then pre-processing data to compute AVG line then
  %  ploting
    axes(handles.axes1);
    D3S1358_global=getappdata(0,'D3S1358_global');
    D3S1358_plot = getappdata(0,'D3S1358_plot');
    [ Avr , D3S1358_plot ] = Average( D3S1358_global, 2 ,D3S1358_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=D3S1358_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2),'r','LineWidth',2);
 
    axes(handles.axes2);
    vWA_global=getappdata(0,'vWA_global');
    vWA_plot = getappdata(0,'vWA_plot');
    [ Avr , vWA_plot ] = Average( vWA_global ,3,vWA_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=vWA_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(vWA_plot(:,2),'r','LineWidth',2);
    
    axes(handles.axes3);
    FGA_global=getappdata(0,'FGA_global');
    FGA_plot = getappdata(0,'FGA_plot');
    [ Avr , FGA_plot ] = Average( FGA_global ,4,FGA_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=FGA_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(FGA_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes4);
    THO1_global=getappdata(0,'THO1_global');
    THO1_plot = getappdata(0,'THO1_plot');
    [ Avr , THO1_plot ] = Average( THO1_global ,5,THO1_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=THO1_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(THO1_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes5);
    TPOX_global=getappdata(0,'TPOX_global');
    TPOX_plot = getappdata(0,'TPOX_plot');
    [ Avr , TPOX_plot ] = Average( TPOX_global ,6,TPOX_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=TPOX_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(TPOX_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes6);
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    CSF1PO_plot = getappdata(0,'CSF1PO_plot');
    [ Avr , CSF1PO_plot ] = Average( CSF1PO_global,7 ,CSF1PO_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=CSF1PO_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(CSF1PO_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes7);
    D5S818_global=getappdata(0,'D5S818_global');
    D5S818_plot = getappdata(0,'D5S818_plot');
    [ Avr , D5S818_plot ] = Average( D5S818_global,8 ,D5S818_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=D5S818_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D5S818_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes8);
    D13S317_global=getappdata(0,'D13S317_global');
    D13S317_plot = getappdata(0,'D13S317_plot');
    [ Avr , D13S317_plot ] = Average( D13S317_global,9,D13S317_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=D13S317_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D13S317_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes9);
    D7S820_global=getappdata(0,'D7S820_global');
    D7S820_plot = getappdata(0,'D7S820_plot');
    [ Avr , D7S820_plot ] = Average( D7S820_global ,10,D7S820_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=D7S820_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D7S820_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes10);
    D8S1179_global=getappdata(0,'D8S1179_global');
    D8S1179_plot = getappdata(0,'D8S1179_plot');
    [ Avr , D8S1179_plot ] = Average( D8S1179_global,11,D8S1179_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=D8S1179_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D8S1179_plot(:,2),'r','LineWidth',2);
  
     axes(handles.axes11);
    D21S11_global=getappdata(0,'D21S11_global');
    D21S11_plot = getappdata(0,'D21S11_plot');
    [ Avr , D21S11_plot ] = Average( D21S11_global ,12,D21S11_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=D21S11_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D21S11_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes12);
    D18S51_global=getappdata(0,'D18S51_global');
    D18S51_plot = getappdata(0,'D18S51_plot');
    [ Avr , D18S51_plot ] = Average( D18S51_global ,13,D18S51_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=D18S51_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D18S51_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes13);
    D16S539_global=getappdata(0,'D16S539_global');
    D16S539_plot = getappdata(0,'D16S539_plot');
    [ Avr , D16S539_plot ] = Average( D16S539_global,14,D16S539_plot  );
    cla ; 
    plot(Avr,'b'); hold on
    a=D16S539_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D16S539_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes14);
    D2S1338_global=getappdata(0,'D2S1338_global');
    D2S1338_plot = getappdata(0,'D2S1338_plot');
    [ Avr , D2S1338_plot ] = Average( D2S1338_global,15,D2S1338_plot );
    cla ; 
    plot(Avr,'b'); hold on
    a=D2S1338_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D2S1338_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes15);
    D19S433_global=getappdata(0,'D19S433_global');
    D19S433_plot = getappdata(0,'D19S433_plot');
    [ Avr , D19S433_plot ] = Average( D19S433_global ,16,D19S433_plot);
    cla ; 
    plot(Avr,'b'); hold on
    a=D19S433_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D19S433_plot(:,2),'r','LineWidth',2);

% setting the title of screen
    population_Name = getappdata(0,'population_Name');
if (isnan(population_Name))
    T1 = 'Allele Frequency Trajectories Global Average Trends' ;  
    set (handles.Main_Title,'String',T1);
   
else
    T1 = ' Allele Frequency Trajectories vs Global Average Trends'; 
    T1 =  strcat (population_Name,' ',T1) ;
    set (handles.Main_Title,'String',T1);
end

    set (handles.AVG_button,'String','AFTs Global Trends');
else
     % (when press again retrive to opening screen)
     % getting global and test population data
        D3S1358_global=getappdata(0,'D3S1358_global');
        D3S1358_plot=getappdata(0,'D3S1358_plot');
        vWA_global=getappdata(0,'vWA_global');
        vWA_plot=getappdata(0,'vWA_plot');
        FGA_global=getappdata(0,'FGA_global');
        FGA_plot=getappdata(0,'FGA_plot');
        THO1_global=getappdata(0,'THO1_global');
        THO1_plot=getappdata(0,'THO1_plot');
        TPOX_global=getappdata(0,'TPOX_global');
        TPOX_plot=getappdata(0,'TPOX_plot');
        CSF1PO_global=getappdata(0,'CSF1PO_global');
        CSF1PO_plot=getappdata(0,'CSF1PO_plot');
        D5S818_global=getappdata(0,'D5S818_global');
        D5S818_plot=getappdata(0,'D5S818_plot');
        D13S317_global=getappdata(0,'D13S317_global');
        D13S317_plot=getappdata(0,'D13S317_plot');
        D7S820_global=getappdata(0,'D7S820_global');
        D7S820_plot=getappdata(0,'D7S820_plot');
        D8S1179_global=getappdata(0,'D8S1179_global');
        D8S1179_plot=getappdata(0,'D8S1179_plot');
        D21S11_global=getappdata(0,'D21S11_global');
        D21S11_plot=getappdata(0,'D21S11_plot');
        D18S51_global=getappdata(0,'D18S51_global');
        D18S51_plot=getappdata(0,'D18S51_plot');
        D16S539_global=getappdata(0,'D16S539_global');
        D16S539_plot=getappdata(0,'D16S539_plot');
        D19S433_global=getappdata(0,'D19S433_global');
        D19S433_plot=getappdata(0,'D19S433_plot');
        D2S1338_global=getappdata(0,'D2S1338_global');
        D2S1338_plot=getappdata(0,'D2S1338_plot');
    
%select axeses ,, clear ,, plot global and hold them ,, then plot
%tested/saved population
    axes(handles.axes1);
    a=D3S1358_global(:,1);
    s=[];
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2),'r','LineWidth',2);
   
   
    axes(handles.axes2);
    a=vWA_global(:,1);
    s=[];
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(vWA_plot(:,2),'r','LineWidth',2);
   
    axes(handles.axes3); 
    a=FGA_global(:,1);
    s=[];
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(FGA_plot(:,2),'r','LineWidth',2);

    axes(handles.axes4);   
    a=THO1_global(:,1);
    s=[];
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(THO1_plot(:,2),'r','LineWidth',2);   
 
    axes(handles.axes5);
    a=TPOX_global(:,1);
    s=[];
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(TPOX_plot(:,2),'r','LineWidth',2);  

    axes(handles.axes6);
    a=CSF1PO_global(:,1);
    s=[];
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(CSF1PO_plot(:,2),'r','LineWidth',2); 

    axes(handles.axes7);
    a=D5S818_global(:,1);
    s=[];
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D5S818_plot(:,2),'r','LineWidth',2); 

    axes(handles.axes8); 
    a=D13S317_global(:,1);
    s=[];
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D13S317_plot(:,2),'r','LineWidth',2); 

    axes(handles.axes9);
    a=D7S820_global(:,1);
    s=[];
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D7S820_plot(:,2),'r','LineWidth',2);
    
    axes(handles.axes10);
    a=D8S1179_global(:,1);
    s=[];
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D8S1179_plot(:,2),'r','LineWidth',2);

    axes(handles.axes11);
    a=D21S11_global(:,1);
    s=[];
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D21S11_plot(:,2),'r','LineWidth',2);
    
    axes(handles.axes12);
    a=D18S51_global(:,1);
    s=[];
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D18S51_plot(:,2),'r','LineWidth',2);

    axes(handles.axes13);
    a=D16S539_global(:,1);
    s=[];
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D16S539_plot(:,2),'r','LineWidth',2);

    axes(handles.axes14);
    a=D2S1338_global(:,1);
    s=[];
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D2S1338_plot(:,2),'r','LineWidth',2);

    axes(handles.axes15);
    a=D19S433_global(:,1);
    s=[];
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);hold on
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    plot(D19S433_plot(:,2),'r','LineWidth',2);

% setting the title of screen
     population_Name = getappdata(0,'population_Name');
if (isnan(population_Name))
    T1 = 'Allele Frequency Trajectories Global Trends' ;  
   set (handles.Main_Title,'String',T1);
else
    T1 = ' Allele Frequency Trajectories vs Global Trends'; 
    T1 =  strcat (population_Name,' ',T1) ;
    set (handles.Main_Title,'String',T1);
end
    
    set (handles.AVG_button,'String','AFTs Global Average');
end

    % saving button to global !
function SaveGlobal_button_Callback(hObject, eventdata, handles)
% hObject    handle to SaveGlobal_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%check if there is a data to save or not
    population_Name=getappdata(0,'population_Name');
    [num,Allcountries]=xlsread('Excel_Files\Tested_populations.xlsx','D3S1358');
    Allcountries = Allcountries(1,:);
    Allcountries=[{'Tested Populations'},Allcountries(1:end)];
    E = strcmp(population_Name,Allcountries) ;
    
    if(isnan(population_Name))
        msgbox('There is no data to save','Error','error')
else if (E==0)

     % add population name to the 15 sheet of 15 locs in the excel file
     % then add the data for all population
    [a,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;
    Allcountries =Allcountries(1,:);
    Allcountries=[Allcountries(1:end) , {population_Name}];
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D3S1358','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'vWA','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'FGA','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'THO1' ,'B1') ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'TPOX' ,'B1') ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'CSF1PO','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D5S818','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D13S317','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D7S820','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D8S1179','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D21S11','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D18S51','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D16S539','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D2S1338','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries ,'D19S433','B1' ) ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries' ,'Color', 'A2') ;
    xlswrite ('Excel_Files\Tested_populations.xlsx' , Allcountries' ,'newpopulation_data1', 'A2') ;

% add the new population data and global data after editing to the excel file that
% contain the global data (named 'Tested_populations')
   
    D3S1358_plot=getappdata(0,'D3S1358_plot');
    vWA_plot=getappdata(0,'vWA_plot');
    FGA_plot=getappdata(0,'FGA_plot');
    THO1_plot=getappdata(0,'THO1_plot');
    TPOX_plot=getappdata(0,'TPOX_plot');
    CSF1PO_plot=getappdata(0,'CSF1PO_plot');
    D5S818_plot=getappdata(0,'D5S818_plot');
    D13S317_plot=getappdata(0,'D13S317_plot');
    D7S820_plot=getappdata(0,'D7S820_plot');
    D8S1179_plot=getappdata(0,'D8S1179_plot');
    D21S11_plot=getappdata(0,'D21S11_plot');
    D18S51_plot=getappdata(0,'D18S51_plot');
    D16S539_plot=getappdata(0,'D16S539_plot');
    D2S1338_plot=getappdata(0,'D2S1338_plot');
    D19S433_plot=getappdata(0,'D19S433_plot');

    D3S1358_global=getappdata(0,'D3S1358_global');
    vWA_global=getappdata(0,'vWA_global');
    FGA_global=getappdata(0,'FGA_global');
    THO1_global=getappdata(0,'THO1_global');
    TPOX_global=getappdata(0,'TPOX_global');
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    D5S818_global=getappdata(0,'D5S818_global');
    D13S317_global=getappdata(0,'D13S317_global');
    D7S820_global=getappdata(0,'D7S820_global');
    D8S1179_global=getappdata(0,'D8S1179_global');
    D21S11_global=getappdata(0,'D21S11_global');
    D18S51_global=getappdata(0,'D18S51_global');
    D16S539_global=getappdata(0,'D16S539_global');
    D2S1338_global=getappdata(0,'D2S1338_global');
    D19S433_global=getappdata(0,'D19S433_global');
D3S1358_global = [ D3S1358_global , D3S1358_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D3S1358_global , 'D3S1358' , 'A2');
setappdata(0,'D3S1358_global',D3S1358_global);

vWA_global = [ vWA_global ,vWA_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , vWA_global , 'vWA' , 'A2');
setappdata(0,'vWA_global',vWA_global);

FGA_global = [ FGA_global, FGA_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , FGA_global , 'FGA' , 'A2');
setappdata(0,'FGA_global',FGA_global);

THO1_global = [ THO1_global ,THO1_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , THO1_global , 'THO1' , 'A2');
setappdata(0,'THO1_global',THO1_global);

TPOX_global = [ TPOX_global, TPOX_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , TPOX_global , 'TPOX' , 'A2');
setappdata(0,'TPOX_global',TPOX_global);

CSF1PO_global = [ CSF1PO_global ,CSF1PO_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , CSF1PO_global , 'CSF1PO' , 'A2');
setappdata(0,'CSF1PO_global',CSF1PO_global);

D5S818_global = [ D5S818_global ,D5S818_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D5S818_global , 'D5S818' , 'A2');
setappdata(0,'D5S818_global',D5S818_global);

D13S317_global = [ D13S317_global, D13S317_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D13S317_global , 'D13S317' , 'A2');
setappdata(0,'D13S317_global',D13S317_global);

D7S820_global = [ D7S820_global ,D7S820_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D7S820_global , 'D7S820' , 'A2');
setappdata(0,'D7S820_global',D7S820_global);

D8S1179_global = [ D8S1179_global, D8S1179_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D8S1179_global , 'D8S1179' , 'A2');
setappdata(0,'D8S1179_global',D8S1179_global);

D21S11_global = [ D21S11_global, D21S11_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D21S11_global , 'D21S11' , 'A2');
setappdata(0,'D21S11_global',D21S11_global);

D18S51_global = [ D18S51_global ,D18S51_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D18S51_global , 'D18S51' , 'A2');
setappdata(0,'D18S51_global',D18S51_global);

D16S539_global = [ D16S539_global, D16S539_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D16S539_global , 'D16S539' , 'A2');
setappdata(0,'D16S539_global',D16S539_global);

D2S1338_global = [ D2S1338_global ,D2S1338_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D2S1338_global , 'D2S1338' , 'A2');
setappdata(0,'D2S1338_global',D2S1338_global);

D19S433_global = [ D19S433_global ,D19S433_plot(:,2) ];
xlswrite('Excel_Files\Tested_populations.xlsx' , D19S433_global , 'D19S433' , 'A2');
setappdata(0,'D19S433_global',D19S433_global);

%set population names to pop up menu after adding the new population
[a,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;
Allcountries=[{'Tested Populations'},Allcountries(1:end)];
set(handles.popupMenu_TestedPopulations,'String', Allcountries);

% saving info text in the same excel file - named with population name -
% which contain matrix data
Jornal=   getappdata(0,'Jornal');
Paper=   getappdata(0,'Paper');
Author=   getappdata(0,'Author');
Date=   getappdata(0,'Date');
Pop_name=   getappdata(0,'Pop_name');
Sample_size=   getappdata(0,'Sample_size');
some_info = getappdata(0,'some_info');
population_Name=getappdata(0,'population_Name');
[num,Allcountries,all]  = xlsread('Excel_Files\Tested_populations.xlsx' , 'newpopulation_data1') ;
Allcountries = Allcountries(:,1);
pop_row = strcmp (population_Name,Allcountries) ;
Jornal_col=all(:,3);
Paper_col=all(:,4);
Author_col=all(:,5);
Date_col=num(:,1);
Pop_name_col=all(:,7);
Sample_size_col=all(:,8);
Somedata=all(:,9);
Somedata(pop_row) = some_info ;
Jornal_col(pop_row) = Jornal ;
Paper_col(pop_row) = Paper ;
Author_col(pop_row) = Author ;
Pop_name_col(pop_row) = Pop_name ;
Date_col(pop_row) = Date ;
Sample_size_col(pop_row) = Sample_size ;
xlswrite('Excel_Files\Tested_populations.xlsx',Somedata,'newpopulation_data1','B1');
xlswrite('Excel_Files\Tested_populations.xlsx',Jornal_col,'newpopulation_data1','C1');
xlswrite('Excel_Files\Tested_populations.xlsx',Paper_col,'newpopulation_data1','D1');
xlswrite('Excel_Files\Tested_populations.xlsx',Author_col,'newpopulation_data1','E1');
xlswrite('Excel_Files\Tested_populations.xlsx',Pop_name_col,'newpopulation_data1','F1');
xlswrite('Excel_Files\Tested_populations.xlsx',Date_col,'newpopulation_data1','G1');
xlswrite('Excel_Files\Tested_populations.xlsx',Sample_size_col,'newpopulation_data1','H1');

    %after saving clear axeses and retrive opening screan
    axes(handles.axes1);
    s=[];
    a=D3S1358_global(:,1);  % the names of allels
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s); 
    
    axes(handles.axes2);
    s=[];
    a=vWA_global(:,1);  % the names of allels
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);   
    
    axes(handles.axes3);    
    s=[];
    a=FGA_global(:,1);  % the names of allels
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes4);
    s=[];
    a=THO1_global(:,1);  % the names of allels
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);    
    
    axes(handles.axes5);
    s=[];
    a=TPOX_global(:,1);  % the names of allels
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes6);
    s=[];
    a=CSF1PO_global(:,1);  % the names of allels
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    s=[];
    a=D5S818_global(:,1);  % the names of allels
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes8); 
    s=[];
    a=D13S317_global(:,1);  % the names of allels
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes9);
    s=[];
    a=D7S820_global(:,1);  % the names of allels
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes10);
    s=[];
    a=D8S1179_global(:,1);  % the names of allels
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    s=[];
    a=D21S11_global(:,1);  % the names of allels
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes12);
    s=[];
    a=D18S51_global(:,1);  % the names of allels
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    s=[];
    a=D16S539_global(:,1);  % the names of allels
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    s=[];
    a=D2S1338_global(:,1);  % the names of allels
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    s=[];
    a=D19S433_global(:,1);  % the names of allels
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    %set all variable to nan to prevent any bugs
    population_Name = nan ;
    D3S1358_plot = [NaN , NaN] ;
    vWA_plot = [nan nan] ;
    FGA_plot = [nan nan] ;
    THO1_plot = [nan nan] ;
    TPOX_plot = [nan nan] ;
    CSF1PO_plot = [nan nan] ;
    D5S818_plot = [nan nan] ;
    D13S317_plot = [nan nan] ;
    D7S820_plot = [nan nan] ;
    D8S1179_plot = [nan nan] ;
    D21S11_plot = [nan nan] ;
    D18S51_plot = [nan nan] ;
    D16S539_plot = [nan nan] ;
    D2S1338_plot = [nan nan] ;
    D19S433_plot = [nan nan] ;
    all_info = nan ;
    some_info = nan ;
setappdata(0,'D3S1358_plot',D3S1358_plot);
setappdata(0,'vWA_plot',vWA_plot);
setappdata(0,'FGA_plot',FGA_plot);
setappdata(0,'THO1_plot',THO1_plot);
setappdata(0,'TPOX_plot',TPOX_plot);
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
setappdata(0,'D5S818_plot',D5S818_plot);
setappdata(0,'D13S317_plot',D13S317_plot);
setappdata(0,'D7S820_plot',D7S820_plot);
setappdata(0,'D8S1179_plot',D8S1179_plot);
setappdata(0,'D21S11_plot',D21S11_plot);
setappdata(0,'D18S51_plot',D18S51_plot);
setappdata(0,'D16S539_plot',D16S539_plot);
setappdata(0,'D2S1338_plot',D2S1338_plot);
setappdata(0,'D19S433_plot',D19S433_plot);
setappdata(0,'population_Name',population_Name);
setappdata(0,'all_info',all_info);
setappdata(0,'some_info',some_info);

% setting the title of screen
T1 = 'Allele Frequency Trajectories Global Trends' ;  
set (handles.Main_Title,'String',T1);
% set information box to nan
set (handles.Population_Information,'String','  ');
  
    else 
        msgbox('Selected population is existed ','Error','error')
    end
    end

    % saving button under test !
function SaveUnderTest_button_Callback(hObject, eventdata, handles)
% hObject    handle to SaveUnderTest_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%check if there is a data to save or not
    population_Name=getappdata(0,'population_Name');
    [num,Allcountries]=xlsread('Excel_Files\Under_test_populations.xlsx','D3S1358');
    Allcountries = Allcountries(1,:);
    Allcountries=[{'Populations Under Test'},Allcountries(1:end)];
    E = strcmp(population_Name,Allcountries) ;
    
    if(isnan(population_Name))
        msgbox('There is no data to save','Error','error')
else if (E==0)
        %population is not existed, save all thing

     % add population name to the 15 sheet of 15 locs in the excel file
     % then add the data for all population
    [a,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;
    Allcountries =Allcountries(1,:);
    Allcountries=[Allcountries(1:end) , {population_Name}];
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D3S1358','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'vWA','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'FGA','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'THO1' ,'A1') ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'TPOX' ,'A1') ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'CSF1PO','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D5S818','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D13S317','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D7S820','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D8S1179','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D21S11','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D18S51','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D16S539','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D2S1338','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries ,'D19S433','A1' ) ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries' ,'Color', 'A1') ;
    xlswrite ('Excel_Files\Under_test_populations.xlsx' , Allcountries' ,'newpopulation_data1', 'A1') ;

% add the new population data and all rejected populations data after editing to the excel file that
% contain the global data (named 'Under_test_populations')
   
    D3S1358_plot=getappdata(0,'D3S1358_plot');
    vWA_plot=getappdata(0,'vWA_plot');
    FGA_plot=getappdata(0,'FGA_plot');
    THO1_plot=getappdata(0,'THO1_plot');
    TPOX_plot=getappdata(0,'TPOX_plot');
    CSF1PO_plot=getappdata(0,'CSF1PO_plot');
    D5S818_plot=getappdata(0,'D5S818_plot');
    D13S317_plot=getappdata(0,'D13S317_plot');
    D7S820_plot=getappdata(0,'D7S820_plot');
    D8S1179_plot=getappdata(0,'D8S1179_plot');
    D21S11_plot=getappdata(0,'D21S11_plot');
    D18S51_plot=getappdata(0,'D18S51_plot');
    D16S539_plot=getappdata(0,'D16S539_plot');
    D2S1338_plot=getappdata(0,'D2S1338_plot');
    D19S433_plot=getappdata(0,'D19S433_plot');

    D3S1358_global=getappdata(0,'D3S1358_global');
    vWA_global=getappdata(0,'vWA_global');
    FGA_global=getappdata(0,'FGA_global');
    THO1_global=getappdata(0,'THO1_global');
    TPOX_global=getappdata(0,'TPOX_global');
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    D5S818_global=getappdata(0,'D5S818_global');
    D13S317_global=getappdata(0,'D13S317_global');
    D7S820_global=getappdata(0,'D7S820_global');
    D8S1179_global=getappdata(0,'D8S1179_global');
    D21S11_global=getappdata(0,'D21S11_global');
    D18S51_global=getappdata(0,'D18S51_global');
    D16S539_global=getappdata(0,'D16S539_global');
    D2S1338_global=getappdata(0,'D2S1338_global');
    D19S433_global=getappdata(0,'D19S433_global');
    
    D3S1358_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D3S1358') ;
    vWA_rej=xlsread('Excel_Files\Under_test_populations.xlsx','vWA') ;
    FGA_rej=xlsread('Excel_Files\Under_test_populations.xlsx','FGA') ;
    THO1_rej=xlsread('Excel_Files\Under_test_populations.xlsx','THO1') ;
    TPOX_rej=xlsread('Excel_Files\Under_test_populations.xlsx','TPOX') ;
    CSF1PO_rej=xlsread('Excel_Files\Under_test_populations.xlsx','CSF1PO') ;
    D5S818_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D5S818') ;
    D13S317_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D13S317') ;
    D7S820_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D7S820') ;
    D8S1179_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D8S1179') ;
    D21S11_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D21S11') ;
    D18S51_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D18S51') ;
    D16S539_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D16S539') ;
    D2S1338_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D2S1338') ;
    D19S433_rej=xlsread('Excel_Files\Under_test_populations.xlsx','D19S433') ;
D3S1358_rej = [ D3S1358_rej , D3S1358_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D3S1358_rej , 'D3S1358' , 'A2');
setappdata(0,'D3S1358_rej',D3S1358_rej);

vWA_rej = [ vWA_rej ,vWA_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , vWA_rej , 'vWA' , 'A2');
setappdata(0,'vWA_rej',vWA_rej);

FGA_rej = [ FGA_rej, FGA_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , FGA_rej , 'FGA' , 'A2');
setappdata(0,'FGA_rej',FGA_rej);

THO1_rej = [ THO1_rej ,THO1_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , THO1_rej , 'THO1' , 'A2');
setappdata(0,'THO1_rej',THO1_rej);

TPOX_rej = [ TPOX_rej, TPOX_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , TPOX_rej , 'TPOX' , 'A2');
setappdata(0,'TPOX_rej',TPOX_rej);

CSF1PO_rej = [ CSF1PO_rej ,CSF1PO_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , CSF1PO_rej , 'CSF1PO' , 'A2');
setappdata(0,'CSF1PO_rej',CSF1PO_rej);

D5S818_rej = [ D5S818_rej ,D5S818_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D5S818_rej , 'D5S818' , 'A2');
setappdata(0,'D5S818_rej',D5S818_rej);

D13S317_rej = [ D13S317_rej, D13S317_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D13S317_rej , 'D13S317' , 'A2');
setappdata(0,'D13S317_rej',D13S317_rej);

D7S820_rej = [ D7S820_rej ,D7S820_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D7S820_rej , 'D7S820' , 'A2');
setappdata(0,'D7S820_rej',D7S820_rej);

D8S1179_rej = [ D8S1179_rej, D8S1179_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D8S1179_rej , 'D8S1179' , 'A2');
setappdata(0,'D8S1179_rej',D8S1179_rej);

D21S11_rej = [ D21S11_rej, D21S11_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D21S11_rej , 'D21S11' , 'A2');
setappdata(0,'D21S11_rej',D21S11_rej);

D18S51_rej = [ D18S51_rej ,D18S51_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D18S51_rej , 'D18S51' , 'A2');
setappdata(0,'D18S51_rej',D18S51_rej);

D16S539_rej = [ D16S539_rej, D16S539_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D16S539_rej , 'D16S539' , 'A2');
setappdata(0,'D16S539_rej',D16S539_rej);

D2S1338_rej = [ D2S1338_rej ,D2S1338_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D2S1338_rej , 'D2S1338' , 'A2');
setappdata(0,'D2S1338_rej',D2S1338_rej);

D19S433_rej = [ D19S433_rej ,D19S433_plot(:,2) ];
xlswrite('Excel_Files\Under_test_populations.xlsx' , D19S433_rej , 'D19S433' , 'A2');
setappdata(0,'D19S433_rej',D19S433_rej);

%set population names to pop up menu after adding the new population
[a,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;
set(handles.Popupmenu_UnderTestPopulation,'String', Allcountries);

% saving info text in the same excel file - named with population name -
% which contain matrix data
Jornal=   getappdata(0,'Jornal');
Paper=   getappdata(0,'Paper');
Author=   getappdata(0,'Author');
Date=   getappdata(0,'Date');
Pop_name=   getappdata(0,'Pop_name');
Sample_size=   getappdata(0,'Sample_size');
some_info = getappdata(0,'some_info');
population_Name=getappdata(0,'population_Name');

[num,Allcountries,all]  = xlsread('Excel_Files\Under_test_populations.xlsx' , 'newpopulation_data1') ;
Allcountries = Allcountries(:,1);
pop_row = strcmp (population_Name,Allcountries) ;
Jornal_col=all(:,3);
Paper_col=all(:,4);
Author_col=all(:,5);
Date_col=num;
Pop_name_col=all(:,7);
Sample_size_col=all(:,8);
Somedata=all(:,2);
Somedata(pop_row) = some_info ;
Jornal_col(pop_row) = Jornal ;
Paper_col(pop_row) = Paper ;
Author_col(pop_row) = Author ;
Pop_name_col(pop_row) = Pop_name ;
Date_col(pop_row) = Date ;
Sample_size_col(pop_row) = Sample_size ;
xlswrite('Excel_Files\Under_test_populations.xlsx',Somedata,'newpopulation_data1','B1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Jornal_col,'newpopulation_data1','C1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Paper_col,'newpopulation_data1','D1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Author_col,'newpopulation_data1','E1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Pop_name_col,'newpopulation_data1','F1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Date_col,'newpopulation_data1','G1');
xlswrite('Excel_Files\Under_test_populations.xlsx',Sample_size_col,'newpopulation_data1','H1');

    %after saving clear axeses and retrive opening screan
    axes(handles.axes1);
    s=[];
    a=D3S1358_global(:,1);  % the names of allels
    D3S1358_global=D3S1358_global(:,(2:end));
    cla
    plot(D3S1358_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s); 
    
    axes(handles.axes2);
    s=[];
    a=vWA_global(:,1);  % the names of allels
    vWA_global=vWA_global(:,(2:end));
    cla
    plot(vWA_global); 
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);   
    
    axes(handles.axes3);    
    s=[];
    a=FGA_global(:,1);  % the names of allels
    FGA_global=FGA_global(:,(2:end));
    cla
    plot(FGA_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes4);
    s=[];
    a=THO1_global(:,1);  % the names of allels
    THO1_global=THO1_global(:,(2:end));
    cla
    plot(THO1_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);    
    
    axes(handles.axes5);
    s=[];
    a=TPOX_global(:,1);  % the names of allels
    TPOX_global=TPOX_global(:,(2:end));
    cla
    plot(TPOX_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes6);
    s=[];
    a=CSF1PO_global(:,1);  % the names of allels
    CSF1PO_global=CSF1PO_global(:,(2:end));
    cla
    plot(CSF1PO_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes7);
    s=[];
    a=D5S818_global(:,1);  % the names of allels
    D5S818_global=D5S818_global(:,(2:end));
    cla
    plot(D5S818_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes8); 
    s=[];
    a=D13S317_global(:,1);  % the names of allels
    D13S317_global=D13S317_global(:,(2:end));
    cla
    plot(D13S317_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes9);
    s=[];
    a=D7S820_global(:,1);  % the names of allels
    D7S820_global=D7S820_global(:,(2:end));
    cla
    plot(D7S820_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes10);
    s=[];
    a=D8S1179_global(:,1);  % the names of allels
    D8S1179_global=D8S1179_global(:,(2:end));
    cla
    plot(D8S1179_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes11);
    s=[];
    a=D21S11_global(:,1);  % the names of allels
    D21S11_global=D21S11_global(:,(2:end));
    cla
    plot(D21S11_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    
    axes(handles.axes12);
    s=[];
    a=D18S51_global(:,1);  % the names of allels
    D18S51_global=D18S51_global(:,(2:end));
    cla
    plot(D18S51_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes13);
    s=[];
    a=D16S539_global(:,1);  % the names of allels
    D16S539_global=D16S539_global(:,(2:end));
    cla
    plot(D16S539_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes14);
    s=[];
    a=D2S1338_global(:,1);  % the names of allels
    D2S1338_global=D2S1338_global(:,(2:end));
    cla
    plot(D2S1338_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

    axes(handles.axes15);
    s=[];
    a=D19S433_global(:,1);  % the names of allels
    D19S433_global=D19S433_global(:,(2:end));
    cla
    plot(D19S433_global);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);

%set all variable to nan to prevent any bugs
    population_Name = nan ;
    D3S1358_plot = [NaN , NaN] ;
    vWA_plot = [nan nan] ;
    FGA_plot = [nan nan] ;
    THO1_plot = [nan nan] ;
    TPOX_plot = [nan nan] ;
    CSF1PO_plot = [nan nan] ;
    D5S818_plot = [nan nan] ;
    D13S317_plot = [nan nan] ;
    D7S820_plot = [nan nan] ;
    D8S1179_plot = [nan nan] ;
    D21S11_plot = [nan nan] ;
    D18S51_plot = [nan nan] ;
    D16S539_plot = [nan nan] ;
    D2S1338_plot = [nan nan] ;
    D19S433_plot = [nan nan] ;
    all_info = nan ;
    some_info = nan ;
setappdata(0,'D3S1358_plot',D3S1358_plot);
setappdata(0,'vWA_plot',vWA_plot);
setappdata(0,'FGA_plot',FGA_plot);
setappdata(0,'THO1_plot',THO1_plot);
setappdata(0,'TPOX_plot',TPOX_plot);
setappdata(0,'CSF1PO_plot',CSF1PO_plot);
setappdata(0,'D5S818_plot',D5S818_plot);
setappdata(0,'D13S317_plot',D13S317_plot);
setappdata(0,'D7S820_plot',D7S820_plot);
setappdata(0,'D8S1179_plot',D8S1179_plot);
setappdata(0,'D21S11_plot',D21S11_plot);
setappdata(0,'D18S51_plot',D18S51_plot);
setappdata(0,'D16S539_plot',D16S539_plot);
setappdata(0,'D2S1338_plot',D2S1338_plot);
setappdata(0,'D19S433_plot',D19S433_plot);
setappdata(0,'population_Name',population_Name);
setappdata(0,'all_info',all_info);
setappdata(0,'some_info',some_info);

% setting the title of screen
T1 = 'Allele Frequency Trajectories Global Trends' ;  
set (handles.Main_Title,'String',T1);
% set information box to nan
set (handles.Population_Information,'String','  ');
 
    else
        msgbox('Selected population is existed ','Error','error')
    end
    end

    % see more info
function SeeMore_button_Callback(hObject, eventdata, handles)
      population_Name=getappdata(0,'population_Name');
      if isnan(population_Name)
         msgbox('No population entered','Error','Error');
      else
    [num,Allcountries,all]  = xlsread('Excel_Files\Tested_populations.xlsx' , 'newpopulation_data1') ;
    Allcountries = Allcountries(:,1);
    pop_row = strcmp(population_Name,Allcountries);
    if (pop_row == 0)
    msg = getappdata(0,'all_info');
        B1 = strcat('Jornal Name:-',msg(1));
        B2 = strcat('Paper Name:-',msg(2));
        B3 = strcat('Authors:-',msg(3));
        B4 = strcat('Date:-',msg(4));
        B5 = strcat('Population Name:-',msg(5));
        B6 = strcat('Sample Size:-',msg(6));
        B = [B1;B2;B3;B4;B5;B6];
        msgbox(B,'All info');
    else
    Jornal=all(:,3);
    paper=all(:,4);
    author=all(:,5);
    pop_name=all(:,6);
    date=all(:,7);
    Size=all(:,8);
    Jornal=Jornal(pop_row);       J =num2str(Jornal{1}) ;
    paper=paper(pop_row);        P =num2str(paper{1}) ;
    author=author(pop_row);       A =num2str(author{1}) ;
    pop_name=pop_name(pop_row);   P_N =num2str(pop_name{1}) ;
    date=date(pop_row);           D =num2str(date{1}) ;
    Size=Size(pop_row);           S =num2str(Size{1}) ;
    msg= {J;P;A;P_N;D;S};
     QW = {'0'};
     if strcmp(QW,msg)
       msgbox('No data available ','Error','Error');
        else
        B1 = strcat('Jornal Name:-',msg(1));
        B2 = strcat('Paper Name:-',msg(2));
        B3 = strcat('Authors:-',msg(3));
        B4 = strcat('Date:-',msg(4));
        B5 = strcat('Population Name:-',msg(5));
        B6 = strcat('Sample Size:-',msg(6));
        B = [B1;B2;B3;B4;B5;B6];
        msgbox(B,'All info');
    end
    end
      end
      
    % --- Executes on button press in Comparison_button.
function Comparison_button_Callback(hObject, eventdata, handles)
% hObject    handle to Comparison_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%open the window of adding info and test file
    get (pop_comparison)

    % --- Executes on button press in D3_Result.
function D3_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D3_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D3S1358' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D3S1358_plot=getappdata(0,'D3S1358_plot'); % data of locus from the saving operation
    global_trend=getappdata(0,'D3S1358_global'); % data of locus from the saving operation
    image_name = 'Images\CCM_Sample_D3S1358.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig] = Result( 'Excel_Files\D3S1358_corr..xlsx' ,'Excel_Files\D3S1358_PV.xlsx',locs_name,D3S1358_plot , image_name , global_trend);
 % button for opening CCM exel file
 btn_cor_1  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_1);

 % button for opening p-value exel file
 btn_PV_1 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_1);

 % button for opening figure contain avg with the population line
 btn_AVG_1 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_1);
    function corr_1(btn_cor_1, EventData)   % callback for correlation button
        winopen('Excel_Files\D3S1358_corr..xlsx');
    function PV_1(btn_PV_1, EventData)   % callback for p-value button
        winopen('Excel_Files\D3S1358_PV.xlsx');
    function AVG_1(btn_AVG_1, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AFTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D3S1358_plot=getappdata(0,'D3S1358_plot'); %  data of locus from the pre-tested populartion  
    
if isnan(D3S1358_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name1') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name1') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end  
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D3S1358_global=getappdata(0,'D3S1358_global'); %read locus data
    [ Avr , D3S1358_plot ] = Average( D3S1358_global ,3,D3S1358_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D3S1358_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D3S1358_plot) == 1 ;
        legend('Global Average') % create box of legend    
    else
      plot(D3S1358_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global Average',Country) % create box of legend    
    end
    
    % --- Executes on button press in vWA_Result.
function vWA_Result_Callback(hObject, eventdata, handles)
% hObject    handle to vWA_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'vWA' ;  
    setappdata (0,'locs_name2' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'vWA_global'); % data of locus from the saving operation
    vWA_plot=getappdata(0,'vWA_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_vWa.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\vWA_corr..xlsx' ,'Excel_Files\vWA_PV.xlsx',locs_name,vWA_plot, image_name , global_trend); 
 % button for opening CCM exel file
 btn_cor_2  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_2);

 % button for opening p-value exel file
 btn_PV_2 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_2);

 % button for opening figure contain avg with the population line
 btn_AVG_2 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_2);
    function corr_2(btn_cor_2, EventData)
        winopen('Excel_Files\vWA_corr..xlsx');
    function PV_2(btn_PV_2, EventData)
        winopen('Excel_Files\vWA_PV.xlsx');
    function AVG_2(btn_AVG_2, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    vWA_plot=getappdata(0,'vWA_plot'); %  data of locus from the pre-tested populartion    
   
if isnan(vWA_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name2') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name2') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    vWA_global=getappdata(0,'vWA_global'); %read locus data
    [ Avr , vWA_plot ] = Average( vWA_global ,3,vWA_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=vWA_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(vWA_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(vWA_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in FGA_Result.
function FGA_Result_Callback(hObject, eventdata, handles)
% hObject    handle to FGA_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'FGA' ;  
    setappdata (0,'locs_name3' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'FGA_global'); % data of locus from the saving operation
    FGA_plot=getappdata(0,'FGA_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_FGA.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\FGA_corr..xlsx' ,'Excel_Files\FGA_PV.xlsx',locs_name,FGA_plot,...
        image_name , global_trend); 
 % button for opening CCM exel file
 btn_cor_3  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_3);

 % button for opening p-value exel file
 btn_PV_3 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_3);

 % button for opening figure contain avg with the population line
 btn_AVG_3 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_3);
    function corr_3(btn_cor_3, EventData)
        winopen('Excel_Files\FGA_corr..xlsx');
    function PV_3(btn_PV_3, EventData)
        winopen('Excel_Files\FGA_PV.xlsx');
    function AVG_3(btn_AVG_3, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    FGA_plot=getappdata(0,'FGA_plot'); %  data of locus from the pre-tested populartion    

if isnan(FGA_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name3') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name3') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end  
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    FGA_global=getappdata(0,'FGA_global'); %read locus data
    [ Avr , FGA_plot ] = Average( FGA_global ,4,FGA_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=FGA_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(FGA_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(FGA_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
     Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in THO1_Result.
function THO1_Result_Callback(hObject, eventdata, handles)
% hObject    handle to THO1_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'THO1' ;  
    setappdata (0,'locs_name4' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'THO1_global'); % data of locus from the saving operation
    THO1_plot=getappdata(0,'THO1_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_THO1.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\THO1_corr..xlsx' ,'Excel_Files\THO1_PV.xlsx',locs_name,THO1_plot , image_name , global_trend); 
 % button for opening CCM exel file
 btn_cor_4  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_4);

 % button for opening p-value exel file
 btn_PV_4 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_4);

 % button for opening figure contain avg with the population line
 btn_AVG_4 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_4);
    function corr_4(btn_cor_4, EventData)
        winopen('Excel_Files\THO1_corr..xlsx');
    function PV_4(btn_PV_4, EventData)
        winopen('Excel_Files\THO1_PV.xlsx');
    function AVG_4(btn_AVG_4, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    THO1_plot=getappdata(0,'THO1_plot'); %  data of locus from the pre-tested populartion    
   
if isnan(THO1_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name4') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name4') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    THO1_global=getappdata(0,'THO1_global'); %read locus data
    
    [ Avr , THO1_plot ] = Average( THO1_global ,5,THO1_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=THO1_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(THO1_plot) == 1 ;
        legend('Global Average') % create box of legend    
    else
      plot(THO1_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global Average',Country) % create box of legend    
    end

% --- Executes on button press in TPOX_Result.
function TPOX_Result_Callback(hObject, eventdata, handles)
% hObject    handle to TPOX_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'TPOX' ;  
    setappdata (0,'locs_name5' , locs_name) ; %store the name of locus
    TPOX_plot=getappdata(0,'TPOX_plot'); % data of locus from the saving operation
    global_trend=getappdata(0,'TPOX_global'); % data of locus from the saving operation
    image_name = 'Images\CCM_Sample_TPOX.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
        [fig] = Result( 'Excel_Files\TPOX_corr..xlsx' ,'Excel_Files\TPOX_PV.xlsx',locs_name,TPOX_plot, image_name , global_trend);
 
 % button for opening CCM exel file
 btn_cor_5  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_5);

 % button for opening p-value exel file
 btn_PV_5 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_5);

 % button for opening figure contain avg with the population line
 btn_AVG_5 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_5);
    function corr_5(btn_cor_5, EventData)
        winopen('Excel_Files\TPOX_corr..xlsx');
    function PV_5(btn_PV_5, EventData)
        winopen('Excel_Files\TPOX_PV.xlsx');
    function AVG_5(btn_AVG_5, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    TPOX_plot=getappdata(0,'TPOX_plot'); %  data of locus from the pre-tested populartion    
 
if isnan(TPOX_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name5') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name5') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    TPOX_global=getappdata(0,'TPOX_global'); %read locus data
    [ Avr , TPOX_plot ] = Average( TPOX_global ,6,TPOX_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=TPOX_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(TPOX_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(TPOX_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in CSF_Result.
function CSF_Result_Callback(hObject, eventdata, handles)
% hObject    handle to CSF_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'CSF1PO' ;  
    setappdata (0,'locs_name6' , locs_name) ; %store the name of locus
    CSF1PO_plot=getappdata(0,'CSF1PO_plot'); % data of locus from the saving operation
    global_trend=getappdata(0,'CSF1PO_global'); % data of locus from the saving operation
    image_name = 'Images\CCM_Sample_CSF1PO.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result(  'Excel_Files\CSF1PO_corr..xlsx' , 'Excel_Files\CSF1PO_PV.xlsx',locs_name,CSF1PO_plot, image_name , global_trend);
 
 % button for opening CCM exel file
 btn_cor_6  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_6);

 % button for opening p-value exel file
 btn_PV_6 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_6);

 % button for opening figure contain avg with the population line
 btn_AVG_6 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_6);
    function corr_6(btn_cor_6, EventData)
        winopen('Excel_Files\CSF1PO_corr..xlsx');
    function PV_6(btn_PV_6, EventData)
        winopen('Excel_Files\CSF1PO_PV.xlsx');
    function AVG_6(btn_AVG_6, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    CSF1PO_plot=getappdata(0,'CSF1PO_plot'); %  data of locus from the pre-tested populartion    

if isnan(CSF1PO_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name6') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name6') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    CSF1PO_global=getappdata(0,'CSF1PO_global'); %read locus data
    [ Avr , CSF1PO_plot ] = Average( CSF1PO_global ,7,CSF1PO_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=CSF1PO_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(CSF1PO_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(CSF1PO_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D5_Result.
function D5_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D5_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D5S818' ;  
    setappdata (0,'locs_name7' , locs_name) ; %store the name of locus
    D5S818_plot=getappdata(0,'D5S818_plot'); % data of locus from the saving operation
    global_trend=getappdata(0,'D5S818_global'); % data of locus from the saving operation
    image_name = 'Images\CCM_Sample_D5S818.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D5S818_corr..xlsx'  ,'Excel_Files\D5S818_PV.xlsx' ,locs_name,D5S818_plot, image_name , global_trend);
 
 % button for opening CCM exel file
 btn_cor_7  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_7);

 % button for opening p-value exel file
 btn_PV_7 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_7);

 % button for opening figure contain avg with the population line
 btn_AVG_7 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_7);
    function corr_7(btn_cor_7, EventData)
        winopen('Excel_Files\D5S818_corr..xlsx');
    function PV_7(btn_PV_7, EventData)
        winopen('Excel_Files\D5S818_PV.xlsx');
    function AVG_7(btn_AVG_7, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D5S818_plot=getappdata(0,'D5S818_plot'); %  data of locus from the pre-tested populartion    

if isnan(D5S818_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name7') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name7') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D5S818_global=getappdata(0,'D5S818_global'); %read locus data
    [ Avr , D5S818_plot ] = Average( D5S818_global ,8,D5S818_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D5S818_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D5S818_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D5S818_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

    
% --- Executes on button press in D13_Result.
function D13_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D13_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D13S317' ;  
    setappdata (0,'locs_name8' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D13S317_global'); % data of locus from the saving operation
    D13S317_plot=getappdata(0,'D13S317_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D13S317.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D13S317_corr..xlsx','Excel_Files\D13S317_PV.xlsx' ,locs_name,D13S317_plot, image_name , global_trend);  
 % button for opening CCM exel file
 btn_cor_8  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_8);

 % button for opening p-value exel file
 btn_PV_8 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_8);

 % button for opening figure contain avg with the population line
 btn_AVG_8 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_8);
    function corr_8(btn_cor_8, EventData)
        winopen('Excel_Files\D13S317_corr..xlsx');
    function PV_8(btn_PV_8, EventData)
        winopen('Excel_Files\D13S317_PV.xlsx');
    function AVG_8(btn_AVG_8, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D13S317_plot=getappdata(0,'D13S317_plot'); %  data of locus from the pre-tested populartion    

if isnan(D13S317_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name8') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name8') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D13S317_global=getappdata(0,'D13S317_global'); %read locus data
    [ Avr , D13S317_plot ] = Average( D13S317_global ,9,D13S317_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D13S317_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D13S317_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D13S317_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D7_Result.
function D7_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D7_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D7S820' ;  
    setappdata (0,'locs_name9' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D7S820_global'); % data of locus from the saving operation
    D7S820_plot=getappdata(0,'D7S820_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D7S820.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D7S820_corr..xlsx' ,'Excel_Files\D7S820_PV.xlsx',locs_name,D7S820_plot,image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_9  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_9);

 % button for opening p-value exel file
 btn_PV_9 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_9);

 % button for opening figure contain avg with the population line
 btn_AVG_9 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_9);
    function corr_9(btn_cor_9, EventData)
        winopen('Excel_Files\D7S820_corr..xlsx');
    function PV_9(btn_PV_9, EventData)
        winopen('Excel_Files\D7S820_PV.xlsx');
    function AVG_9(btn_AVG_9, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D7S820_plot=getappdata(0,'D7S820_plot'); %  data of locus from the pre-tested populartion    

if isnan(D7S820_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name9') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name9') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D7S820_global=getappdata(0,'D7S820_global'); %read locus data
    [ Avr , D7S820_plot ] = Average( D7S820_global ,10,D7S820_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D7S820_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D7S820_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D7S820_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D8_Result.
function D8_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D8_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D8S1179' ;  
    setappdata (0,'locs_name10' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D8S1179_global'); % data of locus from the saving operation
    D8S1179_plot=getappdata(0,'D8S1179_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D8S1179.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result(  'Excel_Files\D8S1179_corr..xlsx','Excel_Files\D8S1179_PV.xlsx',locs_name,D8S1179_plot, image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_10  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_10);

 % button for opening p-value exel file
 btn_PV_10 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_10);

 % button for opening figure contain avg with the population line
 btn_AVG_10 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_10);
    function corr_10(btn_cor_10, EventData)
        winopen('Excel_Files\D8S1179_corr..xlsx');
    function PV_10(btn_PV_10, EventData)
        winopen('Excel_Files\D8S1179_PV.xlsx');
    function AVG_10(btn_AVG_10, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D8S1179_plot=getappdata(0,'D8S1179_plot'); %  data of locus from the pre-tested populartion    

if isnan(D8S1179_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name10') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name10') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D8S1179_global=getappdata(0,'D8S1179_global'); %read locus data 
    [ Avr , D8S1179_plot ] = Average( D8S1179_global ,11,D8S1179_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D8S1179_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D8S1179_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D8S1179_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D21_Result.
function D21_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D21_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D21S11' ;  
    setappdata (0,'locs_name11' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D21S11_global'); % data of locus from the saving operation
    D21S11_plot=getappdata(0,'D21S11_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D21S11.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D21S11_corr..xlsx' ,'Excel_Files\D21S11_PV.xlsx',locs_name,D21S11_plot,image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_11  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_11);

 % button for opening p-value exel file
 btn_PV_11 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_11);

 % button for opening figure contain avg with the population line
 btn_AVG_11 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_11);
    function corr_11(btn_cor_11, EventData)
        winopen('Excel_Files\D21S11_corr..xlsx');
    function PV_11(btn_PV_11, EventData)
        winopen('Excel_Files\D21S11_PV.xlsx');
    function AVG_11(btn_AVG_11, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D21S11_plot=getappdata(0,'D21S11_plot'); %  data of locus from the pre-tested populartion    

if isnan(D21S11_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name11') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name11') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D21S11_global=getappdata(0,'D21S11_global'); %read locus data
    [ Avr , D21S11_plot ] = Average( D21S11_global ,12,D21S11_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D21S11_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D21S11_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D21S11_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end


% --- Executes on button press in D18_Result.
function D18_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D18_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D18S51' ;  
    setappdata (0,'locs_name12' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D18S51_global'); % data of locus from the saving operation
    D18S51_plot=getappdata(0,'D18S51_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D18S51.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D18S51_corr..xlsx','Excel_Files\D18S51_PV.xlsx',locs_name,D18S51_plot,image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_12  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_12);

 % button for opening p-value exel file
 btn_PV_12 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_12);

 % button for opening figure contain avg with the population line
 btn_AVG_12 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_12);
    function corr_12(btn_cor_12, EventData)
        winopen('Excel_Files\D18S51_corr..xlsx');
    function PV_12(btn_PV_12, EventData)
        winopen('Excel_Files\D18S51_PV.xlsx');
    function AVG_12(btn_AVG_12, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D18S51_plot=getappdata(0,'D18S51_plot'); %  data of locus from the pre-tested populartion    
   
if isnan(D18S51_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name12') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name12') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 

       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D18S51_global=getappdata(0,'D18S51_global'); %read locus data
    [ Avr , D18S51_plot ] = Average( D18S51_global ,13,D18S51_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D18S51_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D18S51_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D18S51_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D16_Result.
function D16_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D16_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D16S539' ;  
    setappdata (0,'locs_name13' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D16S539_global'); % data of locus from the saving operation
    D16S539_plot=getappdata(0,'D16S539_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D16S539.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D16S539_corr..xlsx' ,'Excel_Files\D16S539_PV.xlsx',locs_name,D16S539_plot,image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_13  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_13);

 % button for opening p-value exel file
 btn_PV_13 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_13);

 % button for opening figure contain avg with the population line
 btn_AVG_13 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_13);
    function corr_13(btn_cor_13, EventData)
        winopen('Excel_Files\D16S539_corr..xlsx');
    function PV_13(btn_PV_13, EventData)
        winopen('Excel_Files\D16S539_PV.xlsx');
    function AVG_13(btn_AVG_13, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D16S539_plot=getappdata(0,'D16S539_plot'); %  data of locus from the pre-tested populartion    

if isnan(D16S539_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name13') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name13') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D16S539_global=getappdata(0,'D16S539_global'); %read locus data
    [ Avr , D16S539_plot ] = Average( D16S539_global ,14,D16S539_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D16S539_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D16S539_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D16S539_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global average',Country) % create box of legend    
    end

% --- Executes on button press in D2_Result.
function D2_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D2_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D2S1338' ;  
    setappdata (0,'locs_name14' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D2S1338_global'); % data of locus from the saving operation
    D2S1338_plot=getappdata(0,'D2S1338_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D2S1338.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D2S1338_corr..xlsx' ,'Excel_Files\D2S1338_PV.xlsx',locs_name,D2S1338_plot,image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_14  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_14);

 % button for opening p-value exel file
 btn_PV_14 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_14);

 % button for opening figure contain avg with the population line
 btn_AVG_14 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_14);
    function corr_14(btn_cor_14, EventData)
        winopen('Excel_Files\D2S1338_corr..xlsx');
    function PV_14(btn_PV_14, EventData)
        winopen('Excel_Files\D2S1338_PV.xlsx');
    function AVG_14(btn_AVG_14, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D2S1338_plot=getappdata(0,'D2S1338_plot'); %  data of locus from the pre-tested populartion    
   
if isnan(D2S1338_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name14') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name14') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 
       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D2S1338_global=getappdata(0,'D2S1338_global'); %read locus data
    [ Avr , D2S1338_plot ] = Average( D2S1338_global ,2,D2S1338_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D2S1338_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D2S1338_plot) == 1 ;
        legend('Global average') % create box of legend    
    else
      plot(D2S1338_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global Average',Country) % create box of legend    
    end

% --- Executes on button press in D19_Result.
function D19_Result_Callback(hObject, eventdata, handles)
% hObject    handle to D19_Result (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    locs_name = 'D19S433' ;  
    setappdata (0,'locs_name15' , locs_name) ; %store the name of locus
    global_trend=getappdata(0,'D19S433_global'); % data of locus from the saving operation
    D19S433_plot=getappdata(0,'D19S433_plot'); %  data of locus from the pre-tested populartion
    image_name = 'Images\CCM_Sample_D19S433.jpg' ;   % screen shoot of sample of correlation matrix
 
 % Result function to create figure , ploting trends and showing the
 % statistics
    [fig] = Result( 'Excel_Files\D19S433_corr..xlsx' ,'Excel_Files\D19S433_PV.xlsx',locs_name,D19S433_plot, image_name , global_trend);   
 % button for opening CCM exel file
 btn_cor_15  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_15);

 % button for opening p-value exel file
 btn_PV_15 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_15);

 % button for opening figure contain avg with the population line
 btn_AVG_15 = uicontrol('Parent',fig,'Style','pushbutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.86 0.93 0.13 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_15);
    function corr_15(btn_cor_15, EventData)
        winopen('Excel_Files\D19S433_corr..xlsx');
    function PV_15(btn_PV_15, EventData)
        winopen('Excel_Files\D19S433_PV.xlsx');
    function AVG_15(btn_AVG_15, EventData)
    %open figure to plot AVG VS (Tested / saved) population
    figure ('Name','Test vs AVTs avg','units','normalized','outerposition',[0 0 1 1] );
    cla ;
    %divide the figure and write the title for figure
    ax1 = axes('Position',[0 0 1 1],'Visible','off');
    axes(ax1);
    D19S433_plot=getappdata(0,'D19S433_plot'); %  data of locus from the pre-tested populartion    
   
if isnan(D19S433_plot)
    T1 = 'AFTs Global Average';
    locs_name = getappdata(0,'locs_name15') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
else
    population_Name=getappdata(0,'population_Name') ;
    T1 = strcat(population_Name,' AFTs Vs Global Average');
    locs_name = getappdata(0,'locs_name15') ;
    Title_text = text(0.03,0.98,locs_name);
    Title_text2=text(0.38,0.98,T1);
    set(Title_text2, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
    set(Title_text, 'FontName', 'Times', 'FontSize', 26, 'FontWeight', 'bold', 'FontAngle', 'italic')
end 

       
    ax2 = axes('Position',[0 0.05 1 0.89],'Visible','off');    
    D19S433_global=getappdata(0,'D19S433_global'); %read locus data
    [ Avr , D19S433_plot ] = Average( D19S433_global ,2,D19S433_plot); %comput AVG

    axes(ax2);
    plot(Avr,'b'); hold on %plot AVG line (blue line)
    a=D19S433_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
  
    if  isnan(D19S433_plot) == 1 ;
        legend('Global Average') % create box of legend    
    else
      plot(D19S433_plot(:,2),'r','LineWidth',2); %plot tested/saved population (red line)
      Country =  getappdata(0,'population_Name') ;
      legend('Global Average',Country) % create box of legend    
    end
