function varargout = pop_comparison(varargin)
% POP_COMPARISON MATLAB code for pop_comparison.fig
%      POP_COMPARISON, by itself, creates a new POP_COMPARISON or raises the existing
%      singleton*.
%
%      H = POP_COMPARISON returns the handle to a new POP_COMPARISON or the handle to
%      the existing singleton*.
%
%      POP_COMPARISON('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in POP_COMPARISON.M with the given input arguments.
%
%      POP_COMPARISON('Property','Value',...) creates a new POP_COMPARISON or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before pop_comparison_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to pop_comparison_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help pop_comparison

% Last Modified by GUIDE v2.5 27-Aug-2017 12:43:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @pop_comparison_OpeningFcn, ...
                   'gui_OutputFcn',  @pop_comparison_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% --- Executes just before pop_comparison is made visible.
function pop_comparison_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to pop_comparison (see VARARGIN)

% Choose default command line output for pop_comparison
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes pop_comparison wait for user response (see UIRESUME)
% uiwait(handles.figure1);


Selected_populations = ' ' ;
setappdata(0,'Selected_populations',Selected_populations);

D3S1358_all = nan ;     setappdata(0,'D3S1358_all',D3S1358_all);
vWA_all= nan ;          setappdata(0,'vWA_all',vWA_all);
FGA_all = nan ;         setappdata(0,'FGA_all',FGA_all);
THO1_all = nan ;        setappdata(0,'THO1_all',THO1_all);
TPOX_all = nan ;        setappdata(0,'TPOX_all',TPOX_all);
CSF1PO_all = nan ;      setappdata(0,'CSF1PO_all',CSF1PO_all);
D5S818_all = nan ;      setappdata(0,'D5S818_all',D5S818_all);
D13S317_all = nan ;     setappdata(0,'D13S317_all',D13S317_all);
D7S820_all = nan ;      setappdata(0,'D7S820_all',D7S820_all);
D8S1179_all = nan ;     setappdata(0,'D8S1179_all',D8S1179_all);
D21S11_all = nan ;      setappdata(0,'D21S11_all',D21S11_all);
D16S539_all = nan ;     setappdata(0,'D16S539_all',D16S539_all);
D2S1338_all = nan ;     setappdata(0,'D2S1338_all',D2S1338_all);
D19S433_all = nan ;     setappdata(0,'D19S433_all',D19S433_all);
D18S51_all = nan ;     setappdata(0,'D18S51_all',D18S51_all);

% set popup_menu values (global)
[a,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Tested Populations'},Allcountries(1:end)];
set(handles.Popupmenu_TestedCountries,'String', Allcountries);

% set popup_menu values (rejected)
[a,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Populations Under Test'},Allcountries(2:end)];
set(handles.popupmenu_UnderTest,'String', Allcountries);

% T1 is a title of application  and edit67 is a text box that contain the title
T1 = 'Populations Comparison' ;  
set (handles.edit1,'String',T1);

    s=[];  % empty nector to clear x-axes
axes(handles.axes1); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes2); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes3); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes4); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes5); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes6); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes7); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes8); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes9); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes10); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes11); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes12); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes13); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes14); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes15); %select axes 
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s);
Selected_populations = ' ' ;
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

    % --- Outputs from this function are returned to the command line.
function varargout = pop_comparison_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Popupmenu_TestedCountries.
function Popupmenu_TestedCountries_Callback(hObject, eventdata, handles)
% get the string of pushued selection
contents = get(hObject,'String');
population_Name = contents{get(handles.Popupmenu_TestedCountries,'Value')};

if ( strcmp(population_Name , 'Tested Populations') )
  
Selected_populations = ' ' ;
setappdata(0,'Selected_populations',Selected_populations)
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

all_data = nan ;        setappdata(0,'all_data',all_data);
data = nan ;            setappdata(0,'data',data);
D3S1358_all = nan ;     setappdata(0,'D3S1358_all',D3S1358_all);
vWA_all= nan ;          setappdata(0,'vWA_all',vWA_all);
FGA_all = nan ;         setappdata(0,'FGA_all',FGA_all);
THO1_all = nan ;        setappdata(0,'THO1_all',THO1_all);
TPOX_all = nan ;        setappdata(0,'TPOX_all',TPOX_all);
CSF1PO_all = nan ;      setappdata(0,'CSF1PO_all',CSF1PO_all);
D5S818_all = nan ;      setappdata(0,'D5S818_all',D5S818_all);
D13S317_all = nan ;     setappdata(0,'D13S317_all',D13S317_all);
D7S820_all = nan ;      setappdata(0,'D7S820_all',D7S820_all);
D8S1179_all = nan ;     setappdata(0,'D8S1179_all',D8S1179_all);
D21S11_all = nan ;      setappdata(0,'D21S11_all',D21S11_all);
D16S539_all = nan ;     setappdata(0,'D16S539_all',D16S539_all);
D2S1338_all = nan ;     setappdata(0,'D2S1338_all',D2S1338_all);
D19S433_all = nan ;     setappdata(0,'D19S433_all',D19S433_all);
D18S51_all = nan ;     setappdata(0,'D18S51_all',D18S51_all);

[a,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Tested Populations'},Allcountries(1:end)];
set(handles.Popupmenu_TestedCountries,'String', Allcountries);

    s=[];  % empty vector to clear x-axes
axes(handles.axes1); %select axes
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes2); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes3); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes4); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes5); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes6); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes7); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes8); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes9); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes10); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes11); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes12); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes13); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes14); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes15); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s);

else 

Selected_populations = getappdata(0,'Selected_populations');
if (strcmp(Selected_populations(1) , ' '))
Selected_populations =  population_Name ;
else
Selected_populations=[Selected_populations(1:end) ; {population_Name}] ;
end
setappdata(0,'Selected_populations',Selected_populations)

% check if population was selected before or not
b = strcmp(population_Name,Selected_populations(1:end-1)) ;
if b == 0

% getting population data and plotting
[num,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;  
Allcountries =Allcountries(1,:);
Allcountries=[{' '},Allcountries(1:end)];
b = (strcmp(Allcountries,population_Name));

[data_1,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D3S1358') ;  
D3S1358_plot =  data_1(:,b);
D3S1358_all = getappdata(0,'D3S1358_all');
if isnan(D3S1358_all);
    D3S1358_all = [data_1(:,1) ,D3S1358_plot] ;
else
D3S1358_all(:,end+1) =  D3S1358_plot ;
end
setappdata(0,'D3S1358_all',D3S1358_all);

[data_2,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'vWA') ;  
vWA_plot =  data_2(:,b);
vWA_all = getappdata(0,'vWA_all');
if isnan(vWA_all)
    vWA_all =  [data_2(:,1),vWA_plot] ;
else
    vWA_all(:,end+1) =  vWA_plot ;
end
setappdata(0,'vWA_all',vWA_all);

[data_3,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'FGA') ;  
FGA_plot =  data_3(:,b);
FGA_all = getappdata(0,'FGA_all');
if isnan(FGA_all)
    FGA_all =  [data_3(:,1),FGA_plot] ;
else
FGA_all(:,end+1) =  FGA_plot ;
end
setappdata(0,'FGA_all',FGA_all);

[data_4,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'THO1') ;  
THO1_plot =  data_4(:,b);
THO1_all = getappdata(0,'THO1_all');
if isnan(THO1_all)
    THO1_all =  [data_4(:,1),THO1_plot ];
else
THO1_all(:,end+1) =  THO1_plot ;
end
setappdata(0,'THO1_all',THO1_all);

[data_5,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'TPOX') ;  
TPOX_plot =  data_5(:,b);
TPOX_all = getappdata(0,'TPOX_all');
if isnan (TPOX_all)
    TPOX_all =  [data_5(:,1),TPOX_plot] ;
else
TPOX_all(:,end+1) =  TPOX_plot ;
end
setappdata(0,'TPOX_all',TPOX_all);

[data_6,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'CSF1PO') ;  
CSF1PO_plot =  data_6(:,b);
CSF1PO_all = getappdata(0,'CSF1PO_all');
if isnan(CSF1PO_all)
    CSF1PO_all =  [data_6(:,1),CSF1PO_plot] ;
else
CSF1PO_all(:,end+1) =  CSF1PO_plot ;
end
setappdata(0,'CSF1PO_all',CSF1PO_all);

[data_7,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D5S818') ;  
D5S818_plot =  data_7(:,b);
D5S818_all = getappdata(0,'D5S818_all');
if isnan(D5S818_all)
    D5S818_all =  [data_7(:,1),D5S818_plot] ;
else
D5S818_all(:,end+1) =  D5S818_plot ;
end
setappdata(0,'D5S818_all',D5S818_all);

[data_8,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D13S317') ;  
D13S317_plot =  data_8(:,b);
D13S317_all = getappdata(0,'D13S317_all');
if isnan(D13S317_all)
D13S317_all =  [data_8(:,1),D13S317_plot] ;    
else
D13S317_all(:,end+1) =  D13S317_plot ;
end
setappdata(0,'D13S317_all',D13S317_all);

[data_9,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D7S820') ;  
D7S820_plot =  data_9(:,b);
D7S820_all = getappdata(0,'D7S820_all');
if isnan(D7S820_all)
D7S820_all =  [data_9(:,1),D7S820_plot] ;    
else
D7S820_all(:,end+1) =  D7S820_plot ;
end
setappdata(0,'D7S820_all',D7S820_all);

[data_10,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D8S1179') ;  
D8S1179_plot =  data_10(:,b);
D8S1179_all = getappdata(0,'D8S1179_all');
if isnan(D8S1179_all)
D8S1179_all =  [data_10(:,1),D8S1179_plot] ;    
else
D8S1179_all(:,end+1) =  D8S1179_plot ;
end
setappdata(0,'D8S1179_all',D8S1179_all);

[data_11,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D21S11') ;  
D21S11_plot =  data_11(:,b);
D21S11_all = getappdata(0,'D21S11_all');
if isnan(D21S11_all)
    D21S11_all =  [data_11(:,1),D21S11_plot] ;
else
D21S11_all(:,end+1) =  D21S11_plot ;
end
setappdata(0,'D21S11_all',D21S11_all);

[data_12,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D18S51') ;  
D18S51_plot =  data_12(:,b);
D18S51_all = getappdata(0,'D18S51_all');
if isnan(D18S51_all)
D18S51_all =  [data_12(:,1),D18S51_plot] ;    
else
D18S51_all(:,end+1) =  D18S51_plot ;
end
setappdata(0,'D18S51_all',D18S51_all);

[data_13,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D16S539') ;  
D16S539_plot =  data_13(:,b);
D16S539_all = getappdata(0,'D16S539_all');
if isnan(D16S539_all)
D16S539_all=  [data_13(:,1),D16S539_plot] ;    
else
D16S539_all(:,end+1) =  D16S539_plot ;
end
setappdata(0,'D16S539_all',D16S539_all);

[data_14,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D2S1338') ;  
D2S1338_plot =  data_14(:,b);
D2S1338_all = getappdata(0,'D2S1338_all');
if isnan(D2S1338_all)
D2S1338_all =  [data_14(:,1),D2S1338_plot] ;
else
D2S1338_all(:,end+1) =  D2S1338_plot ;
end
setappdata(0,'D2S1338_all',D2S1338_all);

[data_15,Allcountries] = xlsread('Excel_Files\Tested_populations.xlsx' , 'D19S433') ;  
D19S433_plot =  data_15(:,b);
D19S433_all = getappdata(0,'D19S433_all');
if isnan(D19S433_all)
    D19S433_all = [data_15(:,1), D19S433_plot] ;
else
D19S433_all(:,end+1) =  D19S433_plot ;
end
setappdata(0,'D19S433_all',D19S433_all);


    axes(handles.axes1);
    a=D3S1358_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D3S1358_all(:,2:end)); 
      
    axes(handles.axes2);
    a=vWA_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(vWA_all(:,2:end)); 
      
    axes(handles.axes3);
    a=FGA_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(FGA_all(:,2:end)); 
      
    axes(handles.axes4);
    a=THO1_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(THO1_all(:,2:end)); 
      
    axes(handles.axes5);
    a=TPOX_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(TPOX_all(:,2:end)); 
      
    axes(handles.axes6);
    a=CSF1PO_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(CSF1PO_all(:,2:end)); 
      
    axes(handles.axes7);
    a=D5S818_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D5S818_all(:,2:end)); 
      
    axes(handles.axes8);
    a=D13S317_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D13S317_all(:,2:end)); 
      
    axes(handles.axes9);
    a=D7S820_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D7S820_all(:,2:end)); 
      
    axes(handles.axes10);
    a=D8S1179_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D8S1179_all(:,2:end)); 
      
    axes(handles.axes11);
    a=D21S11_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D21S11_all(:,2:end));  
      
    axes(handles.axes12);
    a=D18S51_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D18S51_all(:,2:end)); 
      
    axes(handles.axes13);
    a=D16S539_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D16S539_all(:,2:end)); 
      
    axes(handles.axes14);
    a=D2S1338_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D2S1338_all(:,2:end)); 
      
    axes(handles.axes15);
    a=D19S433_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D19S433_all(:,2:end)); 

Selected_populations = getappdata(0,'Selected_populations') ;
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

else
        msgbox('Selected population is existed ','Error','error')
        Selected_populations = Selected_populations(1:end-1);
        setappdata(0,'Selected_populations',Selected_populations);
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

end
      

end
function Popupmenu_TestedCountries_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function popupmenu_UnderTest_Callback(hObject, eventdata, handles)
% get the string of pushued selection
contents = get(hObject,'String');
population_Name = contents{get(handles.popupmenu_UnderTest,'Value')};

if ( strcmp(population_Name , 'Populations Under Test') )
  
Selected_populations = ' ' ;
setappdata(0,'Selected_populations',Selected_populations)
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

all_data = nan ;        setappdata(0,'all_data',all_data);
data = nan ;            setappdata(0,'data',data);
D3S1358_all = nan ;     setappdata(0,'D3S1358_all',D3S1358_all);
vWA_all= nan ;          setappdata(0,'vWA_all',vWA_all);
FGA_all = nan ;         setappdata(0,'FGA_all',FGA_all);
THO1_all = nan ;        setappdata(0,'THO1_all',THO1_all);
TPOX_all = nan ;        setappdata(0,'TPOX_all',TPOX_all);
CSF1PO_all = nan ;      setappdata(0,'CSF1PO_all',CSF1PO_all);
D5S818_all = nan ;      setappdata(0,'D5S818_all',D5S818_all);
D13S317_all = nan ;     setappdata(0,'D13S317_all',D13S317_all);
D7S820_all = nan ;      setappdata(0,'D7S820_all',D7S820_all);
D8S1179_all = nan ;     setappdata(0,'D8S1179_all',D8S1179_all);
D21S11_all = nan ;      setappdata(0,'D21S11_all',D21S11_all);
D16S539_all = nan ;     setappdata(0,'D16S539_all',D16S539_all);
D2S1338_all = nan ;     setappdata(0,'D2S1338_all',D2S1338_all);
D19S433_all = nan ;     setappdata(0,'D19S433_all',D19S433_all);
D18S51_all = nan ;     setappdata(0,'D18S51_all',D18S51_all);

[a,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;
Allcountries =Allcountries(1,:);
Allcountries=[{'Populations Under Test'},Allcountries(2:end)];
set(handles.popupmenu_UnderTest,'String', Allcountries);

    s=[];  % empty vector to clear x-axes
    axes(handles.axes1); %select axes
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes2); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes3); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes4); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes5); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes6); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes7); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes8); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes9); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes10); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes11); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes12); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes13); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes14); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s); 
axes(handles.axes15); %select axes 
    cla
    set(gca,'XTickLabel',s); 
    set(gca,'YTickLabel',s);

else 
   
Selected_populations = getappdata(0,'Selected_populations');
if (strcmp(Selected_populations(1) , ' '))
Selected_populations =  population_Name ;
else
Selected_populations=[Selected_populations(1:end) ; {population_Name}] ;
end
setappdata(0,'Selected_populations',Selected_populations)

% check if population was selected before or not
b = strcmp(population_Name,Selected_populations(1:end-1)) ;
if b == 0

% getting population data and plotting
[num,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358') ;  
Allcountries =Allcountries(1,:);
Allcountries=[Allcountries(1:end)];
b = (strcmp(Allcountries,population_Name));

[data_1,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D3S1358')   ;
D3S1358_plot =  data_1(:,b);
D3S1358_all = getappdata(0,'D3S1358_all');
if isnan(D3S1358_all);
    D3S1358_all = [data_1(:,1) ,D3S1358_plot] ;
else
D3S1358_all(:,end+1) =  D3S1358_plot ;
end
setappdata(0,'D3S1358_all',D3S1358_all);

[data_2,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'vWA') ;  
vWA_plot =  data_2(:,b);
vWA_all = getappdata(0,'vWA_all');
if isnan(vWA_all)
    vWA_all =  [data_2(:,1),vWA_plot] ;
else
    vWA_all(:,end+1) =  vWA_plot ;
end
setappdata(0,'vWA_all',vWA_all);

[data_3,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'FGA') ;  
FGA_plot =  data_3(:,b);
FGA_all = getappdata(0,'FGA_all');
if isnan(FGA_all)
    FGA_all =  [data_3(:,1),FGA_plot] ;
else
FGA_all(:,end+1) =  FGA_plot ;
end
setappdata(0,'FGA_all',FGA_all);

[data_4,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'THO1') ;  
THO1_plot =  data_4(:,b);
THO1_all = getappdata(0,'THO1_all');
if isnan(THO1_all)
    THO1_all =  [data_4(:,1),THO1_plot ];
else
THO1_all(:,end+1) =  THO1_plot ;
end
setappdata(0,'THO1_all',THO1_all);

[data_5,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'TPOX') ;  
TPOX_plot =  data_5(:,b);
TPOX_all = getappdata(0,'TPOX_all');
if isnan (TPOX_all)
    TPOX_all =  [data_5(:,1),TPOX_plot] ;
else
TPOX_all(:,end+1) =  TPOX_plot ;
end
setappdata(0,'TPOX_all',TPOX_all);

[data_6,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'CSF1PO') ;  
CSF1PO_plot =  data_6(:,b);
CSF1PO_all = getappdata(0,'CSF1PO_all');
if isnan(CSF1PO_all)
    CSF1PO_all =  [data_6(:,1),CSF1PO_plot] ;
else
CSF1PO_all(:,end+1) =  CSF1PO_plot ;
end
setappdata(0,'CSF1PO_all',CSF1PO_all);

[data_7,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D5S818') ;  
D5S818_plot =  data_7(:,b);
D5S818_all = getappdata(0,'D5S818_all');
if isnan(D5S818_all)
    D5S818_all =  [data_7(:,1),D5S818_plot] ;
else
D5S818_all(:,end+1) =  D5S818_plot ;
end
setappdata(0,'D5S818_all',D5S818_all);

[data_8,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D13S317') ;  
D13S317_plot =  data_8(:,b);
D13S317_all = getappdata(0,'D13S317_all');
if isnan(D13S317_all)
D13S317_all =  [data_8(:,1),D13S317_plot] ;    
else
D13S317_all(:,end+1) =  D13S317_plot ;
end
setappdata(0,'D13S317_all',D13S317_all);

[data_9,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D7S820') ;  
D7S820_plot =  data_9(:,b);
D7S820_all = getappdata(0,'D7S820_all');
if isnan(D7S820_all)
D7S820_all =  [data_9(:,1),D7S820_plot] ;    
else
D7S820_all(:,end+1) =  D7S820_plot ;
end
setappdata(0,'D7S820_all',D7S820_all);

[data_10,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D8S1179') ;  
D8S1179_plot =  data_10(:,b);
D8S1179_all = getappdata(0,'D8S1179_all');
if isnan(D8S1179_all)
D8S1179_all =  [data_10(:,1),D8S1179_plot] ;    
else
D8S1179_all(:,end+1) =  D8S1179_plot ;
end
setappdata(0,'D8S1179_all',D8S1179_all);

[data_11,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D21S11') ;  
D21S11_plot =  data_11(:,b);
D21S11_all = getappdata(0,'D21S11_all');
if isnan(D21S11_all)
    D21S11_all =  [data_11(:,1),D21S11_plot] ;
else
D21S11_all(:,end+1) =  D21S11_plot ;
end
setappdata(0,'D21S11_all',D21S11_all);

[data_12,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D18S51') ;  
D18S51_plot =  data_12(:,b);
D18S51_all = getappdata(0,'D18S51_all');
if isnan(D18S51_all)
D18S51_all =  [data_12(:,1),D18S51_plot] ;    
else
D18S51_all(:,end+1) =  D18S51_plot ;
end
setappdata(0,'D18S51_all',D18S51_all);

[data_13,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D16S539') ;  
D16S539_plot =  data_13(:,b);
D16S539_all = getappdata(0,'D16S539_all');
if isnan(D16S539_all)
D16S539_all=  [data_13(:,1),D16S539_plot] ;    
else
D16S539_all(:,end+1) =  D16S539_plot ;
end
setappdata(0,'D16S539_all',D16S539_all);

[data_14,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D2S1338') ;  
D2S1338_plot =  data_14(:,b);
D2S1338_all = getappdata(0,'D2S1338_all');
if isnan(D2S1338_all)
D2S1338_all =  [data_14(:,1),D2S1338_plot] ;
else
D2S1338_all(:,end+1) =  D2S1338_plot ;
end
setappdata(0,'D2S1338_all',D2S1338_all);

[data_15,Allcountries] = xlsread('Excel_Files\Under_test_populations.xlsx' , 'D19S433') ;  
D19S433_plot =  data_15(:,b);
D19S433_all = getappdata(0,'D19S433_all');
if isnan(D19S433_all)
    D19S433_all = [data_15(:,1), D19S433_plot] ;
else
D19S433_all(:,end+1) =  D19S433_plot ;
end
setappdata(0,'D19S433_all',D19S433_all);

    axes(handles.axes1);
    a=data_1(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D3S1358_plot); 
      
    axes(handles.axes2);
    a=data_3(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(vWA_plot); 
      
    axes(handles.axes3);
    a=data_3(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(FGA_plot); 
      
    axes(handles.axes4);
    a=data_4(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(THO1_plot); 
      
    axes(handles.axes5);
    a=data_5(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(TPOX_plot); 
      
    axes(handles.axes6);
    a=data_6(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(CSF1PO_plot); 
      
    axes(handles.axes7);
    a=data_7(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D5S818_plot); 
      
    axes(handles.axes8);
    a=data_8(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D13S317_plot); 
      
    axes(handles.axes9);
    a=data_9(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D7S820_plot); 
      
    axes(handles.axes10);
    a=data_10(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D8S1179_plot); 
      
    axes(handles.axes11);
    a=data_11(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D21S11_plot);  
      
    axes(handles.axes12);
    a=data_12(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D18S51_plot); 
      
    axes(handles.axes13);
    a=data_13(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D16S539_plot); 
      
    axes(handles.axes14);
    a=data_14(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D2S1338_plot); 
      
    axes(handles.axes15);
    a=data_15(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    hold on ;
    plot(D19S433_plot); 
 
    else
        msgbox('Selected population is existed ','Error','error')
        Selected_populations = Selected_populations(1:end-1);
        setappdata(0,'Selected_populations',Selected_populations);
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

end
    
end
function popupmenu_UnderTest_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% plotting average
function togglebutton1_Callback(hObject, eventdata, handles)
a = get(hObject,'Value') ;
if a==1  %pushing button for first time
    
  %  select axeses then pre-processing data to compute AVG line then
  %  ploting
    axes(handles.axes1);
    D3S1358_global=getappdata(0,'D3S1358_global');
    D3S1358_plot = [nan , nan];
    [ Avr , D3S1358_plot ] = Average( D3S1358_global, 2 ,D3S1358_plot );
    plot(Avr(2:end),'b'); hold on
    a=D3S1358_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D3S1358_plot(:,2),'r','LineWidth',2);
 
    axes(handles.axes2);
    vWA_global=getappdata(0,'vWA_global');
    vWA_plot = [nan , nan];
    [ Avr , vWA_plot ] = Average( vWA_global ,3,vWA_plot);
    plot(Avr(2:end),'b'); hold on
    a=vWA_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(vWA_plot(:,2),'r','LineWidth',2);
    
    axes(handles.axes3);
    FGA_global=getappdata(0,'FGA_global');
    FGA_plot = [nan , nan];
    [ Avr , FGA_plot ] = Average( FGA_global ,4,FGA_plot );
    plot(Avr(2:end),'b'); hold on
    a=FGA_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(FGA_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes4);
    THO1_global=getappdata(0,'THO1_global');
    THO1_plot = [nan , nan];
    [ Avr , THO1_plot ] = Average( THO1_global ,5,THO1_plot );
    plot(Avr(2:end),'b'); hold on
    a=THO1_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(THO1_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes5);
    TPOX_global=getappdata(0,'TPOX_global');
    TPOX_plot = [nan , nan];
    [ Avr , TPOX_plot ] = Average( TPOX_global ,6,TPOX_plot);
    plot(Avr(2:end),'b'); hold on
    a=TPOX_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(TPOX_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes6);
    CSF1PO_global=getappdata(0,'CSF1PO_global');
    CSF1PO_plot = [nan , nan];
    [ Avr , CSF1PO_plot ] = Average( CSF1PO_global,7 ,CSF1PO_plot);
    plot(Avr(2:end),'b'); hold on
    a=CSF1PO_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(CSF1PO_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes7);
    D5S818_global=getappdata(0,'D5S818_global');
    D5S818_plot = [nan , nan];
    [ Avr , D5S818_plot ] = Average( D5S818_global,8 ,D5S818_plot);
    plot(Avr(2:end),'b'); hold on
    a=D5S818_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D5S818_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes8);
    D13S317_global=getappdata(0,'D13S317_global');
    D13S317_plot = [nan , nan];
    [ Avr , D13S317_plot ] = Average( D13S317_global,9,D13S317_plot );
    plot(Avr(2:end),'b'); hold on
    a=D13S317_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D13S317_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes9);
    D7S820_global=getappdata(0,'D7S820_global');
    D7S820_plot = [nan , nan];
    [ Avr , D7S820_plot ] = Average( D7S820_global ,10,D7S820_plot);
    plot(Avr(2:end),'b'); hold on
    a=D7S820_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D7S820_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes10);
    D8S1179_global=getappdata(0,'D8S1179_global');
    D8S1179_plot = [nan , nan];
    [ Avr , D8S1179_plot ] = Average( D8S1179_global,11,D8S1179_plot );
    plot(Avr(2:end),'b'); hold on
    a=D8S1179_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D8S1179_plot(:,2),'r','LineWidth',2);
  
     axes(handles.axes11);
    D21S11_global=getappdata(0,'D21S11_global');
    D21S11_plot = [nan , nan];
    [ Avr , D21S11_plot ] = Average( D21S11_global ,12,D21S11_plot);
    plot(Avr(2:end),'b'); hold on
    a=D21S11_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D21S11_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes12);
    D18S51_global=getappdata(0,'D18S51_global');
    D18S51_plot = [nan , nan];
    [ Avr , D18S51_plot ] = Average( D18S51_global ,13,D18S51_plot);
    plot(Avr(2:end),'b'); hold on
    a=D18S51_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D18S51_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes13);
    D16S539_global=getappdata(0,'D16S539_global');
    D16S539_plot = [nan , nan];
    [ Avr , D16S539_plot ] = Average( D16S539_global,14,D16S539_plot  );
    plot(Avr(2:end),'b'); hold on
    a=D16S539_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D16S539_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes14);
    D2S1338_global=getappdata(0,'D2S1338_global');
    D2S1338_plot = [nan , nan];
    [ Avr , D2S1338_plot ] = Average( D2S1338_global,15,D2S1338_plot );
    plot(Avr(2:end),'b'); hold on
    a=D2S1338_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D2S1338_plot(:,2),'r','LineWidth',2);
  
    axes(handles.axes15);
    D19S433_global=getappdata(0,'D19S433_global');
    D19S433_plot = [nan , nan];
    [ Avr , D19S433_plot ] = Average( D19S433_global ,16,D19S433_plot);
    plot(Avr(2:end),'b'); hold on
    a=D19S433_global(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    plot(D19S433_plot(:,2),'r','LineWidth',2);


Selected_populations = getappdata(0,'Selected_populations');
t = strcmp(Selected_populations,'Global Average') ;

if (strcmp(Selected_populations(1) , ' '))
Selected_populations =  'Global Average' ;

else
    if (t == 0)
    Selected_populations=[Selected_populations(1:end) ; {'Global Average'} ] ;
    else
    Selected_populations = Selected_populations ;
    end
end
setappdata(0,'Selected_populations',Selected_populations)
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

set (handles.togglebutton1,'String','Remove Global Average ');
else
    
Selected_populations = getappdata(0,'Selected_populations');
f = strcmp(Selected_populations , 'Global Average');
Selected_populations = Selected_populations(~f); 
if strcmp(Selected_populations , '')
Selected_populations=' ' ;
end
setappdata(0,'Selected_populations',Selected_populations);
set (handles.Selected_populations_table,'Data',cellstr(Selected_populations));

    D3S1358_all = getappdata(0,'D3S1358_all');
    axes(handles.axes1);
    a=D3S1358_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D3S1358_all(:,2:end)); 
      
vWA_all = getappdata(0,'vWA_all');
    axes(handles.axes2);
    a=vWA_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(vWA_all(:,2:end)); 
      
FGA_all = getappdata(0,'FGA_all');
    axes(handles.axes3);
    a=FGA_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(FGA_all(:,2:end)); 
      
THO1_all = getappdata(0,'THO1_all');
    axes(handles.axes4);
    a=THO1_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(THO1_all(:,2:end)); 
      
TPOX_all = getappdata(0,'TPOX_all');
    axes(handles.axes5);
    a=TPOX_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(TPOX_all(:,2:end)); 
      
CSF1PO_all = getappdata(0,'CSF1PO_all');
    axes(handles.axes6);
    a=CSF1PO_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(CSF1PO_all(:,2:end)); 
      
D5S818_all = getappdata(0,'D5S818_all');
    axes(handles.axes7);
    a=D5S818_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D5S818_all(:,2:end)); 
      
D13S317_all = getappdata(0,'D13S317_all');
    axes(handles.axes8);
    a=D13S317_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D13S317_all(:,2:end)); 
      
D7S820_all = getappdata(0,'D7S820_all');
    axes(handles.axes9);
    a=D7S820_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D7S820_all(:,2:end)); 
      
D8S1179_all = getappdata(0,'D8S1179_all');
    axes(handles.axes10);
    a=D8S1179_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D8S1179_all(:,2:end)); 
      
D21S11_all = getappdata(0,'D21S11_all');
    axes(handles.axes11);
    a=D21S11_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D21S11_all(:,2:end));
      
D18S51_all = getappdata(0,'D18S51_all');
    axes(handles.axes12);
    a=D18S51_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D18S51_all(:,2:end)); 
      
D16S539_all = getappdata(0,'D16S539_all');
    axes(handles.axes13);
    a=D16S539_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D16S539_all(:,2:end)); 
      
D2S1338_all = getappdata(0,'D2S1338_all');
    axes(handles.axes14);
    a=D2S1338_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D2S1338_all(:,2:end)); 
      
D19S433_all = getappdata(0,'D19S433_all');
    axes(handles.axes15);
    a=D19S433_all(:,1);
    s=[];
    set(gca,'XTick',[1:size(a,1)]);
    set(gca,'XTickLabel',s);
    cla
    plot(D19S433_all(:,2:end));     
   
    set (handles.togglebutton1,'String','AFTs Global Average');

end


% See result (15 buttons for 15 locs)
function pushbutton5_Callback(hObject, eventdata, handles)
    locs_name = 'D3S1358' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D3S1358_all=getappdata(0,'D3S1358_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D3S1358.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D3S1358_corr..xlsx' ,'\comparison_files\D3S1358_PV.xlsx',locs_name , image_name , D3S1358_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_1  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_1);

 % button for opening p-value exel file
 btn_PV_1 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_1);
 btn_AVG_1 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_1);

end
    function corr_1(btn_cor_1, EventData)   % callback for correlation button
        winopen('comparison_files\D3S1358_corr..xlsx');
    function PV_1(btn_PV_1, EventData)   % callback for p-value button
        winopen('comparison_files\D3S1358_PV.xlsx');
    function AVG_1(btn_AVG_1, EventData)
   if ( btn_AVG_1.Value)
    D3S1358_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D3S1358_global=getappdata(0,'D3S1358_global'); %read locus data
    [ Avr , D3S1358_plot ] = Average( D3S1358_global ,3,D3S1358_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)
    a=D3S1358_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_1.String='Remove Global Average' ;
   else
       cla
    D3S1358_all=getappdata(0,'D3S1358_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D3S1358_all(:,1);
    D3S1358_all=D3S1358_all(:,(2:end));
    hl=plot(D3S1358_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_1.String='AFTs Global Average';
   end
   
function pushbutton6_Callback(hObject, eventdata, handles)
    locs_name = 'vWA' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    vWA_all=getappdata(0,'vWA_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_vWa.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\vWA_corr..xlsx' ,'\comparison_files\vWA_PV.xlsx'...
     ,locs_name , image_name , vWA_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_2  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_2);

 % button for opening p-value exel file
 btn_PV_2 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_2);
 
 btn_AVG_2 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_2);
end
    function corr_2(btn_cor_2, EventData)
        winopen('comparison_files\vWA_corr..xlsx');
    function PV_2(btn_PV_2, EventData)
winopen('comparison_files\vWA_PV.xlsx');
    function AVG_2(btn_AVG_2, EventData)
   if ( btn_AVG_2.Value)
    vWA_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    vWA_global=getappdata(0,'vWA_global'); %read locus data
    [ Avr , vWA_plot ] = Average( vWA_global ,3,vWA_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=vWA_global(:,1);
    a=vWA_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_2.String='Remove Global Average' ;
   else
       cla
    vWA_all=getappdata(0,'vWA_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=vWA_all(:,1);
    vWA_all=vWA_all(:,(2:end));
    hl=plot(vWA_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_2.String='AFTs Global Average';
   end

function pushbutton7_Callback(hObject, eventdata, handles)
    locs_name = 'FGA' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    FGA_all=getappdata(0,'FGA_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_FGA.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\FGA_corr..xlsx' ,'\comparison_files\FGA_PV.xlsx'...
     ,locs_name , image_name , FGA_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_3  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_3);

 % button for opening p-value exel file
 btn_PV_3 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_3);

  btn_AVG_3 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_3);
end
    function corr_3(btn_cor_3, EventData)
        winopen('comparison_files\FGA_corr..xlsx');
    function PV_3(btn_PV_3, EventData)
        winopen('comparison_files\FGA_PV.xlsx');
    function AVG_3(btn_AVG_3, EventData)
   if ( btn_AVG_3.Value)
    FGA_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    FGA_global=getappdata(0,'FGA_global'); %read locus data
    [ Avr , FGA_plot ] = Average( FGA_global ,3,FGA_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=FGA_global(:,1);
    a=FGA_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_3.String='Remove Global Average' ;
else
       cla
    FGA_all=getappdata(0,'FGA_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=FGA_all(:,1);
    FGA_all=FGA_all(:,(2:end));
    hl=plot(FGA_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_3.String='AFTs Global Average';
   end

function pushbutton8_Callback(hObject, eventdata, handles)
    locs_name = 'THO1' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    THO1_all=getappdata(0,'THO1_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_THO1.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\THO1_all_corr..xlsx' ,'\comparison_files\THO1_all_PV.xlsx'...
     ,locs_name , image_name , THO1_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_4  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_4);

 % button for opening p-value exel file
 btn_PV_4 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_4);

  btn_AVG_4 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_4);
end
    function corr_4(btn_cor_4, EventData)
        winopen('comparison_files\THO1_corr..xlsx');
    function PV_4(btn_PV_4, EventData)
        winopen('comparison_files\THO1_PV.xlsx');
    function AVG_4(btn_AVG_4, EventData)
   if ( btn_AVG_4.Value)
    THO1_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    THO1_global=getappdata(0,'THO1_global'); %read locus data
    [ Avr , THO1_plot ] = Average( THO1_global ,3,THO1_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=THO1_global(:,1);
    a=THO1_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_4.String='Remove Global Average' ;
   else
       cla
    THO1_all=getappdata(0,'THO1_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=THO1_all(:,1);
    THO1_all=THO1_all(:,(2:end));
    hl=plot(THO1_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_4.String='AFTs Global Average';
   end

function pushbutton9_Callback(hObject, eventdata, handles)
    locs_name = 'TPOX' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    TPOX_all=getappdata(0,'TPOX_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_TPOX.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\TPOX_corr..xlsx' ,'\comparison_files\TPOX_PV.xlsx'...
     ,locs_name , image_name , TPOX_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_5  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_5);

 % button for opening p-value exel file
 btn_PV_5 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_5);

  btn_AVG_5 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_5);
end
    function corr_5(btn_cor_5, EventData)
        winopen('comparison_files\TPOX_corr..xlsx');
    function PV_5(btn_PV_5, EventData)
        winopen('comparison_files\TPOX_PV.xlsx');
    function AVG_5(btn_AVG_5, EventData)
   if ( btn_AVG_5.Value)
    TPOX_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    TPOX_global=getappdata(0,'TPOX_global'); %read locus data
    [ Avr , TPOX_plot ] = Average( TPOX_global ,3,TPOX_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=TPOX_global(:,1);
    a=TPOX_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_5.String='Remove Global Average' ;
   else
       cla
    TPOX_all=getappdata(0,'TPOX_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=TPOX_all(:,1);
    TPOX_all=TPOX_all(:,(2:end));
    hl=plot(TPOX_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_5.String='AFTs Global Average';
   end

function pushbutton10_Callback(hObject, eventdata, handles)
    locs_name = 'CSF1PO' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    CSF1PO_all=getappdata(0,'CSF1PO_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_CSF1PO.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\CSF1PO_corr..xlsx' ,'\comparison_files\CSF1PO_PV.xlsx'...
     ,locs_name , image_name , CSF1PO_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_6  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_6);

 % button for opening p-value exel file
 btn_PV_6 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_6);
 
 btn_AVG_6 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_6);
end
    function corr_6(btn_cor_6, EventData)
        winopen('comparison_files\CSF1PO_corr..xlsx');
    function PV_6(btn_PV_6, EventData)
        winopen('comparison_files\CSF1PO_PV.xlsx');
    function AVG_6(btn_AVG_6, EventData)
   if ( btn_AVG_6.Value)
    CSF1PO_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    CSF1PO_global=getappdata(0,'CSF1PO_global'); %read locus data
    [ Avr , CSF1PO_plot ] = Average( CSF1PO_global ,3,CSF1PO_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=CSF1PO_global(:,1);
    a=CSF1PO_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_6.String='Remove Global Average' ;
else
       cla
    CSF1PO_all=getappdata(0,'CSF1PO_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=CSF1PO_all(:,1);
    CSF1PO_all=CSF1PO_all(:,(2:end));
    hl=plot(CSF1PO_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_6.String='AFTs Global Average';
   end

function pushbutton11_Callback(hObject, eventdata, handles)
    locs_name = 'D5S818' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D5S818_all=getappdata(0,'D5S818_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D5S818.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D5S818_corr..xlsx' ,'\comparison_files\D5S818_PV.xlsx'...
     ,locs_name , image_name , D5S818_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_7  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_7);

 % button for opening p-value exel file
 btn_PV_7 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_7);

  btn_AVG_7 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_7);
end
    function corr_7(btn_cor_7, EventData)
        winopen('comparison_files\D5S818_corr..xlsx');
    function PV_7(btn_PV_7, EventData)
        winopen('comparison_files\D5S818_PV.xlsx');
    function AVG_7(btn_AVG_7, EventData)
   if ( btn_AVG_7.Value)
    D5S818_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D5S818_global=getappdata(0,'D5S818_global'); %read locus data
    [ Avr , D5S818_plot ] = Average( D5S818_global ,3,D5S818_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D5S818_global(:,1);
    a=D5S818_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
      btn_AVG_7.String='Remove Global Average' ;
else
       cla
    D5S818_all=getappdata(0,'D5S818_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D5S818_all(:,1);
    D5S818_all=D5S818_all(:,(2:end));
    hl=plot(D5S818_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_7.String='AFTs Global Average';
   end

function pushbutton12_Callback(hObject, eventdata, handles)
    locs_name = 'D13S317' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D13S317_all=getappdata(0,'D13S317_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D13S317.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D13S317_corr..xlsx' ,'\comparison_files\D13S317_PV.xlsx'...
     ,locs_name , image_name , D13S317_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_8  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_8);

 % button for opening p-value exel file
 btn_PV_8 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_8);

  btn_AVG_8 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_8);
end
    function corr_8(btn_cor_8, EventData)
        winopen('comparison_files\D13S317_corr..xlsx');
    function PV_8(btn_PV_8, EventData)
        winopen('comparison_files\D13S317_PV.xlsx');
    function AVG_8(btn_AVG_8, EventData)
   if ( btn_AVG_8.Value)
    D13S317_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D13S317_global=getappdata(0,'D13S317_global'); %read locus data
    [ Avr , D13S317_plot ] = Average( D13S317_global ,3,D13S317_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D13S317_global(:,1);
    a=D13S317_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_8.String='Remove Global Average' ;
   else
       cla
    D13S317_all=getappdata(0,'D13S317_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D13S317_all(:,1);
    D13S317_all=D13S317_all(:,(2:end));
    hl=plot(D13S317_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_8.String='AFTs Global Average';
   end

function pushbutton13_Callback(hObject, eventdata, handles)
    locs_name = 'D7S820' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D7S820_all=getappdata(0,'D7S820_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D7S820.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D7S820_corr..xlsx' ,'\comparison_files\D7S820_PV.xlsx'...
     ,locs_name , image_name , D7S820_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_9  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_9);

 % button for opening p-value exel file
 btn_PV_9 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_9);

  btn_AVG_9 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_9);
end
    function corr_9(btn_cor_9, EventData)
        winopen('comparison_files\D7S820_corr..xlsx');
    function PV_9(btn_PV_9, EventData)
        winopen('comparison_files\D7S820_PV.xlsx');
    function AVG_9(btn_AVG_9, EventData)
   if ( btn_AVG_9.Value)
    D7S820_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D7S820_global=getappdata(0,'D7S820_global'); %read locus data
    [ Avr , D7S820_plot ] = Average( D7S820_global ,3,D7S820_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D7S820_global(:,1);
    a=D7S820_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_9.String='Remove Global Average' ;
   else
       cla
    D7S820_all=getappdata(0,'D7S820_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D7S820_all(:,1);
    D7S820_all=D7S820_all(:,(2:end));
    hl=plot(D7S820_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_9.String='AFTs Global Average';
   end

function pushbutton14_Callback(hObject, eventdata, handles)
    locs_name = 'D8S1179' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D8S1179_all=getappdata(0,'D8S1179_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D8S1179.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D8S1179_corr..xlsx' ,'\comparison_files\D8S1179_PV.xlsx'...
     ,locs_name , image_name ,D8S1179_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_10  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_10);

 % button for opening p-value exel file
 btn_PV_10 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_10);

  btn_AVG_10 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_10);
end
    function corr_10(btn_cor_10, EventData)
        winopen('comparison_files\D8S1179_corr..xlsx');
    function PV_10(btn_PV_10, EventData)
        winopen('comparison_files\D8S1179_PV.xlsx');
    function AVG_10(btn_AVG_10, EventData)
   if ( btn_AVG_10.Value)
    D8S1179_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D8S1179_global=getappdata(0,'D8S1179_global'); %read locus data
    [ Avr , D8S1179_plot ] = Average( D8S1179_global ,3,D8S1179_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D8S1179_global(:,1);
    a=D8S1179_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_10.String='Remove Global Average' ;
   else
       cla
    D8S1179_all=getappdata(0,'D8S1179_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D8S1179_all(:,1);
    D8S1179_all=D8S1179_all(:,(2:end));
    hl=plot(D8S1179_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_10.String='AFTs Global Average';
   end

function pushbutton15_Callback(hObject, eventdata, handles)
    locs_name = 'D21S11' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D21S11_all=getappdata(0,'D21S11_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D21S11.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D21S11_corr..xlsx' ,'\comparison_files\D21S11_PV.xlsx'...
     ,locs_name , image_name , D21S11_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_11  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_11);

 % button for opening p-value exel file
 btn_PV_11 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_11);

  btn_AVG_11 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_11);
end
    function corr_11(btn_cor_11, EventData)
        winopen('comparison_files\D21S11_corr..xlsx');
    function PV_11(btn_PV_11, EventData)
        winopen('comparison_files\D21S11_PV.xlsx');
    function AVG_11(btn_AVG_11, EventData)
   if ( btn_AVG_11.Value)
    D21S11_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D21S11_global=getappdata(0,'D21S11_global'); %read locus data
    [ Avr , D21S11_plot ] = Average( D21S11_global ,3,D21S11_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D21S11_global(:,1);
    a=D21S11_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_11.String='Remove Global Average' ;
   else
       cla
    D21S11_all=getappdata(0,'D21S11_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D21S11_all(:,1);
    D21S11_all=D21S11_all(:,(2:end));
    hl=plot(D21S11_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_11.String='AFTs Global Average';
   end

function pushbutton16_Callback(hObject, eventdata, handles)
    locs_name = 'D18S51' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D18S51_all=getappdata(0,'D18S51_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D18S51.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D18S51_corr..xlsx' ,'\comparison_files\D18S51_PV.xlsx'...
     ,locs_name , image_name , D18S51_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_12  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_12);

 % button for opening p-value exel file
 btn_PV_12 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_12);

  btn_AVG_12 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_12);
end
    function corr_12(btn_cor_12, EventData)
        winopen('comparison_files\D18S51_corr..xlsx');
    function PV_12(btn_PV_12, EventData)
        winopen('comparison_files\D18S51_PV.xlsx');
    function AVG_12(btn_AVG_12, EventData)
   if ( btn_AVG_12.Value)
    D18S51_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D18S51_global=getappdata(0,'D18S51_global'); %read locus data
    [ Avr , D18S51_plot ] = Average( D18S51_global ,13,D18S51_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',20); hold on %plot AVG line (blue line)
    a=D18S51_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_12.String='Remove Global Average' ;
   else
       cla
    D18S51_all=getappdata(0,'D18S51_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D18S51_all(:,1);
    D18S51_all=D18S51_all(:,(2:end));
    hl=plot(D18S51_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
    btn_AVG_12.String='AFTs Global Average';
  end

function pushbutton17_Callback(hObject, eventdata, handles)
    locs_name = 'D16S539' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D16S539_all=getappdata(0,'D16S539_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D16S539.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D16S539_corr..xlsx' ,'\comparison_files\D16S539_PV.xlsx'...
     ,locs_name , image_name , D16S539_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_13  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_13);

 % button for opening p-value exel file
 btn_PV_13 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_13);

  btn_AVG_13 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_13);
end
    function corr_13(btn_cor_13, EventData)
        winopen('comparison_files\D16S539_corr..xlsx');
    function PV_13(btn_PV_13, EventData)
        winopen('comparison_files\D16S539_PV.xlsx');
    function AVG_13(btn_AVG_13, EventData)
   if ( btn_AVG_13.Value)
    D16S539_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D16S539_global=getappdata(0,'D16S539_global'); %read locus data
    [ Avr , D16S539_plot ] = Average( D16S539_global ,3,D16S539_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D16S539_global(:,1);
    a=D16S539_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_13.String='Remove Global Average' ;
   else
       cla
    D16S539_all=getappdata(0,'D16S539_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D16S539_all(:,1);
    D16S539_all=D16S539_all(:,(2:end));
    hl=plot(D16S539_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_13.String='AFTs Global Average';
   end

function pushbutton18_Callback(hObject, eventdata, handles)
    locs_name = 'D2S1338' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D2S1338_all=getappdata(0,'D2S1338_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D2S1338.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D2S1338_corr..xlsx' ,'\comparison_files\D2S1338_PV.xlsx'...
     ,locs_name , image_name , D2S1338_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_14  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_14);

 % button for opening p-value exel file
 btn_PV_14 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_14);

  btn_AVG_14 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_14);
end
    function corr_14(btn_cor_14, EventData)
        winopen('comparison_files\D2S1338_corr..xlsx');
    function PV_14(btn_PV_14, EventData)
        winopen('comparison_files\D2S1338_PV.xlsx');
    function AVG_14(btn_AVG_14, EventData)
   if ( btn_AVG_14.Value)
    D2S1338_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D2S1338_global=getappdata(0,'D2S1338_global'); %read locus data
    [ Avr , D2S1338_plot ] = Average( D2S1338_global ,3,D2S1338_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D2S1338_global(:,1);
    a=D2S1338_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_14.String='Remove Global Average' ;
   else
       cla
    D2S1338_all=getappdata(0,'D2S1338_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D2S1338_all(:,1);
    D2S1338_all=D2S1338_all(:,(2:end));
    hl=plot(D2S1338_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_14.String='AFTs Global Average';
   end

function pushbutton19_Callback(hObject, eventdata, handles)
    locs_name = 'D19S433' ;  
    setappdata (0,'locs_name1' , locs_name) ; %store the name of locus
    D19S433_all=getappdata(0,'D19S433_all'); % data of locus from the saving operation
    Selected_populations= getappdata(0,'Selected_populations');
    countries = Selected_populations' ;
    image_name = 'Images\CCM_Sample_D2S1338.jpg' ;   % screen shoot of sample of correlation matrix
 % Result function to create figure , ploting trends and showing the
 % statistics
 [fig,data] = Comparison_Result( '\comparison_files\D19S433_corr..xlsx' ,'\comparison_files\D19S433_PV.xlsx'...
     ,locs_name , image_name , D19S433_all,countries);
 % button for opening CCM exel file
if (~isnan(data))
 btn_cor_15  = uicontrol('Parent',fig,'Style','pushbutton','String','CCM File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ,'Units',...
     'normalized','Position',[0.12 0.665 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@corr_15);

 % button for opening p-value exel file
 btn_PV_15 = uicontrol('Parent',fig,'Style','pushbutton','String','P-value Matrix File' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.12 0.32 0.11 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@PV_15);

  btn_AVG_15 = uicontrol('Parent',fig,'Style','togglebutton','String','AFTs Global Average' ,...
     'FontWeight' ,'bold' ,'FontSize' , 10 ,'FontName' , 'Times' , 'ForegroundColor' , 'W' ...
     ,'Units','normalized','Position',[0.89 0.93 0.1 0.04],'Visible','on',...
     'BackgroundColor' , [0.153 0.227 0.373],'callback',@AVG_15);
end
    function corr_15(btn_cor_15, EventData)
        winopen('comparison_files\D19S433_corr..xlsx');
    function PV_15(btn_PV_15, EventData)
        winopen('comparison_files\D19S433_PV.xlsx');
    function AVG_15(btn_AVG_15, EventData)
   if ( btn_AVG_15.Value)
    D19S433_plot=[nan , nan]; %  data of locus from the pre-tested populartion    
    D19S433_global=getappdata(0,'D19S433_global'); %read locus data
    [ Avr , D19S433_plot ] = Average( D19S433_global ,3,D19S433_plot); %comput AVG
    plot(Avr(2:end-1)); hold on %plot AVG line 
    plot(Avr(end),'Color',[0 0 1],'LineWidth',4); hold on %plot AVG line (blue line)    a=D19S433_global(:,1);
    a=D19S433_global(:,1);
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    countries =  getappdata(0,'countries') ;
    countries=[countries,{'Global Average'}] ;
    legend(countries) % create box of legend    
   btn_AVG_15.String='Remove Global Average' ;
   else
       cla
    D19S433_all=getappdata(0,'D19S433_all'); % data of locus from the saving operation
    countries =  getappdata(0,'countries') ;
    a=D19S433_all(:,1);
    D19S433_all=D19S433_all(:,(2:end));
    hl=plot(D19S433_all); hold on % trend
    s=num2str(a);
    set(gca,'XTick',[1:size(a,1)]); 
    set(gca,'XTickLabel',s);
    legend(countries) % create box of legend    
   btn_AVG_15.String='AFTs Global Average';
   end
